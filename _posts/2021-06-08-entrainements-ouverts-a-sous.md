---
layout: post
date: 2021-06-08 07:17:40 +0000
title: Entraînements ouverts à sous
tags: []
images:
- "/uploads/196716248_1954899691331997_4870286482931342431_n.png"

---
**A partir de lundi 14 juin, pour toutes les séances, de toutes les catégories,** il est possible d'inviter ses ami.e.s pour essayer le hand ! 

Afin de gérer les effectifs, merci de **prévenir par mail de votre présence** : contact@hbc310.fr

N'hésitez pas à nous contacter si vous avez des questions. 