---
layout: post
date: 2021-01-03 11:25:08 +0000
title: Entraînements semaine de la rentrée ( 4-9 janvier )
tags:
- a la une
- club
images:
- "/uploads/programme-de-reprise-14-1.png"

---
Voici le planning de la semaine de rentrée. C'est une semaine provisoire, en attente de voir l'évolution de la situation. On reste dans l'idée de limiter les effectifs pour éviter trop de brassages. La contrainte du couvre feu est forte pour les catégories moins de 15 et surtout moins de 18. On espère d'ici la semaine du 11 janvier proposer deux créneaux par catégories. Et bien entendu, je le répète mais c'est important, on pense fort à nos Loisirs et nos seniors ! 