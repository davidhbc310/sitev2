---
date: 2020-05-17 09:08:00
images:
- /uploads/hbc-310-52__qagrtk.png
layout: post
title: Portrait N°21
---
<div class="news__content smart_pictures_width">
<span>Portrait N°21-Senior garçon-Arnaud</span>
<span>Merci à "Nono l'asticot" d'avoir pris le temps de compléter son portrait tout en restant "évasif" sur le rituel d'après victoires sous la douche des seniors garçons ! </span>
<span>A ce jour, la série s'arrête demain. Si un sursaut de dernière minute vous prend ( Moins de 18, seniors, Loisirs, arbitre...) j'attends toujours vos réponses, idéalement par mail. </span>
<span><span>#Teamhbc310</span></span>
</div>