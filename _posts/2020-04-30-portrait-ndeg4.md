---
date: 2020-04-30 09:03:00
images:
- /uploads/hbc-310-12__q9lee1.png
layout: post
title: Portrait n°4
---
<div class="news__content smart_pictures_width">
<span>Portrait n°4-Romain-Senior garçon</span>
<span>Recrue phare de la saison dernière from "La Meuse", Romain s'est très vite intégré au groupe seniors. Un vrai passionné avec une famille 100% handball. </span>
<span>Le rituel d'avant match est spécifique à l'Est de la France et n'est pas un modèle à suivre ;) ! </span>
<span>Merci Romain </span><span><span><span></span></span></span> </div>