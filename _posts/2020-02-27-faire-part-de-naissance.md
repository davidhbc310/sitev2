---
date: 2020-02-27 09:02:00
images:
- /uploads/le-site-internet-du-hbc-310-et-la-page-facebook-sont-heureux-de-vous-annoncer-la-naissance-du-compte-instagram-du-hbc-310-1__q6cp0e.png
layout: post
title: Faire part de naissance !
---
<div class="news__content smart_pictures_width">
<p>Voici donc la naissance d'un nouvel outil de communication afin de toucher une plus grande partie de nos licencié.e.s, notamment les plus jeunes. N'hésitez pas à suivre ce compte instagram ( qui sera un complément à la page FB ). </p>
<p><a href="https://www.instagram.com/handballclub310/" rel="nofollow noreferrer noopener">https://www.instagram.com/handballclub310/</a></p>
<p> </p> </div>