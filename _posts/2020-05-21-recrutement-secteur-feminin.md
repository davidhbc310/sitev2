---
date: 2020-05-21 11:17:00
images:
- /uploads/97374842_732293107309492_7658648449349533977_n__qaocjg.png
layout: post
title: Recrutement secteur féminin
---
<div class="news__content smart_pictures_width">
<p>Passionném'hand féminin.<br/>Le HBC 310 veut poursuivre le développement et le dynamisme de son pôle féminin. Les effectifs progressent d'années en années avec de plus en plus d'équipes ( 1 moins de 13, deux moins de 15, deux moins de 18, une équipe senior la saison dernière ) et des progrès notables dans les résultats. A vous de motiver et d'en parler autour de vous, débutantes ou confirmées seront les bienvenues avec en priorité : le niveau moins de 13 ( filles nées en 2008-2009 ), le niveau moins de 15 ( nées en 2006-2007 ) avec des joueuses confirmées pour l'équipe 1 région et des joueuses débutantes bienvenues pour l'équipe 2. Enfin l'équipe senior, en D1 départementale, aura aussi besoin de renforts.<br/>On compte sur vous ! Info et contact par mail : ronanhbc310@gmail.com</p> </div>