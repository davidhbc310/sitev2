---
layout: post
date: 2021-09-14 16:59:21 +0000
title: 'Packs de rentrée '
tags:
- important
images:
- "/uploads/baronnie-saint-malo-telechargement.jpg"

---
Attention à bien prendre commande des packs de rentrée, soit directement par le site internet ( [https://hbc310.fr/boutique/offre-rentree/](https://hbc310.fr/boutique/offre-rentree/ "https://hbc310.fr/boutique/offre-rentree/") ) soit en remplissant les flyers distribués et en les remettant aux entraîneurs ou à un responsable avec le paiement.

On arrête en fin de semaine avant de lancer la production. 

MERCI. 

Pour toute question : contact@hbc310.fr