---
layout: page
title: Données personnelles
permalink: /rgpd/
categories: site
---

Ce site n'utilise pas de cookies à l'exception de celui dont à besoin l'outil de gestion des consentements.  
La gestion de vos consentements est faite via tarteaucitron.io. Aucune donnée personnelle n'est récupérée pour réaliser cette action.  
<a href="https://tarteaucitron.io/fr/#tarteaucitron" title="Politique RGPD de tarteaucitron.io" rel="nofollow noreferrer noopener" target="_blank">Politique RGPD de tarteaucitron.io</a>

Aucune donnée personelle ne vous sera demandée sur ce site à l'exception :
 - d'un moyen de vous recontacter (email ou téléphone ...) si vous utilisez le formulaire de contact,
 - des informations nécessaires à la création de votre licence.

Voici les services externes pouvant être utilisés et soumis à votre approbation : 
 - Onesignal : outil outilisé pour la propagation des notifications.  
 <a href="https://onesignal.com/privacy" title="Politique RGPD de Onesignal" rel="nofollow noreferrer noopener" target="_blank">Politique RGPD de Onesignal</a>


 - Helloasso : plateforme utilisée par le club pour les inscriptions, billeteries et autres évenèments.  
 <a href="https://www.helloasso.com/confidentialite" title="Politique RGPD de Helloasso" rel="nofollow noreferrer noopener" target="_blank">Politique RGPD de Helloasso</a>

 - Score n'co : plateforme qui met à disposition les calendriers et classements.  
  <a href="https://scorenco.com/charte-sur-les-cookies/" title="Politique RGPD de Score n'co" rel="nofollow noreferrer noopener" target="_blank">Politique RGPD de Score n'co</a>
 


 

