---
layout: post
date: 2024-03-08 22:00:05 +0000
title: Loto
tags: 
- a la une
images:
- "/uploads/428518296_912040260923226_7866749163018842510_n.jpg"
- "/uploads/428518296_912040260923226_7866749163018842510_n_2.jpg"

---

Attention attention, voici la date de notre LOTO, animé par Gégé Loto.  
On vous donne rendez-vous **dimanche 14 avril** au centre culturel de Bréal, ouverture des portes à 11h30 pour un démarrage à 14h.  
On compte sur vous pour remplir la salle et diffuser l'info au maximum. 
 
*On aura forcément un grand besoin d'aide, on vous sollicitera sur les groupes whatsapp (préparation de la salle, restauration, accueil, caisses, rangement...plus on sera nombreux mieux ce sera ! ).*
