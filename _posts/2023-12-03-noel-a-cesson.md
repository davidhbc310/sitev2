---
layout: post
date: 2023-11-16 22:00:05 +0000
title: Invitation à Cesson
tags: 
- a la une
images:
- "/uploads/407408555_846854957441757_4369076605720642351_n.jpg"

---

**Bonne nouvelle, une première invitation est tombée et nous allons voir Cesson Chambéry à La Glaz Arena !**  
Gros match et grosse ambiance assurée avec un horaire adapté pour les plus jeunes, **le dimanche 17 à 16h** (toutes nos équipes jouent le samedi, à l'exception des seniors filles). Donc on compte sur vous en grand nombre, et n'hésitez pas à en faire profiter vos proches.  
Gratuit pour les licenciés.  
7 euros pour les accompagnateurs.  
**Pré réservation (avec paiement SVP sinon mettre "ZEROHBC310") obligatoire sur helloasso**. ATTENTION il faudra venir chercher les VRAIS billets lors d'une permanence à Bréal le vendredi 15 en fin d'après midi.  
*Ultime rappel il est possible de ne pas payer de frais helloasso, il faut juste le cocher avant le paiement. Si vous souhaitez venir à plusieurs ou être regroupés, essayer de gérer dans les groupes whatsapp ou m'envoyer un message (on verra si c'est possible en fonction du groupe).*  


<a href="https://www.helloasso.com/associations/handball-club-310/evenements/match-cesson-chambery-noel-2023?fbclid=IwAR2cOFvqk6gTbT_iFlhJDdtXL1tgMQY1U_KEqWEpKWpVhB3G5gjl6cSLbpY" class="button is-dark" target="_blank">Je m'inscris</a>