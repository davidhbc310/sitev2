---
layout: post
date: 2021-08-09 16:38:05 +0000
title: 'Permanences AOÛT '
tags:
- agenda
- a la une
images:
- "/uploads/ajouter-un-titre-20.png"

---
A noter, premières permanences d'août à Bréal, salle du collège ( salle de réunion à droite du gymnase ) : 

**-mercredi 18 de 18h30 à 19h30** 

**-vendredi 20 de 18h30 à 19h30**

Venez poser des questions ou finaliser votre inscription ( chèques, chèques vacances, pass sport ou autres ). 