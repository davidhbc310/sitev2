---
layout: post
date: 2021-12-09 07:50:15 +0000
title: 'Mobilisation générale '
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-79.png"

---
On compte sur l'ensemble du club pour venir encourager nos seniors garçons 1 dans un match au sommet **dimanche 12 décembre à 16h à Bréal.**

Ils sont opposés au RMH-Châteaubourg ( 1er et invaincus ) avec une équipe composée d'anciens joueurs de haut niveau. Un vrai défi pour notre équipe elle aussi toujours invaincue.

On compte sur votre mobilisation et votre soutien pour mettre de l'ambiance dans les tribunes !!

( [article ouest France ](https://www.ouest-france.fr/bretagne/breal-sous-montfort-35310/handball-un-match-de-haut-de-tableau-dimanche-890c8d46-cccb-41fe-928d-2ebb4119eb38?fbclid=IwAR1zN1kqdF8D_3Zfq_MNj40M3-5_p6OsOdEBnuQvbxeZUgHOa0QYv6gWqM8))