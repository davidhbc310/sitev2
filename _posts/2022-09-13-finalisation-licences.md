---
layout: post
date: 2022-09-13 16:15:53 +0000
title: 'Finalisation licences '
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-24.png"

---
🚨ULTRA GIGA MEGA IMPORTANT🚨  
Alerte inscriptions, on compte sur votre réactivité pour finaliser le processus !

En cas de problème, de doutes...envoyez un mail à l'adresse de contact du club : contact@hbc310.fr. 