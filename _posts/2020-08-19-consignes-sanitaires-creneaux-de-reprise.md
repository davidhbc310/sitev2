---
date: 2020-08-19 11:26:00
images:
- /uploads/programme-de-reprise-21__qfb0vm.png
layout: post
title: 'Consignes sanitaires : créneaux de reprise'
---
<div class="news__content smart_pictures_width">
<p>Bonjour à toutes et à tous, voici donc les règles mises en place pour continuer à pouvoir jouer dans les salles de Mordelles. Merci d'en tenir compte pour l'ensemble des créneaux de reprise. </p>
<p><strong>Et encore merci à la mairie de Mordelles pour sa confiance. </strong></p>
<p>Rappel : mercredi 19 : entraînement moins de 15 à Beauséjour de 17h à 18h15 puis entraînement moins de 18 de 18h15 à 19h30.</p>
<p>jeudi : créneau senior de 19h à 20h à Beauséjour</p>
<p>vendredi : Beauséjour, moins de 15 de 17h à 18h15, moins de 18 de 18h15 à 19h30 et seniors de 19h30 à 20h30.</p> </div>