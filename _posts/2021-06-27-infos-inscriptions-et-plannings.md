---
layout: post
date: 2021-06-27T15:47:41.000+00:00
title: Infos inscriptions et plannings
tags: []
images: []

---
Bonjour à toutes et à tous, 

Les inscriptions vont démarrer en milieu de semaine, idéalement dès mercredi 30 juin. On attend le conseil d'administration ( mardi soir ) qui va élire le Bureau directeur afin de lancer la procédure. 

Comme l'an dernier, la majorité des opérations sera dématérialisée. 

Pour les mineurs, le certificat médical n'est plus obligatoire, un simple questionnaire de santé sera à compléter. Tous les documents seront indiqués lors de la mise en ligne. 

Merci pour votre compréhension,

Ronan Céron-co président-pour le Bureau directeur.