---
layout: post
date: 2020-11-05T07:25:58.000+00:00
title: 'Opération chocolats '
tags: []
images:
- "/uploads/ajouter-un-titre-3.png"

---
Vos enfants ont dû ramener un flyer pour les chocolats avant les vacances. Nous avons décidé de maintenir l'opération. Pour passer commande, vous pouvez envoyer le flyer et un chèque par la Poste à Isabelle Kerzreho, la Petite Piletière, 35580 Bréal sous Montfort. 

Vous pouvez aussi le faire directement en ligne via [**_helloasso_**](https://www.helloasso.com/associations/handball-club-310/evenements/operation-chocolats-de-noel)**_._** 

Vous serez informés pour la livraison qui sera assurée dans le respect des consignes sanitaires ( on proposera les sorties d'écoles et de collèges par exemple à Bréal et Mordelles ).