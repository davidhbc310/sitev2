---
layout: post
date: 2021-08-20T07:25:50.000+00:00
title: 'Reprise semaine 2 et permanences ( 23 au 28 août ) '
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-34.png"

---
**Reprise pour les équipes engagées en compétition à Mordelles, salle Beauséjour**.

U11 + U13 : mardi 24 et jeudi 26 de 16h à 17h30

U15 : mardi 24 et jeudi 26 de 17h30 à 19h

U18 : mercredi 25 et vendredi 27 de 17h à 19h

Seniors ( + loisirs ) : mercredi 25 et vendredi 27 de 19h à 21h ( PASS SANITAIRE obligatoire )

A noter des créneaux exceptionnels dans le cadre du "challenge des Hermines" le samedi 28 août :

\-9h30-10h45 : école de hand ( lancelot et Arthur ) et U11

\-14h-15h45 : U13 + U15

Ce qui laissera la possibilité d'assister aux matchs de haut niveau en spectateurs juste après.

**Permanences licences ( chèques vacances, pass sport...)**

\-jeudi 26 de 17h à 18h ( entrée de Beauséjour )

\-vendredi 27 de 18h30 à 19h15

\-samedi 28 au moment des entraînements.