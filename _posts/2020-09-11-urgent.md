---
date: 2020-09-11 15:21:00
images:
- /uploads/programme-de-reprise-32__qghx3m.png
layout: post
title: URGENT
tags: [club,important]
---
<div class="news__content smart_pictures_width">
<span>[ URGENT ] Dernière minute. Merci de faire passer le message et de diffuser l'information autour de vous, aux personnes qui vous connaissez et qui ne suivent pas trop les actus sur internet. </span>
<span>On applique le protocole de la Fédé et nous sommes suspendus aux résultats. </span>
<strong>Annulation des créneaux des moins de 11 filles, moins de 11 garçons 1 et 2, moins de 13 garçons et moins de 13 filles, moins de 15 filles, moins de 18 filles équipe 1 et seniors filles en attendant les résultats du test Covid. </strong>
</div>