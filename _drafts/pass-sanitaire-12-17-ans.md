---
layout: post
date: 2021-09-30 06:27:29 +0000
title: PASS SANITAIRE 12/17 ans
tags:
- important
images:
- "/uploads/un-homme-montre-un-pass-sanitaire-sur-une-terrasse-de-restaurant-a-montpellier-le-11-aout-2021-1133195.jpg"

---
**A partir d'aujourd'hui ( jeudi 30 septembre ), le pass sanitaire est obligatoire pour les 12 ans et 2 mois /17 ans.**

Extrait de l'article Ouest France :

"_Qui se charge de vérifier les passes sanitaires ?_

Il appartient aux clubs de désigner un référent covid. C’est obligatoire. Ce dernier se charge d’organiser les contrôles de ses adhérents, ce qui n’est pas sans poser de problèmes. C’est la responsabilité qu’on donne aux associations qui complique la donne. Il n’y a pas les moyens humains pour le faire, regrette un président de fédération souhaitant garder l’anonymat. C’est clairement un enjeu, les bénévoles ne sont pas formés pour ça et ne peuvent pas tout faire​, renchérit Emmanuelle Bonnet-Oulaldj, co-présidente de la Fédération Sportive et gymnique du travail (FSGT), qui aurait préféré que cette tâche soit confiée partout sur le territoire aux agents municipaux ou au personnel de la sécurité privée.

_Quels risques en cas de manquement ?_

Selon la pratique et le lieu où elle est exercée, l’activité sportive peut être soumise à la présentation d’un passe sanitaire pour limiter la circulation du virus Covid-19 et de ses variants. En cas de manquement aux contrôles, la responsabilité civile d’un club pour la mise en place des règles sanitaires, et la responsabilité pénale de l’organisateur en cas de négligence avérée et grave, peuvent être engagées."

Source : [https://www.ouest-france.fr/sport/sport-tout-savoir-sur-l-entree-en-vigueur-du-passe-sanitaire-chez-les-12-17-ans-6fb8e36e-1d25-11ec-8d94-c5a42d01c526](https://www.ouest-france.fr/sport/sport-tout-savoir-sur-l-entree-en-vigueur-du-passe-sanitaire-chez-les-12-17-ans-6fb8e36e-1d25-11ec-8d94-c5a42d01c526 "https://www.ouest-france.fr/sport/sport-tout-savoir-sur-l-entree-en-vigueur-du-passe-sanitaire-chez-les-12-17-ans-6fb8e36e-1d25-11ec-8d94-c5a42d01c526")