

 

document.addEventListener('DOMContentLoaded', () => {

 

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }




  //quivkview
  var quickviews = bulmaQuickview.attach(); 


  //Filtre sur equipe si passé en parametre
  const urlParams = new URLSearchParams(window.location.search);
  const equipe = urlParams.get('equipe')
  if(equipe != undefined){

    document.getElementById("filtre-equipes").value = "equipe"+equipe;

    if(document.getElementById("filtre-equipes").value === ""){
      window.location.href = "https://hbc310.fr/equipes/";
    }

    filterElement("filtre-equipes")
  }

  //Si un filtre semaine est présent sur la page, on le cale sur la semaine en cours
  const filtreSemaine = document.getElementById("filtre-semaines")
  if(filtreSemaine != undefined){
    var now = new Date();

    const currentTimeStamp = Math.round(now.getTime()/1000)
    
    //Parcours de toutes les semaines

    for (let i = 0; i < (filtreSemaine.children).length ; i++) {



      const words = filtreSemaine[i].getAttribute("value").split('_');



      let weekStart = words[0];
      let weekEnd = words[1];



      
      if(currentTimeStamp >= weekStart && currentTimeStamp <= weekEnd){
        document.getElementById("filtre-semaines").value = filtreSemaine[i].value;
        filterElementInterval("filtre-semaines","data-match-week-start","data-match-week-end","content-filter-semaine")
        break;

      }


        
    }
  }

// Initialize all input of date type.
const calendars = bulmaCalendar.attach('[type="date"]', {
  showTodayButton: false,
  clearLabel: "Effacer",
  cancelLabel: "Annuler",
  dateFormat:"yyyy/MM/dd",
  weekStart: 1,
  enableYearSwitch: false,
  onReady: function(datepicker) {
    //alert(datepicker.data.value());
  }
});

$("#respSallesForm").submit(function(event){
  
  event.preventDefault(); 
  sendFormHelpWE
});








// Functions to open and close a modal


// Add a click event on buttons to open a specific modal
(document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
  const modal = $trigger.dataset.target;
  const $target = document.getElementById(modal);


  $trigger.addEventListener('click', () => {


    openModalJoueur($target);
  });
});

// Add a click event on various child elements to close the parent modal
(document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
  const $target = $close.closest('.modal');

  $close.addEventListener('click', () => {
    closeModalJoueur($target);
  });
});

// Add a keyboard event to close all modals
document.addEventListener('keydown', (event) => {
  const e = event || window.event;

  if (e.keyCode === 27) { // Escape key
    closeAllModalsJoueur();
  }
});



});

function openModalJoueur($el) {

  $el.classList.toggle('is-active',true);
}

function closeModalJoueur($el) {
  $el.classList.toggle('is-active',false);
}

function closeAllModalsJoueur() {
  (document.querySelectorAll('.modal') || []).forEach(($modal) => {
    closeModalJoueur($modal);
  });
}

function afficherJoueurSuivant(modalJoueurCourant){
  const currentId = modalJoueurCourant;

  var modals = document.querySelectorAll('.modal');

  var modalAAfficher = modals[0];
  var modalAMasquer = modals[modals.length-1];

  for (let i = 0; i < modals.length -1; i++) {
    let current = modals[i];

    if(currentId == current.id){
      modalAMasquer = current;
      modalAAfficher = modals[i+1];
      break;
    }
  }

  openModalJoueur(modalAAfficher);
  closeModalJoueur(modalAMasquer);
    
}

function afficherJoueurPrecedent(modalJoueurCourant){
  const currentId = modalJoueurCourant;

  var modals = document.querySelectorAll('.modal');

  var modalAAfficher = modals[0];
  var modalAMasquer = modals[modals.length-1];

  for (let i = modals.length -1; i >= 0; i--) {
    let current = modals[i];

    if(currentId == current.id){
      modalAMasquer = current;
      modalAAfficher = modals[i-1];
      break;
    }
  }

  openModalJoueur(modalAAfficher);
  closeModalJoueur(modalAMasquer);
}



function copyLink(url){  
  
  var dummy = $('<input>').val(url).appendTo('body').select()
  document.execCommand('copy')
  
}




function capitalize(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}



function updateActuHome(tagElement,newStyle){

news = document.getElementsByClassName("frontend-news");
for (i = 0; i < news.length; i++) {
    box=news[i];
    if(!box.classList.contains("is-hidden")){
      box.classList.add("is-hidden");
    }
    

   
    //box.parentElement.className = "column";
}
for (i = 1; i < 4; i++) {
  var column = document.getElementById(tagElement+"-"+i);
  column.classList.remove("is-hidden");
}
//MAJ du tire du caroussel news
document.getElementById("titreNews").innerText = capitalize(tagElement);

tagsElmts = document.getElementsByClassName("tagHomeNews");
for (i = 0; i < tagsElmts.length; i++) {
    tagElt=tagsElmts[i];
    if(tagElt.classList.contains(newStyle)){
      tagElt.classList.remove(newStyle);
      break;
    }
}

document.getElementById("tag-"+tagElement).classList.add(newStyle);



}


/**** lightbox ***/
function openModal() {
  document.getElementById('myModal').style.display = "flex";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("item-slide");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}


/** Pagination matchs */

function initStatusPagination(pagination,equipe){
  
  var table = document.getElementById("calendrier"+equipe);
  var nextButton = document.getElementById("nextButton"+equipe);
  var previousButton = document.getElementById("previousButton"+equipe);
  var nbEvents = (table.children).length;

  if(  nbEvents > (pagination+1)){
    previousButton.classList.remove("is-hidden");
    nextButton.classList.remove("is-hidden");
  }
  else{
    nextButton.classList.add("is-hidden");
    previousButton.classList.add("is-hidden");   
  }
}

function afficherPagePrecedente(pagination,equipe,scrollToElementId){
  var currentPageElement = document.getElementById("currentPage"+equipe);
  var numCurrentPage = parseInt(currentPageElement.value);
  var previousPage = numCurrentPage-1;

  afficherPage(numCurrentPage,previousPage,pagination,equipe);

  document.getElementById(scrollToElementId).scrollIntoView();

  currentPageElement.value = previousPage;
}

function afficherPageSuivante(pagination,equipe,scrollToElementId){
  var currentPageElement = document.getElementById("currentPage"+equipe);
  var numCurrentPage = parseInt(currentPageElement.value);
  var nextPage = numCurrentPage+1;

  afficherPage(numCurrentPage,nextPage,pagination,equipe);

  document.getElementById(scrollToElementId).scrollIntoView();

  currentPageElement.value = nextPage;

}

function afficherPage(pageOrigine,pageDestination,pagination,equipe){
  var table = document.getElementById("calendrier"+equipe);
  var nextButton = document.getElementById("nextButton"+equipe);
  var previousButton = document.getElementById("previousButton"+equipe);

  for (let i = 0; i < (table.children).length-1 ; i++) {

    var row = table.children[i];
    var borneInf = pageDestination*pagination;
    var borneSup = borneInf+pagination;

    if(i>=borneInf && i<borneSup){
      row.classList.remove('is-hidden');
    }
    else{
      row.classList.add('is-hidden');
    }
    
  }

  if((pageDestination+1)*pagination>=(table.children).length-1){
    nextButton.setAttribute("disabled","disabled");
  }
  else{
    nextButton.removeAttribute("disabled");
  }

  if((pageDestination-1)*pagination<0){
    previousButton.setAttribute("disabled","disabled");
  }
  else{
    previousButton.removeAttribute("disabled");
  }

}




function openTab(evt, tabName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("content-tab");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tab");
  for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" is-active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " is-active";
}

function filterElement(filterValue,contentFilterName = "content-filter") {
  var i;
  var selectedValue = document.getElementById(filterValue).value;
  

  var x = document.getElementsByClassName(contentFilterName);
  
  for (i = 0; i < x.length; i++) {
      
    if(x[i].id.includes(selectedValue) || selectedValue == -1){
      x[i].classList.add("is-visible");
      x[i].classList.remove("is-hidden");
    }
    else{
      x[i].classList.remove("is-visible");
      x[i].classList.add("is-hidden");
    }
  }

  //Suppression des jours vides
  
  var jours = document.getElementsByClassName("jour-container");

  for (i = 0; i < jours.length; i++) {

    var jour = jours[i].getAttribute('data-jour');

      var seances = jours[i].getElementsByClassName("column");
      var pasDeSeances=true;
      for (var j = 0; j < seances.length; j++) {
        if(seances[j].classList.contains('is-visible')){
          pasDeSeances=false;
          break;
        }
      }

      if(pasDeSeances){
        document.getElementById(jour).classList.add("is-hidden");
        document.getElementById(jour).classList.remove("is-visible");
      }
      else{
        document.getElementById(jour).classList.add("is-visible");
        document.getElementById(jour).classList.remove("is-hidden");
      }


    }
  }

  function filterElementInterval(newValue,minField,maxField,contentFilterName) {
    var i;
    var selectedValue = document.getElementById(newValue).value;

    const words = selectedValue.split('_');


    let selectedWeekStart = words[0];
    let selectedWeekEnd = words[1];



    var x = document.getElementsByClassName(contentFilterName);
  
    for (i = 0; i < x.length; i++) {
      
      if(selectedWeekStart <= x[i].getAttribute(minField) &&  selectedWeekEnd >= x[i].getAttribute(maxField)  ){
        x[i].classList.add("is-visible");
        x[i].classList.remove("is-hidden");
      }
      else{
        x[i].classList.remove("is-visible");
        x[i].classList.add("is-hidden");
      }

    }

    //Suppression pour les calendriers vides
  
  var cals = document.getElementsByClassName("list-calendar ");

  var nbCalsVisible = 0;
  for (i = 0; i < cals.length; i++) {

    var cal = cals[i].getAttribute('data-calendrier');
      var matchs = cals[i].getElementsByClassName("list-item");
      var pasDematch=true;
      for (var j = 0; j < matchs.length; j++) {
        if(matchs[j].classList.contains('is-visible')){
          pasDematch=false;
          nbCalsVisible++;
        }
      }

      if(pasDematch){
        cals[i].classList.add("is-hidden");
        cals[i].classList.remove("is-visible");

        cals[i].parentElement.parentElement.classList.add("is-hidden");
        cals[i].parentElement.parentElement.classList.remove("is-visible");
      }
      else{
        cals[i].classList.add("is-visible");
        cals[i].classList.remove("is-hidden");

        cals[i].parentElement.parentElement.classList.add("is-visible");
        cals[i].parentElement.parentElement.classList.remove("is-hidden");
      }


    }


    //Affichage des infos liées à la présence de match
    var noMatchInfos = document.getElementsByClassName("no-match-conditional");

    for (i = 0; i < noMatchInfos.length; i++) {
      noMatchInfo=noMatchInfos[i];

      if(nbCalsVisible > 0){
        if(!noMatchInfo.classList.contains("is-hidden")){
          noMatchInfo.classList.add("is-hidden");
          noMatchInfo.classList.remove("is-visible");
        }
        
      }
      else{
        if(!noMatchInfo.classList.contains("is-visible")){
          noMatchInfo.classList.add("is-visible");
          noMatchInfo.classList.remove("is-hidden");
        }
        
      }
    }

    var matchInfosConditionnal = document.getElementsByClassName("match-conditional");

    for (i = 0; i < matchInfosConditionnal.length; i++) {
      matchInfosConditionnal=matchInfosConditionnal[i];

      if(nbCalsVisible > 0){
        if(!matchInfosConditionnal.classList.contains("is-visible")){
          matchInfosConditionnal.classList.add("is-visible");
          matchInfosConditionnal.classList.remove("is-hididen");
        }
        
      }
      else{
        if(!matchInfosConditionnal.classList.contains("is-hidden")){
          matchInfosConditionnal.classList.add("is-hidden");
          matchInfosConditionnal.classList.remove("is-visible");
        }
        
      }
    }
    
    

  }

  function selectNextSemaine(){

    var selectedWeek = document.getElementById("filtre-semaines").selectedIndex;
    var target = selectedWeek+1;
    const nbWeek = document.getElementById("filtre-semaines").options.length;
    
    if(target<=nbWeek-1){


        document.getElementById("filtre-semaines").value = document.getElementById("filtre-semaines").options[target].value;
        filterElementInterval("filtre-semaines","data-match-week-start","data-match-week-end","content-filter-semaine")
    }
  }

  function selectPreviousSemaine(){
    var selectedWeek = document.getElementById("filtre-semaines").selectedIndex;
    var target = selectedWeek-1;


    if(target>=0){

        document.getElementById("filtre-semaines").value = document.getElementById("filtre-semaines").options[target].value
        filterElementInterval("filtre-semaines","data-match-week-start","data-match-week-end","content-filter-semaine")
    }
  }

  


