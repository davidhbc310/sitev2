---
date: 2020-02-07 16:10:00
images:
- /uploads/saucisson__q5c7hk.png
layout: post
title: Solidarité HBC 310
---
<div class="news__content smart_pictures_width">
<p>L'un de nos seniors garçons, Thomas Méric est actuellement étudiant à l'IFSI de Ma<span>yenne.</span></p>
<p><span> Il va partir faire un stage à la Réunion en octobre et mène différentes opérations pour réduire le coût du voyage.</span></p>
<p><span><br/>Il propose la vente de saucissons ( au choix : nature, fumé, aux noisettes, aux noix, au reblochon, aux herbes ou au poivre ). </span></p>
<p><span><strong>4 euros pièce, 10 euros les 3, 15 euros les 5 et 20 euros les 7.</strong></span></p>
<p><span><br/>Parfait pour fêter les victoires d'après match du week-end. <span><span> </span></span></span></p></div>