---
date: 2020-05-09 10:56:00
images:
- /uploads/hbc-310-222__qa241z.png
layout: post
title: Message du Bureau directeur
---
<div class="news__content smart_pictures_width">
<p><strong>Cher(e)s licencié(e)s, </strong></p>
<p>Voilà maintenant plus de 50 jours que le confinement a commencé et que les activités du club se sont arrêtées. </p>
<p>Nous espérons que la grande famille du HBC 310 se porte bien, avec une pensée particulière à celles et ceux qui ont pu être touchés de près ou de loin par ce virus et aussi bien entendu à tous les membres du club qui ont été en première ligne, à la fois dans le monde médical ou paramédical mais aussi dans toutes les professions qui ont contribué à la chaîne de solidarité. </p>
<p>Nous sommes impatients de tous vous retrouver pour revoir les sourires des plus petits de l'école de hand, pour revoir l'ensemble des catégories du club, des moins de 11 aux seniors, pour retrouver la bonne humeur des licenciés Loisirs, les parents, les bénévoles ainsi que l'ensemble des partenaires qui soutiennent le HBC 310 ! </p>
<p>Comme vous le savez, la décision d'une éventuelle reprise d'activités est encore impossible à prendre faute d'une vision claire de l'évolution de la situation sanitaire. </p>
<p>Nous dépendons des décisions à différentes échelles : mairies, préfecture et gouvernement. Pour le moment les activités physiques en salle restent interdites pour plusieurs semaines. Un nouveau point sera réalisé au début du mois de juin. Nous ferons une nouvelle communication à cette occasion.</p>
<p>Dès que les Autorités le permettront et dans le respect des règles en vigueur prévues à ce moment, le club organisera une journée de partage pour nous retrouver, jouer à nouveau si possible et échanger dans le cadre de l’Assemblée Générale. </p>
<p>Nous ne préférons pas prendre de décisions hâtives, certains clubs ont annoncé déjà l'arrêt définitif de leurs activités, nous préférons rester optimistes et garder un ( mince ) espoir d'une reprise partielle avec des modalités qui seront peut être à inventer ( extérieur ? petits groupes ? )</p>
<p>Dans l’attente de ces jours meilleurs, nous préparons depuis le début de la crise la saison prochaine avec beaucoup d'enthousiasme et de motivation. Nous avons trouvé notre nouvel entraîneur principal, qui sera épaulé par un 2ème salariè à temps partiel pour une meilleure prise en charge des différentes catégories. </p>
<p> </p>
<p>Pour le moment, le casse tête qui s'annonce concerne les plannings, avec un retard de la nouvelle salle de sports de Bréal ( qui deviendra "notre" salle ) et les dégradations de la salle de tennis ( qui impliquent un partage et une redistribution des créneaux de façon temporaire ). </p>
<p>Nous faisons au mieux, en avançant encore un peu dans le brouillard. Là aussi bien entendu, nous vous tiendrons informés dès que nous aurons plus d'informations. </p>
<p> </p>
<p>Enfin, n'oubliez pas de suivre le club sur les réseaux sociaux ( site internet mais surtout page FB et compte Instagram ) qui restent très actifs depuis le début du confinement et qui permettent de maintenir le lien...dans la bonne humeur ! </p>
<p> </p>
<p>Nous espérons toutes et tous vous revoir au plus vite et surtout en pleine forme.   </p>
<p> </p>
<p><strong>Les membres du Bureau du HBC 310</strong></p> </div>