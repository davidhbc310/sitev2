---
layout: post
date: 2021-06-13 19:39:06 +0000
title: 'Entraînements ouverts à tous '
tags: []
images:
- "/uploads/handball-club-310-89.png"

---
A partir de lundi 14 juin, toutes les séances, de toutes les catégories, sont ouvertes pour des essais. Les débutants sont les bienvenus ainsi que les anciens handballeurs qui auraient fait une pause la saison dernière. 

Il n'y a pas d'âge pour commencer. 

Merci de prévenir avant par mail à contact@hbc310.fr

On compte sur vous nombreux ! 