---
layout: post
date: 2021-09-01 16:39:55 +0000
title: 'Permanences FORUMS des associations '
tags:
- a la une
images:
- "/uploads/241029511_2022216457933653_1502984619562282341_n.png"

---
Le HBC 310 sera présent : 

\-samedi 4 septembre aux forums de Bréal, de Mordelles et du Rheu. 

\-samedi 11 septembre à St Thurial. 

N'hésitez pas à venir nous voir pour les papiers, le paiement des licences ou...pour dire bonjour ! 