---
layout: post
date: 2020-12-14T19:49:37.000+00:00
title: 'Reprise exceptionnelle du 15 au 19 décembre '
tags: []
images:
- "/uploads/programme-de-reprise-4.png"

---
Et oui la reprise est effective à partir de mardi 15 décembre pour les mineur.e.s dans le strict respect des consignes sanitaires. Le planning a été établi pour permettre à toutes les équipes d'avoir un créneau, en limitant les brassages et en respectant les contraintes du couvre-feu.