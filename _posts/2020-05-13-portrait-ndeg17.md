---
date: 2020-05-13 08:52:00
images:
- /uploads/hbc-310-46__qa9cfq.png
layout: post
title: Portrait N°17
---
<div class="news__content smart_pictures_width">
<span>Portrait N°17-Senior fille-Margaux </span>
<span>Merci à Margaux d'avoir pris le temps de compléter ce portrait afin de mieux la découvrir. J'espère que ça donnera envie à certains d'entre vous de rejoindre l'équipe Loisirs ;) et de venir plus nombreux soutenir les seniors filles l'an prochain.</span>
<span>Je n'ai plus beaucoup de stocks, donc si vous voulez maintenir cette publication quotidienne, à vous de motiver et de relancer autour de vous les moins de 18 ( où sont les gars ? ) les seniors, les arbitres du club et les dirigeants. </span>
<span><span>#TeamHBC310</span></span> <span><span><span> </span></span></span></div>