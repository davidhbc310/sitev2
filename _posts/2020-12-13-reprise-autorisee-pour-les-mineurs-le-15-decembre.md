---
layout: post
date: 2020-12-13 15:38:08 +0000
title: 'Reprise autorisée pour les mineurs le 15 décembre '
tags:
- club
images:
- "/uploads/programme-de-reprise-1.png"

---
Le ministère des sports a donc finalement validé la reprise pour les mineurs. Pour le moment nous ne connaissons pas l'ensemble du protocole sanitaire imposé. Nous attendons donc lundi 14 avant de communiquer le planning exceptionnel de cette semaine. A cette occasion nous donnerons aussi un calendrier de permanences pour récupérer les chocolats de Noël. Enfin, nous donnerons aussi les dates des stages de Noël. 