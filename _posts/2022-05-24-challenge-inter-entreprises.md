---
layout: post
date: 2022-05-24 05:59:41 +0000
title: 'Challenge inter entreprises '
tags:
- a la une
images:
- "/uploads/chartres-de-bretagne-1.png"

---
A noter dans vos agendas, un moment convivial avec les entreprises du secteur ( plus ou moins proche ).

Tout seul, à plusieurs ou carrément en groupe, on vous accueillera avec grand plaisir pour un petit tournoi amical. **Mercredi 15 juin à Bréal** ( nouvelle salle ) à partir de 20h30.

Les débutants sont les bienvenus.

N'hésitez pas à partager autour de vous et à inviter du monde !

Contact via les réseaux, par mail ou téléphone.

On compte sur vous.