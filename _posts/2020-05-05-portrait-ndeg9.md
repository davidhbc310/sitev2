---
date: 2020-05-05 09:49:00
images:
- /uploads/hbc-310-122__q9ulqa.png
layout: post
title: Portrait n°9
---
<div class="news__content smart_pictures_width">
<p>Portrait N°9-Senior garçon-Dirigeant-Laurent.   Merci à Laurent d'avoir joué le jeu et d'avoir complété lui aussi son portrait. Et bien entendu, car c'est amplement mérité, un immense merci pour son engagement sans faille au sein du club depuis plusieurs années, en tant qu'entraîneur, coach et co président en charge du secteur sportif depuis cette saison. Il aurait pu rajouter de nombreuses qualités dans son portrait, mais ce que je retiens surtout c'est son altruisme. #TeamHBC310</p> </div>