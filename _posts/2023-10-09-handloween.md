---
layout: post
date: 2023-10-09 22:00:05 +0000
title: Halloween
tags: 
- a la une
images:
- "/uploads/386255001_807179218075998_7810081181800390096_n.jpg"

---

A noter dans votre agenda.  
**Samedi 21 octobre** on se retrouve au manoir de Bréal pour faire Hand'loween et frissonner tous ensemble 👻💀🎃 !  
Merci à toute la team animation 😉 !  