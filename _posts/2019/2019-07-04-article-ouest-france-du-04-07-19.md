---
date: 2019-07-04 21:53:00
images:
- /uploads/sf__pu4va3.jpg
layout: post
title: ARTICLE OUEST FRANCE  du 04/07/19
---
<div class="news__content smart_pictures_width">
            Bréal-sous-Montfort. Handball : une saison toute en prouesse
<p> </p>
<p>L’année sportive 2018-2019 restera une grande satisfaction. L’équipe des seniors filles a terminé première de son groupe et jouera donc l’an prochain en première division départementale. L’équipe 1 des U18 garçons a obtenu de très bons résultats et évoluera à haut niveau, en pré-national en septembre. Les U15 filles, quant à elles, ont réalisé un très beau parcours en coupe du Comité 35.</p>
<p>Pour Adrien Letertre, entraîneur et salarié du club, <strong>« il est difficile de mettre en avant une équipe plutôt qu’une autre. L’état d’esprit a été très bon toute l’année et chacun a pu progresser et apporter sa pierre à l’édifice. »</strong></p>
<p><strong>Au niveau sportif, pour la troisième année consécutive, le club a été récompensé par la Fédération française de handball qui lui a attribué la médaille de bronze pour son école de handball. Le HBC 310 a aussi mis en place une école d’arbitrage et entend ainsi donner envie aux plus jeunes de s’engager et de se former à cette pratique. Enfin, le club a pu, grâce aux bénévoles, organiser tout au long de l’année, de nombreux événements : un loto, un tournoi partenaires, un tournoi interne, un match amical de niveau national 2.</strong></p>
<p>Pour clôturer cette saison, l’assemblée générale se tiendra vendredi, à partir de 19 h, au complexe Colette-Besson. Tous les membres du club sont conviés pour ce moment important dans la vie de l’association.</p>
<p> </p>
<p>Le club recrute dans toutes les catégories. Les permanences pour les inscriptions et réinscriptions ouvrent mercredi 10 juillet, de 19 h 30 à 20 h 30 à Bréal, et jeudi 11 juillet de 19 h 30 à 20 h 30, à Mordelles (salle Beauséjour).</p>
<p> </p>
<p> </p>
<p> </p>
<p><a href="https://www.ouest-france.fr/bretagne/breal-sous-montfort-35310/breal-sous-montfort-handball-une-saison-toute-en-prouesse-6429459?fbclid=IwAR1FIlW6ND41D2zqTSt21d_Ff-jlW8LbiyA6nWkWHKmigPpDHhRo44dq7gc" rel="nofollow noreferrer noopener">https://www.ouest-france.fr/bretagne/breal-sous-montfort-35310/breal-sous-montfort-handball-une-saison-toute-en-prouesse-6429459?fbclid=IwAR1FIlW6ND41D2zqTSt21d_Ff-jlW8LbiyA6nWkWHKmigPpDHhRo44dq7gc</a></p>
<p> </p> </div>