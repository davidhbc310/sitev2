---
date: 2020-08-08 19:46:00
images:
- /uploads/programme-de-reprise-17__qf7pl1.png
- /uploads/programme-de-reprise-12__qerapi.png
layout: post
title: '29 août : journée 100% HAND'
---
<div class="news__content smart_pictures_width">
<p>En parallèle du tournoi féminin que nous organisons, des créneaux ont été mis en place pour toutes les catégories jeunes, afin d'en profiter aussi. Des essais seront possibles à cette occasion. N'hésitez pas à en parler autour de vous. </p> </div>