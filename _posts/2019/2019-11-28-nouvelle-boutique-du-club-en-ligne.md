---
date: 2019-11-28 18:27:00
images:
- /uploads/infographie-match-6__q1owim.png
layout: post
title: Nouvelle boutique du club en ligne
---
<div class="news__content smart_pictures_width">
<p>Allez visiter la boutique du club avec comme nouveauté la possibilité d'acheter sa tenue de match et de la personnaliser ( numéro de 1 à 99 et prénom ou surnom ). </p>
<p>Un grand merci à David pour tout le travail réalisé et à Baptiste qui gère les commandes. </p>
<p>C'est ici : <a href="https://hbc310.ecwid.com/" rel="nofollow noreferrer noopener">https://hbc310.ecwid.com/</a></p> </div>