---
date: 2020-02-05 12:07:00
images:
- /uploads/400_5bfd60a3f0445__q586vu.png
layout: post
title: Merci à notre partenaire Lion Celtique
---
<div class="news__content smart_pictures_width">
<p>Un grand merci à notre partenaire <a href="https://www.facebook.com/LionCeltique/?__tn__=K-R-R&amp;eid=ARBPFX03Tkqot88oEtQZaSG5d-Hv0fiEKYz8VxqBHeEp1OYCXZ8Xq32Dh-z0W8VAEOkXdr9WtoD48zy4&amp;fref=mentions&amp;__xts__%5B0%5D=68.ARCaDl0Pm3bildvOAPMhJ2wDyZvMPjG9fDrPFP0qCgOEuIvvCE44Uozr-0za6AbqkU21JjsvAmKvIH-VtYlgvs1Mqg06uPVktMjKJkSrlp1KJJ1Yfid70bEmxjqT6Bc58SvQkLP5_WJOWVN5MHJMp4himBikLJtfM1GAc9SgKgQpgCSps5PiS5Q_Kpu5dh5RoQobnWq8YegyRMFDIjrF1eA3iBvsv9sLdD4nW8Ur2DkWfZj_25bB4PBZZQuX6g0VneAFhgsZOK9XrsM28ZJ-SjHs5ebPLOMqqMIoMiJrR8mkUQmFIoxU5uwesk6fVBD4b_bu" rel="nofollow noreferrer noopener">Lion Celtique Alarme et Domotique</a> pour son soutien depuis fin 2018 et sa présence sur les nouvelles tenues ( et pour son engagement au sein du club <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png" width="16"/><span>;)</span></span> ). Si vous avez besoin de mettre en place un système de sécurité dans votre entreprise ou pour protéger votre domicile, n'hésitez pas à contacter Laurent. Site internet : <a href="https://www.lion-celtique.com/?fbclid=IwAR2j796MCQpmu_Cg1TrRK8Y3hN_hOU4TvLBYF894TO2KpfUKRrH048Z_zC8" rel="nofollow noreferrer noopener" target="_blank">https://www.lion-celtique.com/</a><br/><span><span style="font-size:16px;"></span></span></p> </div>