---
layout: post
date: 2022-07-07T07:50:21.000+00:00
title: 'Offre boutique spéciale rentrée '
tags:
- a la une
images:
- "/uploads/offre_rentree2022.png"

---
Première pré commande de la tenue "collector" de rentrée à prix très intéressant grâce à notre partenaire Sport 2000 GUICHEN.

Au choix : le T SHIRT d'entraînement / d'échauffement KEMPA blanc et bleu, avec logo du club. ( version enfant 19€ ou adulte, coupe homme ou femme 21€ ).

Ou le pack complet : T SHIRT + SHORT + chaussettes ( version enfant 39 € / version adulte, coupe homme ou femme 44€ ).

Comme les délais de livraison restent approximatifs on préfère repasser au modèle pré commande avec paiement à la livraison. **Donc au moment du paiement mettez le code ZEROHBC310 ( vous ne paierez rien )**

La veste et le maillot bleu seront dispos dans la boutique en ligne.

Première phase de commande du 6 au 12 juillet ( pour livraison à la rentrée ). 

Une seconde phase sera organisée ensuite pour une commande en août si le nombre le permet ou au pire mi septembre. 

Pour pré commander : cliquez [https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree](https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree)
