---
date: 2018-05-26 10:52:00
images:
- /uploads/operationfille3__p9bvi4.jpg
layout: post
title: Le HBC310 se conjugue au féminin, en présence du SGRMH!
---
<div class="news__content smart_pictures_width">
<h2 style="text-align:center;"><span style="color:#ff00ff;font-size:36px;"><strong>Jeudi 14 Juin<br/><span style="font-size:14px;">(18h/19h30)</span><br/></strong></span></h2>
<h3><strong>Séance découverte du Handball Féminin pour les joueuses de <span style="text-decoration:underline;">2006 à 2009.</span></strong></h3>
<h3 style="text-align:center;"><span style="text-decoration:underline;font-size:24px;"><span style="color:#ff00ff;"><strong>Présence exceptionnelle des </strong></span></span></h3>
<h3 style="text-align:center;"><span style="text-decoration:underline;font-size:24px;"><span style="color:#ff00ff;"><strong>joueuses du SG-RMH </strong></span></span><span style="text-decoration:underline;font-size:24px;"><span style="color:#ff00ff;"><strong>(D2 et N2)!</strong></span></span></h3>
<p style="text-align:center;">VENEZ NOMBREUX...et NOMBREUSES!!!</p>
<p> </p> </div>