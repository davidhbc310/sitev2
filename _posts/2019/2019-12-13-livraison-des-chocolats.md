---
date: 2019-12-13 15:05:00
images:
- /uploads/i89783__q2gf5h.jpg
layout: post
title: Livraison des chocolats
---
<div class="news__content smart_pictures_width">
<span>Le chocolat est arrivé, les paquets</span> seront livrés la semaine prochaine, lors des tournois festifs parents-enfants : mercredi à partir de 18 h salle Colette Besson, vendredi à partir de 18h30 à Mordelles Beauséjour et samedi matin à partir de 10h à Mordelles Beauséjour.
Le Bureau. 
    </div>