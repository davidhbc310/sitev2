---
date: 2020-05-09 10:36:00
images:
- /uploads/hbc-310-212__qa22kk.png
layout: post
title: Portrait N°13
---
<div class="news__content smart_pictures_width">
<p>Portrait N°13-Senior garçon-Manu<br/>Merci à Emmanuel d'avoir lui aussi contribué à cette série de portraits. Vous y apprendrez la relation quasi fusionnelle d'un gardien avec ses poteaux...<br/>Et un grand merci pour son engagement au sein du club, dans les entraînements et le coaching.<br/><a href="https://www.facebook.com/hashtag/teamhbc310?__eep__=6&amp;source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARCqQIG3bK5BCkERZKbhTA1obb9VGzNVTGQ-CEo-yDL7zY1x3PTYnEMIJg5fGL7EuDOO0kyx-sZMF2bO73-Mhmn653AtrHWLhhXUXAfGsz4_frzVvkR2eJUS_p1nwaSWi5CGxYzEgEKY0tlqLDYUZettMVLs1-acBtkAy3pLsdCjiS3opVXVp2E-subFKyhgRn5llN96xyqvGHZjlapoZUlK6iUZQREYufPjr9Bu98ls33PikAfMBdCodQbDV6_N1Dqu49c2MCJw_CK0odt49FCk-3wVuKRyix4rWnOgmU8hJ7--3KAV6ZUO3XCLZ4Z4eA92ZFWeTArbfnKWW4GpSI2Udw&amp;__tn__=%2ANK-R" rel="nofollow noreferrer noopener"><span><span>#</span><span>Teamhbc310</span></span></a> <span><span> </span></span></p></div>