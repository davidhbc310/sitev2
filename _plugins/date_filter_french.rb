# coding: utf-8
module Jekyll
    module DateFilterFrench
      MONTHS = {"01" => "janvier",
                "02" => "février",
                "03" => "mars",
                "04" => "avril",
                "05" => "mai",
                "06" => "juin",
                "07" => "juillet",
                "08" => "août",
                "09" => "septembre",
                "10" => "octobre",
                "11" => "novembre",
                "12" => "décembre"
               }
  
      def date_to_french(date)
        day = time(date).strftime("%e")
        month = time(date).strftime("%m")
        year = time(date).strftime("%Y")
        day + ' ' + MONTHS[month] + ' ' 
      end

      def month_to_french(date)
        month = time(date).strftime("%m")
        (''+MONTHS[month]+'').capitalize
      end

      def date_to_french_without_year(date)
        day = time(date).strftime("%e")
        month = time(date).strftime("%m")
        day + ' ' + MONTHS[month] + ' ' 
      end

      def date_to_filterable_key(date)
        return time(date).strftime("%Q")
      end
      
    end
  end
  
  Liquid::Template.register_filter(Jekyll::DateFilterFrench)