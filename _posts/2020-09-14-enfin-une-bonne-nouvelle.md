---
date: 2020-09-14 21:09:00
images:
- /uploads/programme-de-reprise-27__qgnx7c.png
layout: post
title: Enfin une bonne nouvelle
tags: [club]
---
<div class="news__content smart_pictures_width">
<p>Dans ce contexte compliqué, entre la situation sanitaire et les problèmes ponctuels de salle, une petite éclaircie avec la barre des 200 inscrits franchie aujourd'hui. Ne tardez pas si vous voulez vous inscrire car les démarches prennent un peu de temps et les premiers matchs approchent très vite !! Ce serait dommage...On compte sur vous pour mobiliser, faire venir des jeunes, notamment des filles, pour faire un essai ! </p>
<p> </p> </div>