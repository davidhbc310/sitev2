---
layout: post
date: 2021-10-10 10:32:44 +0000
title: Packs de rentrée livraison
tags:
- a la une
- important
images:
- "/uploads/images/pages/boutique_home_texte.png"

---
Les commandes des packs rentrée sont quasiment toutes arrivées. 

Elles seront livrées aux entraînements de la semaine ou au match samedi prochain. 

Pour info : un peu de retard ( problème d'approvisionnement chez Kempa ) 

\-sur les packs adultes 

\-sur les packs avec la veste et le t shirt club en coton ( comme annoncé par mail ). 