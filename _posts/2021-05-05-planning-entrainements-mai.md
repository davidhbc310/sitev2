---
layout: post
date: 2021-05-05T19:29:31.000+00:00
title: 'Planning entraînements MAI '
tags:
- a la une
images:
- "/uploads/handball-club-310-40.png"

---
Voici les plannings d'entraînements en extérieur pour le mois de mai. 

N'oubliez pas de suivre les infos sur les réseaux sociaux, facebook ou Instagram. 

Séances en extérieur donc une annulation est possible en cas de mauvaises conditions météo.