---
date: 2019-09-01 10:28:00
images:
- /uploads/tournoi__px58v5.jpg
layout: post
title: Une journée 100% hand féminin pleinement réussie !
---
<div class="news__content smart_pictures_width">
<p>Un immense merci aux bénévoles qui ont aidé tout au long de la journée, aux membres du club qui ont apporté des gâteaux et qui sont venus assister aux rencontres. Merci aussi aux équipes et à leur staff. On souhaite une belle saison à Lanester, Granville, au CPB et au SGRMH. </p> </div>