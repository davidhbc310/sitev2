---
date: 2019-10-29 11:21:00
images:
- /uploads/7583255611196063844__q04sr9.jpg
layout: post
title: Livraison commandes boutique du club
---
<div class="news__content smart_pictures_width">
<p>Les derniers colis ont bien été livrés par notre partenaire SPORT 2000 Guichen, vos commandes seront donc distribuées lors des entraînements à la rentrée. </p>
<p>Merci à Baptiste pour tout le travail réalisé ! </p> </div>