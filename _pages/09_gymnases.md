---

layout: gymnases
title: Gymnases
description: Les gymnases utilisés par le HBC 310 pour les entraînements ou les matchs, sur les communes de Bréal-sous-Montfort, Mordelles et Saint-Thurial
subtitle: Saison 2023-2024
permalink: /gymnases/
categories: club
images:
 - /uploads/images/pages/gymnases.png
header-buttons:
 - {label: Bréal-sous-Montfort,
        link: /gymnases/#Bréal-sous-Montfort
   }
 - {label: Mordelles,
        link: /gymnases/#Mordelles
   }
 - {label: Bruz,
        link: /gymnases/#Bruz
   }
 

---
