---
layout: post
date: 2021-11-24 17:13:31 +0000
title: 'REPAS CLUB '
tags:
- a la une
- important
images:
- "/uploads/ajouter-un-titre-68.png"

---
Repas club le vendredi 21 janvier à St Thurial. 

On souhaite vous voir nombreux pour partager ce moment convivial entre licenciés, parents et anciens du club. 

Menu à 12 euros pour les adultes et à 7 euros pour les moins de 12 ans. 

Réservation obligatoire avant le 8 janvier, en déposant les flyers et le règlement lors des entraînements ou des matchs. 

Pour toute question envoyez un mail à "contact@hbc310.fr". 