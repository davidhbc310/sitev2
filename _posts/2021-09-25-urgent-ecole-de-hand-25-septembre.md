---
layout: post
date: 2021-09-25 07:12:51 +0000
title: 'URGENT ECOLE DE HAND 25 septembre '
tags:
- a la une
- important
images: []

---
Attention attention, dernière minute. 

**Les entraînements de l'école de hand samedi matin 25 septembre sont rapatriés à Bréal, salle du collège.** 

La salle Beauséjour n'est pas disponible. 