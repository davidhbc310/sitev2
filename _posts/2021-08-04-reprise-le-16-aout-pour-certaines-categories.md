---
layout: post
date: 2021-08-04 10:54:48 +0000
title: Reprise le 16 août pour certaines catégories
tags:
- reprise
images:
- "/uploads/ajouter-un-titre-18.png"

---
A noter déjà le calendrier de la première semaine de reprise avec les seniors et loisirs, les U18 et les U15. En attendant les U13 et U11 la semaine du 23.

L'école de hand reprendra en septembre après les forums.

La nouvelle réglementation impose pour les adultes de présenter un pass sanitaire ou un test PCR négatif ( en attendant la décision du conseil constitutionnel à ce sujet dans quelques jours ). On compte sur vous.