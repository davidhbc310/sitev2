---
date: 2020-07-03 10:44:00
images:
- /uploads/40__mz38z9.jpg
layout: post
title: Formulaire de pré contact
---
<div class="news__content smart_pictures_width">
<p>Vous hésitez ? Vous avez envie de faire un essai à la rentrée ? Votre enfant est complètement débutant ou confirmé ? Vous êtes dans un club voisin et vous avez envie de venir au HBC 310 dans l'une des catégories performance ? </p>
<p>Prenez contact, qu'on en discute et qu'on puisse répondre à vos questions. </p>
<p>N'hésitez pas à remplir ce formulaire QUI NE VOUS ENGAGE EN RIEN !! </p>
<p><a href="https://forms.gle/nRvgA8aAM1feT9nf7" rel="nofollow noreferrer noopener">https://forms.gle/nRvgA8aAM1feT9nf7</a></p> </div>