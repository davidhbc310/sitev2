# FAQ de gestion du contenu du site

## Comment créer une actualité ?

## Comment créer une page ?

## Comment charger une image dans le site
Les images sont à déposer dans le répertoire *uploads/images*

## Comment personnaliser une page ?
Pour toutes les pages, il est possible de personnaliser les éléments suivants :
 - Un titre
 - Un sous titre
 - Une ou plusieurs images
 - Des logos partenaires
 - Affichage ou non du pied de page *Partenaires*
 
 Tous ces éléments sont à préciser dans le bloc situé en haut d'une page (entouré par les - - - )

    title: Handball Club 310
    subtitle: Bréal-sous-Monfort, Mordelles et Saint-Thurial.
    images: ["/uploads/images/pages/welcome_image.png"]
    header-buttons:
    - {label: Les matchs du WE,
       link: /#matchsWe
    }
    - {label: Rejoignez-nous !,
       link: /equipes/
    }


## Comment personnaliser la page d'accueil ?
En plus des éléments communs à toutes les pages (titre, sous titre ...), il est possible de paramétrer les éléments suivants : 

 - Equipes à afficher


## Comment ajouter un élément dans le pied de page ?
Il n'est pas possible d'ajouter d'élément dans le pied de page. Lorsqu'un élément sera rajouté aux listes affichées (une équipe par exemple), il sera automatiquement ajouté au pied de page.

## Comment ajouter une page dans le menu ?

## Comment créer un formulaire ?
Créer un Google Form et l'insérer sur la page

## Comment ajouter une vidéo

## Comment ajouter un diaporama


# Données

## Comment créer un événement ?
Un événement est un type d'actualité particulier.
Pour créer un événement, il suffit de créer une actualité avec le tag *agenda*

## Comment créer un portrait ?
Un portrait est un type d'actualité particulier.
Pour créer un portrait, il suffit de créer une actualité avec le tag *portrait*

## Comment ajouter/modifier une équipe ?

Une équipe est définie par les élément suivants :


| Nom du champ | Description | Exemple |
|--|--|--|
|code  |Identifiant unique  | SG1 |
|title  |Nom de l'équipe  | Séniors Garçons 1|
|subtitle  |Rapide description  | Pré-Région|
|images|Identifiant unique  | [/uploads/images/pages/equipes/seniors-garcons-1__q6yxjr.jpg]|
|responsable|Nom du responsable  | Laurent|
|un contenu|Du texte présentant l'équipe  | Lorem ipsum dolor sit amet, consectetur adipisicing...| 
|pools|Identifiant unique  | voir ci-dessous

Le champ *pools* permet de spécifier une ou plusieurs compétitions concernées par l'équipe :

    - {title: +16 ANS 1ERE DIV TER MASCULINE BRETAGNE,
       code: pool65273,
       link: "https://www.ffhandball.fr/fr/competition/13619#poule-65273"
    }

| Nom du champ | Description | Exemple |
|--|--|--|
|title  |Nom de la compétition  | +16 ANS 1ERE DIV TER MASCULINE BRETAGNE|
|code|Identifiant FFHB de la coméption précédé par *pool*  | pool65273|
|link|Lien de la page de la compétition  | https://www.ffhandball.fr/fr/competition/13619#poule-65273|

## Comment récupérer les informations d'une compétition ?

 1. Se rendre sur la page [https://www.ffhandball.fr/fr/competitions/L53](https://www.ffhandball.fr/fr/competitions/L53) qui regroupe toutes les compétitions de la ligue de Bretagne.
 2. Rechercher une compétition
 3. Récupérer les informations suivantes : 
 
| Information | Emplacement | Example|
|--|--|--|
| Lien de la compétition| URL de la page courante|https://www.ffhandball.fr/fr/competition/13620#poule-65279
| Code de la page|A la fin de l'URL  | 65279
| Nom de la compétition| Le titre de la page, en rouge | +16 ANS 2E DIV TER MASCULINE BRETAGNE


## Comment ajouter/modifier un partenaire ?

## Comment ajouter/modifier une salle ?

## Comment ajouter/modifier un entraînement ?

## Comment ajouter/modifier un dirigeant ?

