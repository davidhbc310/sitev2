---
layout: post
date: 2021-02-16 12:48:43 +0000
title: 'Entraînements en extérieur '
tags: []
images: []

---
Le club propose des séances en extérieur à Mordelles ( près de Beauséjour ). 

Les séances seront annulées si les conditions météo sont mauvaises ( suivez l'info sur les réseaux ! ). 