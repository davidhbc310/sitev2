---
date: 2020-02-26 16:47:00
images:
- /uploads/les-bons-reflexes-a-adopter-dans-les-tribunes-pendant-les-matchs-de-jeunes-1__q6bfvc.png
layout: post
title: Comportement à adopter dans les tribunes pendant les matchs de vos enfants
---
<div class="news__content smart_pictures_width">
<p>Pas simple de gérer ses émotions devant ses enfants les jours de match mais c'est vraiment très important.</p>
<p><br/>Voici une petite infographie qu'on va essayer d'imprimer en grand pour les salles de Bréal et Mordelles.</p> </div>