---
layout: post
date: 2022-10-20 18:00:50 +0000
title: 'Attention modifications horaires HAND''LOWEEN '
tags:
- a la une
- important
images:
- "/uploads/orange-and-black-spooky-halloween-sale-promotion-instagram-post-1-_2022_10_20.png"

---
Attention : modification des horaires de HAND'LOWEEN ( car nous n'avons pas la salle le matin ! ). Un grand merci à toute la team animation qui a réussi à s'organiser en urgence pour repenser l'ensemble des créneaux et faire au mieux.

On espère vous voir nombreux malgré ces changements pour faire un tournoi convivial et ... horrifique à souhait !

Petite restauration sur place.

Inscriptions ici : [https://forms.gle/h2rfmGFF1N8WmFiV8](https://forms.gle/h2rfmGFF1N8WmFiV8 "https://forms.gle/h2rfmGFF1N8WmFiV8")