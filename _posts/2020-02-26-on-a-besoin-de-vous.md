---
date: 2020-02-26 17:25:00
images:
- /uploads/comment-aider-le-club-_-1__q6bhmh.png
layout: post
title: On a besoin de vous !
---
<div class="news__content smart_pictures_width">
<p>La démarche de recherche de partenaires pour le club est l'affaire de tout le monde ! Merci d'y penser, c'est très important !</p> </div>