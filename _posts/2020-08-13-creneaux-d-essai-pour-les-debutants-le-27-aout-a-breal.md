---
date: 2020-08-13 14:33:00
images:
- /uploads/programme-de-reprise-22__qf06ot.png
layout: post
title: Créneaux d'essai pour les débutants le 27 août à Bréal
tags: [agenda]
---
<div class="news__content smart_pictures_width">
<p>Tout est ( presque ) dans le titre. Des créneaux pour l'ensemble de nos catégories jeunes sont souverts le jeudi 27 août. Merci de bien prévenir à l'avance de la présence de vos enfant par mail ou téléphone comme indiqué sur l'infographie. </p>
<p> </p> </div>