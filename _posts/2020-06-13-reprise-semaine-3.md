---
date: 2020-06-13 15:44:00
images:
- /uploads/hbc-310-72__qbvgx5.png
layout: post
title: 'Reprise : semaine 3'
---
<div class="news__content smart_pictures_width">
<span>Planning de reprise de la semaine prochaine. </span>
<span>Désolé, c'est un peu tardif, on avait bon espoir de retrouver la salle Beauséjour et on a donc attendu le dernier moment ( mais ce n'est que partie remise, on ne lâche rien ). </span>
<span>On ouvre encore un tout petit plus, dans l'espoir d'avoir du monde. Par contre on a constaté plusieurs fois que des inscrits bloquent des places et ne viennent pas au dernier moment, sans prévenir. </span>
<span>Merci de penser COLLECTIF ;) ! </span>
<span> </span>
<span>Lien inscription via doodle : <a href="https://doodle.com/poll/bzhask699xf38g4r" rel="nofollow noreferrer noopener">https://doodle.com/poll/bzhask699xf38g4r</a></span>
</div>