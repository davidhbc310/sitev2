---
layout: post
date: 2022-04-26 19:37:41 +0000
title: 'Encore une invitation pour un match à Cesson ! '
tags:
- a la une
images:
- "/uploads/278132850_390546723076013_865710283295142435_n-1.jpg"

---
\[IMPORTANT\]

Nous sommes invités une nouvelle fois par CRMHB - Les Irréductibles Cessonnais pour le GROS match contre la belle équipe de Chambéry le **jeudi 5 mai à 20h30** à Cesson !

Les licenciés sont invités et donc ne paient rien, les accompagnants ont un tarif réduit à 7€. On avisera pour les modalités de paiement en fonction du nombre.

C'est ultra urgent donc réponse avant LUNDI MIDI.

Inscriptions via un doodle ce qui me facilitera la tâche : [https://forms.gle/NbFBTDiVJBQbVV4c8](https://forms.gle/NbFBTDiVJBQbVV4c8 "https://forms.gle/NbFBTDiVJBQbVV4c8")