---
date: 2020-05-19 11:07:00
images:
- /uploads/hbc-310-13__qakmoy.png
layout: post
title: Bilan des 22 portraits
---
<div class="news__content smart_pictures_width">
<span>Et oui ça fait bizarre de ne pas lire de portraits, j'ose imaginer que ça rythmait un peu votre quotidien pendant ces trois dernières semaines. En tous les cas sur FB et Insta, en nombre de vues et en interactions ça a été un beau succès et ça a permis de mieux se connaître. </span>
<span>N'hésitez pas à utiliser les surnoms, même pendant les matchs ( j'ai hâte d'entendre un "bien joué Belette intrépide" résonner dans la nouvelle HBC 310 Arena :) ! </span>
<span><span>#TeamHBC310</span></span> <span><span><span> </span></span></span></div>