---
date: 2020-09-04 15:05:00
images:
- /uploads/programme-de-reprise-28__qg4xpr.png
layout: post
title: Forums des associations
tags: [agenda]
---
<div class="news__content smart_pictures_width">
<p>Forum des associations demain, le HBC 310 se démultiplie et sera présent dans 4 communes pour répondre à vos questions. Masques obligatoires et respect des gestes barrière.</p> </div>