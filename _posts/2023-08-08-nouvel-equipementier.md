---
layout: post
date: 2023-08-08 22:00:05 +0000
title: Nouvel équipementier
tags: 
- boutique

images:
- "/uploads/366357841_768232665303987_7434663310040868316_n_erima.jpg"

---

Le HBC 310 sera désormais équipé en produits Erima avec une gamme personnalisée pour porter les couleurs du HBC 310 un peu partout : dans toutes les salles de sport de Bretagne, à la maison, à l'école, au collège, au travail, en vacances, à la Brasserie Yamas...  

Encore un tout petit peu de patience pour la boutique en ligne (en train d'être finalisée par Sport 2000) avec 20% de réduction sur les produits Erima sélectionnés.  

Nous allons aussi vous proposer des tarifs encore plus avantageux sur des offres spéciales rentrée (elles arrivent très très vite, je procède aux ultimes tests).  

Avec ERIMA France et SPORT 2000 Guichen on sera pas toujours les meilleurs sur le terrain mais on sera les plus beaux 😉 !