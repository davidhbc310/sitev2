---
layout: post
date: 2024-07-03 22:00:05 +0000
title: Inscriptions ouvertes à tous
tags:
- a la une
- inscriptions
- important
images:
- "/uploads/inscriptions2425ouvertes.jpg"
- "/uploads/infosinscriptions20242025.png"
- "/uploads/trouveunsponsor20242025.jpg"
- "/documents/prixlicences.png"
- "/uploads/inscriptions/categories2425.png"


---
Les inscriptions sont maintenant ouvertes à tous via la <a href="{{site.url}}/inscriptions/">procédure habituelle</a>.  

Pour rappel :
* Une nouvelle entente avec le club de Bruz pour les U15, U18 et Seniors
* Le nouveau <a href="{{site.url}}/entrainements/">planning d'entraînement</a> est déjà disponible (sous réserve de légers ajustements à la rentrée)  
* **Permanence N°2** : Samedi 06 juillet, 10h30-11h30, Salle Daniel Costantini à Bréal, pour vos réglements par chèques vacances, chèques ou autres ou pour toutes questions.

Un grand merci par avance aux secrétaires de la team licences qui vont suivre toute la procédure.