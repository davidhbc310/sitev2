---
date: 2020-01-05 11:15:00
images:
- /uploads/20200104_201533__q3mpt7.jpg
- /uploads/20200104_201601__q3mpte.jpg
layout: post
title: Récompense
---
<div class="news__content smart_pictures_width">
<span>L'équipe moins de 18 garçons du HBC 310 1 a été récompensée hier pour son accession en championnat prénationale lors de la cérémonie des voeux à la mairie de Bréal sous Montfort. </span><span> Nous remercions la mairie pour ce beau trophée.</span>
 


<span>Bonne reprise des entraînements à tout le monde la semaine prochaine.</span>
 


<span><span>#TeamHBC310</span></span>
</div>