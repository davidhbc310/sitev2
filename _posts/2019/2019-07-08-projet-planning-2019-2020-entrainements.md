---
date: 2019-07-08 11:57:00
images:
- /uploads/who_-1__pugt9u.png
layout: post
title: projet planning 2019-2020 entraînements
---
<div class="news__content smart_pictures_width">
<p>Planning sous réserve de quelques modifications ( à la marge ) en fonction des effectifs de rentrée en septembre. </p>
<p>Nous répondrons à vos questions éventuelles lors des permanences ou par mail : <a href="mailto:35hbc310@gmail.com">35hbc310@gmail.com</a> </p>
<p> </p> </div>