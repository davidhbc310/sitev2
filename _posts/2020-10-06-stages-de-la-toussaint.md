---
date: 2020-10-06 13:45:00
images:
- /uploads/programme-de-reprise-23__qhs3ck.png
layout: post
title: Stages de la Toussaint
tags: [club,a la une]
---
<div class="news__content smart_pictures_width">
<p>Voici les dates pour les stages de la Toussaint. Inscription obligatoire par mail. Vous recevrez ensuite la fiche d'inscription ainsi que les modalités de paiement.</p> </div>