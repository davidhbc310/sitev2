---
layout: post
date: 2022-08-16T15:27:46.000+00:00
title: 'Créneaux entraînements saison 2022 2023 '
tags:
- a la une
- important
images:
- "/uploads/bandeau-site-internet-bis-2-scaled.jpg"

---
Voici le planning 2022-2023  sous réserve de quelques modifications à la marge en fonction des effectifs.

Pour une meilleure visibilité vous le trouverez en grand format ci-dessous et en cliquant [ICI](https://hbc310.fr/uploads/planning_2022-2023.png "planning"). 

![](/uploads/planning-2020-2021.png)