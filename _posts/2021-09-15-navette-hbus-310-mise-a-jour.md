---
layout: post
date: 2021-09-15T16:17:41.000+00:00
title: 'Navette HBUS 310 mise à jour '
tags:
- écoles
images:
- "/uploads/pedibus2-1000x670.jpg"

---
Le HBUS 310 reprend du service et passera les lundi, jeudi et vendredi récupérer les enfants des écoles de Bréal vers 16h15 pour l'école publique et vers 16h20 pour l'école privée.

2 mails ont été envoyés aux parents de l'école de hand.

A noter une petite adaptation par rapport à ce qui avait été annoncé.

**Merci de vous inscrire obligatoirement :**

\-auprès du club : [https://forms.gle/SGcnnUfMLseKxSmc8](https://forms.gle/SGcnnUfMLseKxSmc8 "https://forms.gle/SGcnnUfMLseKxSmc8")

\-à la garderie

Si vous avez des questions, comme toujours, envoyez un mail à l'adresse "contact@hbc310.fr"