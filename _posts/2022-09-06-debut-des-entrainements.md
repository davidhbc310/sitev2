---
layout: post
date: 2022-09-06 08:58:00 +0000
title: 'Début des entraînements '
tags:
- a la une
images:
- "/uploads/reprise.png"

---
C'est parti officiellement pour cette nouvelle saison. 

Tous les entraînements démarrent à partir du lundi 5 septembre. 

Vous trouverez les créneaux et les lieux sur ce site dans l'onglet "club" et donc "entraînement". 

[https://hbc310.fr/entrainements/](https://hbc310.fr/entrainements/ "créneaux ")

Il est possible de venir faire un ou deux essais avant de faire sa licence. 

N'hésitez pas à prendre contact en cas de questions par mail en priorité : contact@hbc310.fr

A noter : la navette pour l'école de hand à Bréal démarrera à partir de lundi 12 septembre ( voir le message sur ce site ). 