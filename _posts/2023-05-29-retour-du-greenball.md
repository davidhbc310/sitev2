---
layout: post
date: 2023-05-29 17:00:05 +0000
title: Retour du Greenball
tags:
- a la une
images:
- "/uploads/349369689_1404953117008578_5112892972322637548_n.jpg"

---
<p>Samedi 3 juin, à Bréal (terrain de foot en herbe, près de la salle de sports du collège), journée entière conviviale de handball sur herbe. École de hand et U11 le matin. Ados et adultes l'après midi. Licenciés, parents, débutants, tout le monde est bienvenu, tournoi en mode convivialité et plaisir avant tout. 
</p>
N'hésitez pas à faire des équipes. Horaires et programmes sur l'infographie. Petite restauration et buvette sur place. 
Merci à la team technique et au pôle animation une nouvelle fois mobilisé pour cet évènement. 