---
layout: post
date: 2021-12-09 07:49:17 +0000
title: 'Ecole de hand samedi '
tags: []
images:
- "/uploads/important-2.jpg"

---
\[IMPORTANT\] école de hand samedi

**Il n'y aura pas d'entraînement pour les Lancelot et les Merlin samedi matin.**

Le club organise un plateau pour les Arthur à Bréal et la salle Beauséjour est prise par un gala de gym toute la journée.

Merci pour votre compréhension.