---
layout: post
date: 2022-09-21 18:50:25 +0000
title: 'IMPORTANT : école de HAND presque au complet'
tags: []
images:
- "/uploads/important-2.jpg"

---
\[IMPORTANT\]

Pour la première fois de son histoire, l'école de hand du HBC 310 est proche d'afficher complet ! A ce jour ( mercredi 21/09\] nous accueillons 62 mini champions dans nos trois catégories ! Les entraînements U5 et U7 à Bréal sont quasiment complets. Pour les Arthur (U9) il y a encore un tout petit peu de marge. Les créneaux sont plus chargés sur Bréal en semaine que sur Mordelles le samedi matin, donc cela nous laisse une petite solution de secours ( il y a eu encore de nombreux essais cette semaine ). Nous bloquerons donc assez rapidement pour garantir de bonnes conditions de pratique.

Merci pour votre compréhension.