---
layout: post
date: 2022-12-11 20:39:14 +0000
title: Ecole de hand samedi 17-12
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-18-_2022_12_11.png"

---
\[IMPORTANT\]

On délocalise l'école de hand samedi 17 décembre à Bréal ( salle du collège ) avec une matinée spéciale Noël avant les vacances.

Mêmes horaires que d'habitude. On compte sur vous pour le dress code avec la spécialité qui a fait notre renommée dans toute la Bretagne, le PULL DE NOËL ( et le bonnet, ce serra plus pratique pour jouer ). 

On compte sur vous ! 