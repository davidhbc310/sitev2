---
date: 2019-09-25 15:07:00
images:
- /uploads/infographie-match-5__pye1ry.png
layout: post
title: Tous à Mordelles pour soutenir les seniors en coupe de France !
---
<div class="news__content smart_pictures_width">
<p>Venez nombreux à Beauséjour samedi soir à 19h30 pour encourager nos seniors garçons qui seront opposés à HB Détente dans le cadre du premier tour de la coupe de France. On compte sur vous. </p>
<p> </p> </div>