---
layout: post
date: 2024-07-10 22:00:05 +0000
title: Dates de reprise
tags:
- a la une
images:
- "/uploads/450234879_1005661228227795_407628714776549084_n.jpg"


---
<p>Vous avez du recevoir sur les groupes whatsapp concernés un programme de prépa physique préparé par Jimmy (les seniors gars en PLS depuis 🙂).</p>
<p>Voici les dates de reprise proposées pour les équipes engagées en compétition, valables pour les collectifs départementaux et régionaux (en entente avec la JA BRUZ Handball).</p>
<p><strong>Des matchs amicaux seront positionnés</strong> fin août/début septembre, <a href="{{site.url}}/inscriptions/">il faudra donc des licences validées</a>.<p>

