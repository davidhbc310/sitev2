---
layout: post
date: 2022-11-18 16:28:03 +0000
title: 'Opération tartiflette '
tags:
- a la une
images:
- "/uploads/chartres-de-bretagne-5-_2022_11_18.png"

---
La team animation du HBC 310 vous propose des parts de tartiflette à emporter.

**7 euros la portion** ( généreuse, promis, on peut leur faire confiance ).

Précommande obligatoire via helloasso : cliquez [ICI](https://www.helloasso.com/associations/handball-club-310/boutiques/operation-tartiflette "LIEN HELLOASSO")

Date limite des commandes le 28 novembre.

**La récupération des portions et le paiement se feront à la salle des sports du collège de Bréal de 16h15 à 20h le vendredi 2 décembre.**

Vous pourrez profiter d'un bon vin chaud maison à cette occasion.

On compte sur vous pour un maximum de commandes.

_Un grand merci à l'ensemble du pôle animation pour cette belle proposition._