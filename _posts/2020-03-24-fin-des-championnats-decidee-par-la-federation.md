---
date: 2020-03-24 19:10:00
images:
- /uploads/36__mz38z9.jpg
layout: post
title: Fin des championnats décidée par la Fédération
---
<div class="news__content smart_pictures_width">
<p>Information du jour : <a href="https://www.ffhandball.fr/fr/articles/articles-publies/saison-2019-2020-arret-definitif-des-championnats-amateurs-et-annulation-de-la-coupe-de-france?fbclid=IwAR2AfvXiFpGy2eDWL_0yBpfTfbKXFxRuLheKT2e3C2u-ITTsg_Ob__1QUp0" rel="nofollow noreferrer noopener">https://www.ffhandball.fr/fr/articles/articles-publies/saison-2019-2020-arret-definitif-des-championnats-amateurs-et-annulation-de-la-coupe-de-france?fbclid=IwAR2AfvXiFpGy2eDWL_0yBpfTfbKXFxRuLheKT2e3C2u-ITTsg_Ob__1QUp0</a></p> </div>