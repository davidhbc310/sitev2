---
date: 2019-10-21 12:41:00
images:
- /uploads/infographie-match-12__pzq0ch.png
layout: post
title: 2ème tour de coupe de France-Seniors masculins
---
<div class="news__content smart_pictures_width">
<p><strong>Samedi 26 octobre à 18h30,</strong> rendez-vous au complexe sportif les Batailles à Montfort sur Meu pour encourager notre équipe seniors opposée à Brocéli'hand. On compte sur vous ! </p> </div>