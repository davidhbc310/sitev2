---
layout: post
date: 2021-09-26 17:15:01 +0000
title: 'IMPORTANT : modification horaires entraînement '
tags:
- a la une
- important
images:
- "/uploads/unnamed-1.jpg"

---
\[IMPORTANT\]

**Petits ajustements horaires à partir de demain :**

**Lundi à Mordelles Coubertin.**

Ecole de hand de 17h15 à 18h.

U11 et U13 Filles de 18h à 19h30

**Jeudi à Bréal** :

U15 G de 19h à 20h30

**Vendredi à Mordelles Beauséjour** :

U18F de 19h30 à 21h