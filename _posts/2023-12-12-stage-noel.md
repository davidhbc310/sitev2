---
layout: post
date: 2023-12-12 22:00:05 +0000
title: Stages de Noël
tags: 
- a la une
images:
- "/uploads/410159672_854531983340721_6874473107530686922_n.jpg"

---

Voici le calendrier des stages des vacances de Noël.  
Désolé pour l'annonce tardive car tout était prêt depuis longtemps... mais la communication et la validation des mairies est parfois bien longue.  
Mêmes modalités que les vacances de la Toussaint : **inscription et paiement via Helloasso**.  

- Mercredi 03 janvier de 9h à 17h : U11
- Jeudi 04 janvier de 9h à 17h : U13
- Vendredi 05 janvier de 9h à 17h : U15

    
<a href="https://www.helloasso.com/associations/handball-club-310/evenements/stages-noel-hbc-310" class="button is-dark" target="_blank">Je m'inscris au stage</a>


