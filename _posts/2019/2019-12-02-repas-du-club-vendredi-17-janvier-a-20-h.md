---
date: 2019-12-02 08:22:00
images:
- /uploads/repas-du-club-1__q1vj5o.png
layout: post
title: 'Repas du club : vendredi 17 janvier à 20 h'
tags: [agenda]
dateEvenement: 2020-01-17 20h30
---
<div class="news__content smart_pictures_width">
<p>Des flyers vont être distribués aux entraînements, on espère réunir un maximum de personnes pour un moment convivial pour une fois en dehors des terrains. Tous les volontaires pour donner un coup de main sont aussi les bienvenus et peuvent se manifester auprès des coachs ou d'Adrien ou bien envoyer un mail à "isabellehbc310@gmail.com". </p> </div>