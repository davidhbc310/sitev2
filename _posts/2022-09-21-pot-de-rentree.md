---
layout: post
date: 2022-09-21 18:49:13 +0000
title: 'POT DE RENTRÉE '
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-4-_2022_09_21.png"

---
RAPPEL : **Pot de rentrée vendredi 23 septembre à 19h30 à Bréal, salle du collège.**

Occasion de passer un moment convivial avant le début de saison.

N'hésitez pas à apporter les vêtements de hand trop petits de vos enfants pour une fripe party 310 ( achat / échange...).

On compte sur vous nombreux.