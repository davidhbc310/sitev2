---
date: 2020-06-23 11:46:00
images:
- /uploads/telechargement__qcdhtq.jpg
layout: post
title: Certificat médical obligatoire
---
<div class="news__content smart_pictures_width">
<p>On va ouvrir de façon imminente les inscriptions ( dernière phase de tests de la plateforme en ligne ). </p>
<p><strong>N'oubliez pas que le certificat médical sera obligatoire pour TOUT le monde. </strong></p>
<p>Donc anticipez et prenez rendez-vous chez votre médecin. </p>
<p>1-Voici le questionnaire personnel post période de COVID à compléter et à remettre à votre médecin : <a href="http://staff.clubeo.com/uploads/hbc310/Medias/FFHandball_Questionnaire_covid_mai2020_inscrip.pdf" rel="nofollow noreferrer noopener">FFHandball_Questionnaire_covid_mai2020_inscrip.pdf</a></p>
<p>2-Voici le certificat médical à imprimer et à faire remplir par votre médecin, soyez bien vigilants sur le cachet lisible lorsqu'il vous remet le document après la visite. </p>
<p><a href="http://staff.clubeo.com/uploads/hbc310/Medias/2020-21_certificat_medical.pdf" rel="nofollow noreferrer noopener">2020-21_certificat_medical.pdf</a></p>
<p>Ce certificat sera à scanner ou à prendre en photo avec votre téléphone ( de bonne qualité ) pour faire votre licence. </p>
<p> </p> </div>