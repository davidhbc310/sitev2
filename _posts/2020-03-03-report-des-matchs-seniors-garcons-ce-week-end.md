---
date: 2020-03-03 15:33:00
images:
- /uploads/31__mz38z9.jpg
layout: post
title: Report des matchs seniors garçons ce week-end
---
<div class="news__content smart_pictures_width">
<p>Suite à l'arrêté préfectorale du Morbihan, les matchs des seniors garçons 2 à Séné et des seniors garçons 2 à Mordelles contre Noyal Muzillac sont reportés.</p> </div>