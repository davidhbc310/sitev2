---
layout: post
date: 2024-01-21 21:00:05 +0000
title: Match à Saint-Grégoire
tags: 
- a la une
images:
- "/uploads/421848766_884146277045958_7835921320761382142_n.jpg"

---

Pour une fois qu'on va pas voir Chambéry, mais ça reste quand même une ville de montagnes !  
On se mobilise pour encourager les Roses **samedi 10 février** et passer une belle soirée.  

Ça va venir vite donc comme toujours on va montrer que le HBC 310 sait se mobiliser aussi pour le Hand féminin : 
on compte sur vous pour diffuser au maximum et inviter la famille, les amis, les voisins et motiver dans chaque équipe pour qu’on se retrouve tous ensemble à passer encore un super moment de handball !  

**Inscription via helloasso** (sans paiement, il sera à effectuer sur place) et merci de bien arriver pour 19h30 pour bien réguler les flux.  

<a href="https://www.helloasso.com/associations/handball-club-310/evenements/match-sgrmh-contre-clermont-10-fevrier?fbclid=IwAR05sPd5pukB7GeZQ_a8EsvNd8MCtmacBGfYfA9OS0M7EpJSDSGK32boqeE" class="button is-dark" target="_blank">Je m'inscris</a>


