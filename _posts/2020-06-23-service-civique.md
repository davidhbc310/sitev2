---
date: 2020-06-23 16:10:00
images:
- /uploads/hbc-310-110__qcdu1d.png
layout: post
title: Service civique
---
<div class="news__content smart_pictures_width">
<p>Nous aimerions accueillir un jeune en service civique la saison prochaine, si vous connaissez un jeune entre 16 et 25 motivé à l'idée de nous rejoindre n'hésitez pas à lui en parler. La fiche de missions est en cours d'écriture. </p>
<p>Pas de besoin d'être licencié au club et même pas besoin de faire du hand. </p>
<p>Mail : <a href="mailto:ronanhbc310@gmail.com">ronanhbc310@gmail.com</a> </p> </div>