---
layout: post
date: 2022-10-12 15:06:56 +0000
title: 'Stages handball TOUSSAINT '
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-34-_2022_10_13.png"

---
Stages vacances de la Toussaint.

Merci de bien vouloir inscrire vos enfants pour faciliter l'organisation et la préparation.

Pour toute information envoyez directement un mail à Fodé "fodehbc310@gmail.com".

Formulaire d'inscription : [https://forms.gle/jPzddBXqqBCgXzgK6](https://forms.gle/jPzddBXqqBCgXzgK6 "https://forms.gle/jPzddBXqqBCgXzgK6")