---
date: 2020-09-30 17:58:00
images:
- /uploads/jaune-et-blanc-avec-photo-protection-de-l-environnement-evenement-affiche__qhhb24.png
layout: post
title: IMPORTANT
tags: [club,important]
---
<div class="news__content smart_pictures_width">
<span>On va insister comme tous les ans. Vous y aurez le droit tous les vendredis ;) ! </span>
<span>Encore plus cette année avec le plaisir de retrouver enfin les terrains. L'image du club est l'affaire de chacun d'entre nous. Il est impératif de gérer vos émotions et de respecter l'adversaire et l'arbitrage. On compte sur vous dès samedi. </span>
</div>