---
layout: post
date: 2022-10-23 17:37:44 +0000
title: 'Récupération des places pour Cesson '
tags:
- a la une
- important
images:
- "/uploads/310665617_531273412333248_7562530142530758703_n_2022_10_11.jpg"

---
MATCH de CESSON mercredi.   
Le chiffre a gonflé et nous serons donc 156 dans les tribunes !   
On avise pour récupérer les places à Cesson qui seront imprimées lundi après-midi.   
On propose **un créneau de récupération à Bréal** ( au foyer en haut de la salle ) MARDI  soir de 20h à 20h45. Merci de privilégier le retrait à cette occasion ( quitte à vous arranger entre amis, voisins...).   
Si vous ne pouvez pas vous déplacer, on verra directement sur place mais, j'ai aussi envie de profiter de ma soirée donc idéalement assez tôt de 19h30 à 19h45 ( entrée principale la plus à droite, j'aurai la veste du club pour ne pas que certains me confondent avec un joueur de Cesson car j'ai un peu la même carrure !! ). 

Si les personnes inscrites à la dernières minutes pouvaient prévoir un chèque à l'avance à l'ordre du HBC310 ce serait plus pratique ). 

  
PS : vous pouvez aussi venir sur ce créneau de mardi récupérer les packs HBC 310 ( je m'excuse d'avoir tardé un peu pour la livraison, c'est un peu compliqué pour moi ces dernières semaines ).