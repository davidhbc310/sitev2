---
date: 2020-02-05 12:09:00
images:
- /uploads/84145859_1523189854502985_6806546076280029184_o2__q586zd.jpg
layout: post
title: Commande de T SHIRT HBC 310
tags: [boutique]
---
<div class="news__content smart_pictures_width">
<p>Bonne nouvelle !</p>
<p>Face au succès de l'opération T SHIRT HBC 310 <a href="https://www.facebook.com/sport2000guichen/?__tn__=K-R&amp;eid=ARDHuXTvWXCiFBRU-sd2W3K2tjZFZnJXy8pTPOmkWJ9Wil8WqdLjCoZ8_4okI_ilnqWciykwKHcbJ9ns&amp;fref=mentions&amp;__xts__%5B0%5D=68.ARBydTrQpJDIUimSXcNEE_hghbCu20Xdpck5y7sbwzEYyXJnFcxh1Vf881_9GUEY1_xDBEKEmm0EHBOoKd2Brznsmh07l6GJ0HhPAsyuxchdLP8vE8PDT5z5kj7tt1psWugZFKAe3RecKsVJGUa4JpLWLJ-FI6kGjW76BeWZZWJETkSP2PRAm9DY_7ggiRAdHXSzFxFQFxeUrcQC3qj1U3GBD2VIH1y6ahQY4FhRq5hrjTSRRRdmcEFpBPRAQfBIPvNCH0bTQkhJzy70jXnJZL6hFDl7-cd9YPSgXszmWg39unHCF8PpPWZbU7fxiWeCQTOZ0dH_t73YNI4tw7ieia0vow" rel="nofollow noreferrer noopener">SPORT 2000 Guichen</a> pour l'école de hand, nous avons décidé de généraliser la proposition à l'ensemble du club et à toutes les catégories. Les coachs vont vous proposer le même modèle en bleu royal, avec le logo du club, le logo sport 2000 et le flocage HBC 310 dans le dos, à prix coûtant ( 7 euros pour les enfants jusqu'au 12-14 ans / 10 euros pour les tailles adultes du XS au 4XL avec coupe homme ou femme ).</p>
<p><br/> A savoir que les <strong>T SHIRT enfants mixtes taillent grands</strong> ( on en a quelques uns en stock ), les T SHIRT homme aussi taillent grands, en revanche les coupes femmes taillent plus petits et sont plus cintrées.</p>
<p>Ce sont des T SHIRT en coton, pratiques comme tenue d'échauffement par exemple.</p>
<p>Après les supporters, les parents, grands parents peuvent aussi en commander <img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/f57/1/16/1f609.png" width="16"/>;) pour mettre l'ambiance en tribune ! Merci de faire vite, pour les avoir le plus rapidement possible ( avant les vacances, car il y a un délai de fabrication de trois semaines ). On compte sur vous. <strong>DATE LIMITE VENDREDI 14 février </strong></p>
<p>Pré commande via les coachs ou par mail ronanhbc310@gmail.com.</p>
<p><br/> PS : par rapport au visuel initial je vais juste réduire un peu la proportion du logo et le texte HBC 310 pour l'espacer un peu plus ).</p>
<p> </p> </div>