---
date: 2020-02-08 10:29:00
images:
- /uploads/infographie-match-62__q5dmcy.png
layout: post
title: STAGE HBC 310 des vacances de février
---
<div class="news__content smart_pictures_width">
<p>Vous avez dû recevoir un mail avec le calendrier des stages, tout est dans l'infographie.</p>
<p><strong>Rappel : vous devez prévenir Adrien de la présence de votre enfant avant le 14 février ! ( aux entraînements ou par mail : <a href="mailto:adrienhbc310@gmail.com">adrienhbc310@gmail.com</a> ) </strong></p>
<p>Voici les fiches d'inscription : </p>
<p>Stages OSCOR à St Gilles pour les moins de 15 : <a href="{{site.url}}/uploads/pdf/U15StGilles.pdf" rel="nofollow noreferrer noopener">Handball U15, inscription stage Février 2020 à St Gilles.pdf</a></p>
<p>Stages HBC 310 : <a href="{{site.url}}/uploads/pdf/HANDBALL%20STAGES%20FEVRIER.pdf" rel="nofollow noreferrer noopener">HANDBALL_STAGES_FEVRIER.pdf</a></p>
<p> </p> </div>