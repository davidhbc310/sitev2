---
date: 2020-06-25 09:31:00
images:
- /uploads/35__mz38z9.jpg
layout: post
title: 'Info : mise au point'
---
<div class="news__content smart_pictures_width">
<p>Petite mise au point : vous lisez partout que la pratique du hand est à nouveau autorisée. On espérait pouvoir vous proposer un dernier créneau en salle pour terminer la saison de belle manière. Mais chaque mairie décide ou pas de réouvrir, l'urgence sanitaire s'arrête seulement le 10 juillet !<br/>Le complexe de Bréal ne réouvrira pas. Beauséjour à Mordelles réouvre partiellement mais avec des conditions d'utilisation compliquées et des créneaux pas forcément liés aux disponibilités d'Adrien. Il y a un décalage entre les décisions ministérielles et leur application à l'échelle locale.<br/>On proposera donc une dernière session la semaine prochaine en extérieur, avec ballons et sans limite de nombre.<br/>Ce sera aussi une belle occasion de dire au revoir et merci à Adrien ( qu'on remerciera de manière plus "organisée" à l'AG en tout début de saison prochaine ). Plus d'infos à venir très vite...ça se bouscule un peu en ce moment <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png" width="16"/><span>;)</span></span> !</p> </div>