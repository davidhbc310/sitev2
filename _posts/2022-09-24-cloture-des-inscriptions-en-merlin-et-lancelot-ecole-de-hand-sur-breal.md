---
layout: post
date: 2022-09-24 08:40:11 +0000
title: 'Clôture des inscriptions en Merlin et Lancelot sur Bréal '
tags:
- important
- a la une
images:
- "/uploads/important-2.jpg"

---
IMPORTANT : comme évoqué il y a quelques jours, les inscriptions en U5 ( Merlin ) et U7 ( Lancelot ) sont clôturées sur Bréal.   
Si vous voulez inscrire votre enfant sur l'entraînement à Mordelles le samedi matin, c'est encore possible mais ça passera par une demande par mail uniquement via contact@hbc310.fr  
Il reste encore quelques places en U9.   
Pour des raisons de sécurité et pour garantir l'épanouissement des pratiquants ( et des entraîneurs ! ) nous ne pouvons pas trop charger les effectifs des séances ! 

Merci pour votre compréhension. 