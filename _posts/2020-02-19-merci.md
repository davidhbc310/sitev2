---
date: 2020-02-19 09:44:00
images:
- /uploads/47079584_1164894130332867_4904282181590843392_o__q5xxmc.jpg
layout: post
title: MERCI
---
<div class="news__content smart_pictures_width">
<p>Le club remercie la boulangerie <a href="https://www.facebook.com/La-petite-fourn%C3%A9e-Mordelles-226078030881153/?__tn__=K-R&amp;eid=ARCfLGwd8dxIsQ5FcI8S1KV60VXgItAPQEzJG_G0EfUnHz7GHz2lDBUziiypSz9lV36Z2jjXY0wrAcle&amp;fref=mentions&amp;__xts__%5B0%5D=68.ARDnnR3TbetbgasQt3k8RW_ZB0u4nQOy4Vsowv-dTFEzOBRZkhk7lvQfCNltyZqPnb47I3WEk9OUj9ooNui-dx5N0cVBa_DDTCoh40PcK1Sz_doH2Ecf25b3jOo2iwlfMDVYDwr8NEOVxpyvjDwxiXGOQ0_lzKdwpj9YKNJQ6uRJA9ceIv8NYFMxF0_BZrYmaxN_MiigXArPbtu2rfHNRxxcFRDSCM5WLyokdoVi5Q0iQxAYfclxCIbYojm1_I4UPs1LNh_XGCUVTPyWct5pTazk1w1cvrVnBzLiSSDIErXcVkoIG2eFdqNpsVV4uaprw7jXyIAJslsnVdeSICvC8iokSw" rel="nofollow noreferrer noopener">La petite fournée. Mordelles</a> pour son soutien depuis janvier 2019.</p>
<p><br/>Le logo de la "petite fournée" est présent sur la manche de nos maillots, elle a donc contribué directement à l'équipement de l'ensemble de nos équipes.</p>
<p><br/>Encore un grand merci au non de tout le HBC 310 ! </p> </div>