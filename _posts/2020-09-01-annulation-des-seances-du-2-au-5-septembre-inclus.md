---
date: 2020-09-01 19:45:00
images:
- /uploads/important__qfzqn6.png
layout: post
title: Annulation des séances du 2 au 5 septembre inclus
---
<div class="news__content smart_pictures_width">
            Nous avons appris en début d'après midi, que le gymnase Coubertin à Mordelles était réquisitionné dès ce mercredi pour préparer le Forum des associations se déroulant Samedi 5 septembre.
Donc nous n'avons pas d'autre choix d'annuler TOUS les entraînements de cette semaine ( mercredi, jeudi et vendredi inclus ). Reprise la semaine prochaine avec les plannings disponibles sur le site du club ( en haut saison 2020-2021 ).
Petite particularité liée au retard de la construction du gymnase de Bréal, les séances du Lundi seront en extérieur, jusqu'à fin septembre.
Veuillez nous excuser pour ces multiples messages aux informations contradictoires, nous subissons cette situation comme vous.    </div>