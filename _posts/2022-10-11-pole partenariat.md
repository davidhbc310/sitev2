---
layout: post
date: 2022-10-11 21:15:21 +0000
title: ''
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-32-_2022_10_11.png"

---
\[IMPORTANT\]

A toutes les personnes qui veulent aider ( même en étant peu disponibles ) rendez-vous **mercredi 19 octobre à 20h à la salle de Bréa**l pour constituer le pôle partenariat.

On a vraiment besoin de vous ! C'est très important pour le club. 