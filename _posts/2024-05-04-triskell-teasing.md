---
layout: post
date: 2024-05-04 22:00:05 +0000
title: Triskell Challenge
tags: 
- a la une
images:
- "/uploads/435762137_939070138220238_5329020768826641038_n.png"

---

1er juin chargé (donc grand besoin de bénévoles) avec toute la journée à Bréal, salle Daniel Costantini et salle Colette Besson la 2è édition du TRISKELL CHALLENGE, un **tournoi U13 de haut niveau** !  

Au programme un beau plateau masculin et féminin avec **16 équipes au total venues de tout le Grand-Ouest**.  
Un peu de teasing pour les participants du tournoi, encore un peu de patience...  

Et après hop on enchaîne avec la [Bréal Beer Fest](https://hbc310.fr/breal-beer-fest/), elle est pas belle la vie ?  

