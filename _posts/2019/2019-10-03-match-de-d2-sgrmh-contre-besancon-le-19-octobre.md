---
date: 2019-10-03 18:52:00
images:
- /uploads/teal-and-yellow-stripes-father-s-day-facebook-post__pz3x7x.png
layout: post
title: Match de D2 SGRMH contre Besançon le 19 octobre
tags: [agenda]
dateEvenement: 2019-10-19 20h30
---
<div class="news__content smart_pictures_width">
<p><strong><em>Logiquement vous avez du recevoir le mail suivant : </em></strong></p>
<p>Le 19 octobre, à 20h30, le Saint Grégoire Rennes métropole handball reçoit Besançon, dans le cadre du championnat de D2 féminine. </p>
A cette occasion, nous bénéficions de conditions privilégiées. Les plus de 18 ans ne paieront que 3 euros ( au lieu de 7 euros + gratuit pour les mineurs ), une tribune nous sera réservée et le HBC 310 sera mis à l'honneur. 

Nos plus jeunes licenciés auront la chance de faire le protocole de début de match en accompagnant les joueuses sur le terrain. On privilégie à cette occasion les catégories moins de 9 ( Lancelot de l'école de hand ), les moins de 11 mixtes et les moins de 13 filles éventuellement. 
 
Il est impératif de prévenir ( assez vite ) de votre présence : nombre d'adultes / d'enfants et nombre d'enfants à vouloir faire le protocole ( prévoir short-chaussures, ils auront le maillot du club ). Le rendez-vous sera à 19h30 devant l'entrée principale de la Ricoquais. Privilégiez le covoiturage et parlez-en aux entraînements ! 
 
Merci de répondre par mail ( <a href="mailto:ronanhbc310@gmail.com" rel="noreferrer noopener" target="_blank">ronanhbc310@gmail.com</a> ) ou directement via le formulaire suivant : <a href="https://forms.gle/oCuUEqJYVKAj2uxe6" rel="nofollow noreferrer noopener" target="_blank">https://forms.gle/oCuUEqJYVKAj2uxe6</a>.
 
<strong>On compte sur vous NOMBREUX !! </strong>
 
 
 
    </div>