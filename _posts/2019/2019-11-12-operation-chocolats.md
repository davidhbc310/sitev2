---
date: 2019-11-12 18:43:00
images:
- /uploads/4266671765231729269__q0val1.jpg
layout: post
title: Opération chocolats
---
<div class="news__content smart_pictures_width">
<p>N'oubliez pas de rapporter le bon de commande de chocolats de Noël complété au plus vite ! Vous pouvez le déposer aux entraînements en le donnant directement à Adrien. Merci d'avance.</p>
<p>Si vous avez des questions envoyez un mail à adrienhbc310@gmail.com</p> </div>