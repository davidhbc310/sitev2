---
layout: page
title: Inscriptions
subtitle: Saison 2024-2025
permalink: "/inscriptions/"
categories: club
description: Pour la saison 2024-2025, rejoignez le Handball Club 310 - Inscription
  et paiement en ligne
header-buttons:
- label: Documents
  link: "/inscriptions/#documents"
- label: Tarifs
  link: "/inscriptions/#tarifs"

images:
 - /uploads/images/pages/inscriptions2024.png

---






<div class="content">
  <h4 id="procedure" name="procedure">Procédure d'inscription</h4>
  <br>
  <ul class="steps has-content-centered">
    <li class="steps-segment is-active" style="list-style: none;">
      <span class="steps-marker">1</span>
      <div class="steps-content">
        <p class="is-size-4 has-text-primary">Formulaire Hello Asso</p>
        <p class="underline-p"><a href="{{site.url}}/inscriptions/formulaire/">C'est par ici</a></p> 
      </div>
    </li>
    <li class="steps-segment" style="list-style: none;">
      <span class="steps-marker">2</span>
      <div class="steps-content">
        <p class="is-size-4 has-text-primary">Paiement</p>
        <p>En ligne, ou par chèque, liquide ou chèques vacances</p>
      </div>
    </li>
    <li class="steps-segment " style="list-style: none;">
      <span class="steps-marker">3</span>
      <div class="steps-content">
        <p class="is-size-4 has-text-primary">Formulaire de la fédération</p>
        <p>Un lien sera envoyé par la fédération. Remplir et renseigner les différents documents</p>
      </div>
    </li>
    <li class="steps-segment" style="list-style: none;">
      <span class="steps-marker">4</span>
      <div class="steps-content">
        <p class="is-size-4 has-text-primary">Validation</p>
        <p>Vérification des documents puis validation de la licence</p>
      </div>
    </li>
  </ul>
</div>

<div class="columns is-multiline is-mobile">
    <div class="column is-8-desktop is-12-mobile">
        <div class="card card-bloc-home" >
            <div class="card-content has-dark-effect"  style="height: 100%">
                <p class="title is-4 has-text-white ">Attention</p>
                <p class="subtitle has-text-white">Pour que votre place soit validée, votre paiement doit être réalisé !</p>
                <div class="">
                    <a href="{{site.url}}/uploads/infosinscriptions20242025.png" target="blank" class="button is-light">Plus d'informations</a>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="content">
  <h4 style="margin-bottom: 0px">Catégories</h4>
  <div class="columns is-multiline">
      <div class="column is-4-desktop is-10-tablet is-12-touch">
          <img src="{{site.url}}/uploads/inscriptions/categories2425.png">
      </div>
  </div>
</div>

<br>
<div class="content">
  <h4 id="tarifs" name="tarifs" style="margin-bottom: 0px">Tarifs</h4>
  <div class="columns is-multiline">
      <div class="column is-4-desktop is-10-tablet is-12-touch">
          <img src="{{site.url}}/documents/prixlicences.png">
      </div>
      <div class="column is-4-desktop is-10-tablet is-12-touch">
          <img src="{{site.url}}/documents/reducs20242025.png">
      </div>
      <div class="column is-4-desktop is-10-tablet is-12-touch">
          <img src="{{site.url}}/uploads/trouveunsponsor20242025.jpg">
      </div>
  </div>
</div>




<br>   
<div class="content">
  <h4 id="documents" name="documents">Documents utiles</h4>
  <br>
  <a class="button is-primary is-outlined" href="{{site.url}}/documents/certificat_medical.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Certificat médical</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/attestation_questionnaire_sante_mineurs.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Questionnaire de santé - Mineurs</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/attestation_questionnaire_sante_majeurs.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Questionnaire de santé - Majeurs</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/attestation_honorabilite_dirigeants_encadrants.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Attestation d'honorabilité</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/autorisation_parentale.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Autorisation parentale</span>
</a>


</div>


<div class="content">
<br>
  <h4 >Questions et contacts</h4>
  <p>
    Si vous avez des doutes, des questions, des problèmes :
    envoyez un mail à contact@hbc310.fr.
  </p>
</div>

