---
date: 2020-09-14 12:37:00
images:
- /uploads/ordre-du-jour-ca-hbc310-1__qgn9hy.png
layout: post
title: Assemblée générale
tags: [agenda,club]
---
<div class="news__content smart_pictures_width">
<p>Les convocations ont été envoyées par mail, elles concernent les licenciés 2019-2020. </p>
<p><strong>L'Assemblée se tiendra à l'espace culturel Brocéliande salle Viviane à 20h30 le lundi 28 septembre. </strong></p> </div>