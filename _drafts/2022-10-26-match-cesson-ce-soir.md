---
layout: post
date: 2022-10-26 10:40:05 +0000
title: 'Match CESSON ce soir '
tags:
- a la une
- important
images:
- "/uploads/310665617_531273412333248_7562530142530758703_n_2022_10_11.jpg"

---
\[IMPORTANT\]

Petit rappel pour le match à Cesson :

\-3500 places vendues, donc ce sera quasi plein ! Pensez à venir tôt pour ne pas galérer ( ça peut bouchonner fort, c'est un vrai goulet d'étranglement avec les ronds points et les travaux ).

\-j'ai envoyé un mail à toutes les personnes qui n'ont pas pu venir hier soir prendre leurs billets, merci de bien venir entre 19h30 et 19h45 file la plus à droite de l'entrée principale ( proche de la route ). J'y serai même un peu plus tôt.

\-ATTENTION ATTENTION : vous n'avez des billets réservés QUE si vous avez bien reçu un mail de validation de HELLOASSO ! ( un "ticket virtuel" de confirmation ). Dans le cas contraire désolé mais ça n'a pas été pris en compte et je ne peux plus rien faire, j'ai tout tenté mais l'espace club est complet. En cas de doute envoyez moi un mail ou un MP ( ronanhbc310@gmail.com ).

\-je compte sur vous pour donner de la voix et continuer à prouver que le HBC310 sait mettre le feu dans les tribunes.