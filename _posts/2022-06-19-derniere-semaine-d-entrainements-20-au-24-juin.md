---
layout: post
date: 2022-06-19 17:37:50 +0000
title: Dernière semaine d'entraînements 20 au 24 juin
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-13.png"

---
Dernière semaine d'entraînement, avec un petit mix équipes actuelles / équipes de la saison prochaine proposé par Fodé.

J'ai essayé d'être clair, logiquement les infos ont été diffusées par les groupes Whatsapp. Si vous avez des questions, les adresses mails sont sur l'infographie !

Les loisirs ne sont pas concernés par cette annonce et continuent donc les mardi ( Mordelles Coubertin ) et mercredi ( Bréal ) de 20h30 à 22h. 
