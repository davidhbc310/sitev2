---
layout: team
code: U15G_REGION
title: U15 Garçons - Groupe Région
coach: 
categorie: jeunes
ages: "en 2010 et 2011"
sexe: G
#widget-prochain-match : 5f8c6afbe7a7213ebd51fd1e
#widget-classement : 5f8c66cda3ff7e0a24e838d4
description: L'équipe 1 masculine des moins de 15 ans du Handball Club 310 regroupe les joueurs nés en 2010 et 2011 et évolue en championnat régional
competitions:
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/u15-masculin-territoriale-25565/poule-156081",
        file: saison-2024-2025-20_regional_u15-masculin-territoriale-25565_poule-156081,
        equipe: 1
    }


   
---

