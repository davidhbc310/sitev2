---
date: 2020-04-05T16:22:00.000+00:00
images:
- "/uploads/ajouter-un-titre__q8bjya.png"
- "/uploads/ajouter-un-titre-3__q8bjyb.png"
- "/uploads/ajouter-un-titre-2__q8bjyc.png"
- "/uploads/ajouter-un-titre-1__q8bjyc.png"
- "/uploads/ajouter-un-titre-5__q8ogr5.png"
layout: post
title: On garde le contact sur les réseaux sociaux !

---
<div class="news__content smart_pictures_width">
<p>La communication du club est plus active sur les réseaux sociaux en cette période de confinement, pour garder le contact ( avec une dose d'humour ) n'hésitez pas à nous rejoindre sur : </p>
<p>-la page Facebook : <a href="https://www.facebook.com/hbc310" rel="nofollow noreferrer noopener">https://www.facebook.com/</a></p>
<p>-le compte Instagram : <a href="https://www.instagram.com/handballclub310/" rel="nofollow noreferrer noopener">https://www.instagram.com/handballclub310/</a></p>
<p>En photo quelques fausses annonces de fin de confinement mises en avant sur les réseaux ! </p> </div>