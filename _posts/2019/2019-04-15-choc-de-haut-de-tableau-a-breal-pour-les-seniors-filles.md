---
date: 2019-04-15 17:20:00
images:
- /uploads/copie-de-copie-de-rightontutorialservices-2__pq0dae.png
layout: post
title: Choc de haut de tableau à Bréal pour les seniors filles
---
<div class="news__content smart_pictures_width">
<p>Venez en NOMBRE et mobilisez autour de vous pour le match des seniors filles <strong>dimanche 28 avril à 14h à Bréa</strong>l, le HBC 310 sera opposé à l'ASC Rennais, match capital pour la montée en 1ère division ! </p> </div>