---
layout: post
date: 2022-09-02T12:57:11.000+00:00
title: 'Navette école de HAND à Bréal '
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-17.png"

---
Fodé viendra chercher les inscrits à la navette à partir du **lundi 12 septembre** pour les entraînements de l'école de hand du lundi, du jeudi et du vendredi à 16h45.

ATTENTION : double inscription OBLIGATOIRE _( mise à jour du 9 septembre )_

* via le site de la mairie : 

  En page d'accueil du portail famille sur le site de la mairie, dans le menu contacter, faites une "demande de contact" et rédigez un mail indiquant : NOM-Prénom du responsable légal / nom et prénom de l'enfant / école et classe de l'enfant / jour + horaire de l'activité +handball club 310.
* sur le formulaire du club en cliquant [ICI ](https://docs.google.com/forms/d/e/1FAIpQLSfy7cuSxg2vR7Ca4m54WPRH-7-KRgUEYbEogIC-YeU3u1WPLA/viewform?vc=0&c=0&w=1&flr=0 "HBUS 310")

Pour toute question, comme d'habitude : contact@hbc310.fr