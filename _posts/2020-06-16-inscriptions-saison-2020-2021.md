---
date: 2020-06-16 22:08:00
images:
- /uploads/hbc-310-92__qc1bxu.png
layout: post
title: Inscriptions saison 2020 2021
---
<div class="news__content smart_pictures_width">
<p>Je commence à recevoir pas mal de mails au sujet des inscriptions. Pas d'inquiétude, on a un peu de retard, mais c'est à cause de la situation des dernières semaines ( mise en place des conseils municipaux tardive, fonctionnement au ralenti des mairies ). Encore un tout petit peu de patience, de notre côté tout est bouclé, il ne reste que la validation des créneaux d'entraînement par les mairies que nous attendons ( <strong>avec impatience</strong> ! ). </p> </div>