---
date: 2020-08-27 10:15:00
images:
- /uploads/samedi-29-aout-tournoi-amical-de-hand-feminin-salle-beausejour-a-mordelles-7__qfpqxx.png
layout: post
title: Besoin de bénévoles samedi !
---
<div class="news__content smart_pictures_width">
<span>Samedi 29 août : tournoi amical de haut niveau à Mordelles Beauséjour. </span>
<span>Répartition des bénévoles dans la journée, en guise d'ultime rappel n'hésitez pas à vous inscrire même sur une plage horaire limitée. On compte aussi sur vous pour faire des gâteaux ! </span>
<span>MERCI </span>
<span><span><a href="https://doodle.com/poll/tcsgk2zw9ri24rtx" rel="nofollow noreferrer noopener">https://doodle.com/poll/tcsgk2zw9ri24rtx</a></span></span>
</div>