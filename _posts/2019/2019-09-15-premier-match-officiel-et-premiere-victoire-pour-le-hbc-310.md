---
date: 2019-09-15 09:10:00
images:
- /uploads/70767023_1362065047282134_5536414244150444032_n__pxv2lk.jpg
layout: post
title: Premier match officiel et première victoire pour le HBC 310 !
---
<div class="news__content smart_pictures_width">
<p>Un grand bravo aux moins de 18 garçons 1 en déplacement à Châteaulin qui ont remporté la victoire 33 à 26 contre Entente Aulne Porzay avec l'inauguration des nouveaux maillots.</p>
<p>Une première victoire en prénationale qui en appelle d'autres pour les équipes du club la semaine prochaine..</p>
<p>La saison est parfaitement lancée. </p>
<p>#TeamHBC310</p> </div>