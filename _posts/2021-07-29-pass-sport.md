---
layout: post
date: 2021-07-29 15:28:52 +0000
title: 'PASS SPORT '
tags:
- important
images:
- "/uploads/ajouter-un-titre-16.png"

---
Mise à jour du processus d'inscription avec le PASS SPORT. Certains ont reçu leur courrier, nous faisons bien partie du dispositif. 

Voici donc en infographie la marche à suivre dans les deux cas de figure qui peuvent se présenter.

[https://hbc310.fr/inscriptions/](https://hbc310.fr/inscriptions/ "https://hbc310.fr/inscriptions/")

Soit création soit renouvellement en fonction de votre situation. 

Puis vous indiquez bien 00HBC310 ( zéro zéro étant les 2 premiers signes ) ce qui vous évite de payer et vous permet de vous pré inscrire. 

Ensuite il faudra venir aux permanences avec votre justificatif et le reste à charge ( en 1 ou 3 chèques ). 