---
date: 2020-02-21 16:15:00
images:
- /uploads/responsable-de-salle-2__q6251v.png
layout: post
title: Très important
---
<div class="news__content smart_pictures_width">
<p><span><span>⚠️</span></span> Très important <span><span>⚠️</span></span></p>
<p>Pour assurer la bonne tenue des prochains matchs de championnat à domicile, il est impératif d'avoir un nombre d'adultes volontaires ( et avec une licence ! ) suffisants pour assumer les responsabilités OBLIGATOIRES imposées par la Ligue dès la catégorie moins de 11.<br/>C'est de plus en plus compliqué à gérer car nous jouons quasiment tous les samedis dans deux salles à Bréal et Mordelles.<br/>Le document joint explique les principales missions à assurer.<span><br/>Merci aux licencié.e.s majeur.e.s ( compétition ou loisirs ) de compléter le doodle (<br/><a href="https://doodle.com/poll/kq3esz7f537abpbq?fbclid=IwAR16aRSX48MBJpkgu0Syy9fEtApbfw3gRA8iYP6BhVzFfygL2ubR1k5ZEpU" rel="nofollow noreferrer noopener" target="_blank">https://doodle.com/poll/kq3esz7f537abpbq</a> ) pour assurer une présence de 2 à 3 heures ( l'équivalent de 2 matchs ) d'ici la fin de l'année en fonction de vos disponibilités en priorité pour les postes de responsable de salle.<br/>Les bonnes volontés qui bouchent les trous tous les weekends commencent à fatiguer et en cas de manquement le club sera pénalisé.<br/>Merci d'en prendre conscience.</span></p> </div>