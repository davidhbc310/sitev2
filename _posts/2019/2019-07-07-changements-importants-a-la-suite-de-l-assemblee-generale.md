---
date: 2019-07-07 15:46:00
images:
- /uploads/7835607-important-blue-grunge-stamp__pu9y9y.jpg
layout: post
title: Changements importants à la suite de l'Assemblée générale
---
<div class="news__content smart_pictures_width">
<p>Un grand merci aux membres du HBC 310 présents à l'Assemblée générale et au repas convivial de vendredi. Un grand merci aussi aux bénévoles pour l'organisation et <span>toute la logistique. </span></p>
<p><span>Comme annoncé par mail il y a quelques semaines, le club change de gouvernance avec les démissions du président Eric Sauvourel, du vice président Mehdi Rabin et du trésorier Ivan Le Goff. </span></p>
<p><span>Ils ont été chaleureusement remerciés à la fin de l'Assemblée générale pour leur engagement très important et leurs belles réussites au sein du club. </span></p>
<p><span><br/>Un nouveau bureau directeur va donc prendre le relais avec une co-présidence : Laurent Le Flecher co-président responsable du pôle sportif, Isabelle Kerzreho co-présidente responsable du pôle événementiel, Ronan Céron co-président responsable du pôle communication. </span></p>
<p><span><br/>Ils seront assistés de deux nouveaux trésoriers ( Marc et Laurent ) et d'un pôle secrétariat réorganisé. </span></p>
<p><span><br/>N'hésitez pas à vous faire connaître si vous souhaitez intégrer le conseil d'administration du club. </span></p> </div>