---
date: 2019-03-13 08:52:00
images:
- /uploads/17__poaoji.png
layout: post
title: ENSEMBLE AVEC NOS EQUIPES SENIORS FEMININES ET MASCULINES
---
<div class="news__content smart_pictures_width">
<h2 style="text-align:center;">Mobilisation générale du club dimanche après-midi à Bréal pour encourager nos équipes seniors.</h2>
<p> </p>
<p> </p>
<p>A 14 heures les seniors filles reçoivent Pontorson et veulent confirmer leur première place au classement.</p>
<p> </p>
<p><br/>A 16 heures  l'équipe des seniors garçons 1 ( 2ème ) affrontent St Avé-Meucon (1er à égalité avec deux autres équipes ).</p>
<p> </p>
<p style="text-align:center;">L'une des forces du HBC 310 est son public nombreux, fidèle...et bruyant !</p>
<p style="text-align:center;">mobilisez au maximum autour de vous pour porter nos équipes vers la victoire. <br/> On compte sur vous. <br/><a href="https://www.facebook.com/hashtag/hbc310?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBb-nPkPzGvCol9J7G7HccZPCoQJ9zPzVsDBVYQmbjcLryE-M9R9uROO7jgEXQmdekHzrJZFehxWWYs_lTsb4rn8WFl5TpIrHp52-TzXcWI08OWQE5wNuBV8ED3ei-dJr45eKRvenOHJZ4_zgNzy1q5ji98ZEpjqPv8L3lPEBgrl4k4dOz6qDqITdJNibStnPtgydLQWXKpPigY7C6tpu_jgrwLEtX8aqwiD3vEZwP3JR3qIZg4CnRGx5_RleOcZ40EGj4PC0gHVLko98PfE29FKtE4S-vMGnyXk2MEUGi-1w9JmtZM0d-3z3HBWxFoe6ZCyjocQJx7AZQVhaVaGDgIaQ&amp;__tn__=%2ANK-R" rel="nofollow noreferrer noopener"><span><span>#</span><span>HBC310</span></span></a></p>
</div>