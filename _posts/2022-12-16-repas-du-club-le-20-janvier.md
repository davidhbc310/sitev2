---
layout: post
date: 2022-12-16 15:30:07 +0000
title: 'Repas du club le 20 janvier '
tags:
- a la une
images:
- "/uploads/img-20221216-wa0004_2022_12_16.jpg"

---
\[REPAS DU CLUB\]

A noter dans vos agendas, le repas club aura lieu vendredi 20 janvier à St Thurial.

Rougail saucisse et galette des rois.

Un moment convivial pour se retrouver tous ensemble. On compte sur vous nombreux, merci de réserver assez tôt car les places sont limitées.

12 euros pour les + de 12 ans et 7 euros pour les moins de 12 ans.

Réservation et paiement via helloasso : [CLIQUEZ ICI](https://www.helloasso.com/associations/handball-club-310/boutiques/repas-club-hbc-310 "lien helloasso")