---
layout: post
date: 2023-09-11 22:00:05 +0000
title: Pot de rentrée
tags: 
- a la une
images:
- "/uploads/376408902_790412279752692_3088086814547519265_n.jpg"

---

**Vendredi 15 septembre** à partir de **19h30**, les licenciés et leurs parents sont conviés à un moment convivial pour vous présenter les membres du Bureau, le staff technique, les grandes lignes de la saison.  
On compte sur vous pour bien démarrer l'année sportive !