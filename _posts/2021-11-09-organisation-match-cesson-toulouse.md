---
layout: post
date: 2021-11-09 07:23:20 +0000
title: 'Organisation match CESSON Toulouse '
tags: []
images:
- "/uploads/telechargement.png"

---
Les places seront à récupérer le jour même à la GLAZ ARENA. 

Il y aura une file spécifique et je donnerai les billets réservés à cette occasion. 

Merci de bien venir en AVANCE, **entre 18h45 et 19h30,** avec le pass sanitaire et l'appoint pour les billets accompagnateurs à 5 euros. 

Attention à la circulation toujours très dense sur la rocade un vendredi en fin d'après midi !

On distribuera des maillots du club dans la salle pour être bien visibles. 

Nous serons 208 du HBC 310 pour encourager Cesson !