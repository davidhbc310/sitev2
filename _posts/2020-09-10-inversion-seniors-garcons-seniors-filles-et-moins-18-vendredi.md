---
date: 2020-09-10 21:22:00
images:
- /uploads/programme-de-reprise-282__qgossq.png
layout: post
title: Inversion seniors garçons / seniors filles ( et moins 18 ) vendredi
---
<div class="news__content smart_pictures_width">
<span>Petite inversion entre les garçons et filles le vendredi soir ( à la demande générale ). </span>
<span>On ajuste le planning pour la semaine prochaine et on le communique au plus vite, on a trouvé quelques solutions, on attend encore quelques réponses pour faire au mieux. Pas de gros chamboulements à prévoir, juste quelques créneaux en moins pour certaines catégories...le temps que tout rentre dans l'ordre avec la nouvelle salle. </span> </div>