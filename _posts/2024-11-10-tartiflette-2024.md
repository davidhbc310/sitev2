---
layout: post
date: 2024-11-10 22:00:05 +0000
title: Opération Tartiflette 2024
tags: 
- a la une
images:
- "/uploads/466644948_1107865071340743_2156224823515364337_n.jpg"

---

Et ouiiiii, le retour de notre plat à emporter fétiche.  
On vous propose une excellente tartiflette à déguster sans modération, **8 euros la portion**, **à récupérer la salle Daniel Costantini de Bréal le vendredi 29 novembre de 16h30 à 20h**.
Lors du retrait vous pourrez profiter d'un bon vin chaud maison.  
N'hésitez à partager autour de vous pour faire de cette vente une belle réussite.  
Paiement via helloasso (possibilité de décocher la participation à l'appli).

⚠️Vous pouvez tout à fait mettre à zéro la participation HELLOASSO, n'oubliez pas de décocher.⚠️
   
<a href="https://www.helloasso.com/associations/handball-club-310/boutiques/operation-tartiflette-2024" class="button is-dark" target="_blank">Je commande</a>
