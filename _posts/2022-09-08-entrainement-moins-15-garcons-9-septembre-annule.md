---
layout: post
date: 2022-09-08 19:15:50 +0000
title: entraînement moins 15 garçons 9 septembre annulé
tags: []
images: []

---
IMPORTANT : la salle Beauséjour étant exceptionnellement indisponible vendredi 9 septembre, il n'y aura pas entraînement pour les moins de 15 garçons à Mordelles. 

Désolé pour le désagrément indépendant de notre volonté. 

Les autres catégories ne sont pas impactées