---
layout: post
date: 2021-08-21 06:51:09 +0000
title: Challenge des Hermines 28 et 29 août
tags:
- a la une
images:
- "/uploads/st-gregoire-rennes-metropole-handball-n2-12.png"

---
Le HBC 310 organise pour la 3è année consécutive un tournoi amical féminin de préparation de haut niveau à Mordelles, complexe Beauséjour.

**Samedi 28 août :**

\-9h30-10h45 : entraînement École de Hand et U11

\-11h : MATCH 1 / CPB Rennes ( N2 ) contre CL Colombelles ( N1 )

\-14h-15h45 : entraînement U13 + U15

\-16h30 : MATCH 2 / Saint Grégoire Rennes métropole handball ( N1 ) / CL Colombelles ( N1 )

**Dimanche 29 août :**

15h : MATCH 3 / CPB Rennes ( N2 ) / SGRMH ( N1 )

On compte sur vous dans les tribunes, l'entrée est gratuite ( accès avec pass sanitaire pour les adultes et masque ).

A cette occasion, nous avons besoin d'aide, vous pouvez apporter un gâteau ou des crêpes qui seront vendus, et si vous pouvez aussi aider au rangement, à l'accueil, même une heure, ça permettrait à tout le monde d'en profiter.

Merci de vous manifester par mail : "contact@hbc310.fr"