---
layout: post
date: 2022-10-11 07:07:45 +0000
title: Invitation match CESSON / CHARTRES à la Glaz le 26 octobre
tags:
- a la une
- important
images:
- "/uploads/310665617_531273412333248_7562530142530758703_n_2022_10_11.jpg"

---
Mercredi 26 octobre-20h30

Le HBC 310 est invité à la Glaz Arena pour le match Cesson / Chartres métropole handball.

Profitez des vacances pour venir en famille !

Gratuit pour les licenciés du club et 7 euros pour les accompagnateurs. Afin de faciliter la gestion, places à prendre sur Helloasso. Les modalités de récupération des billets seront précisées un peu plus tard en fonction du nombre.

On compte sur vous !!

[https://www.helloasso.com/associations/handball-club-310/evenements/match-cesson-chartres-metropole-handball](https://www.helloasso.com/associations/handball-club-310/evenements/match-cesson-chartres-metropole-handball "https://www.helloasso.com/associations/handball-club-310/evenements/match-cesson-chartres-metropole-handball")