---
layout: team
code: HANDFIT
title: Handfit
categorie: loisirs
coach: 
ages: "en 2006 et avant"
sexe: M
need-help: false
hide_competitions_stats: true
#widget-prochain-match : 5f8c6b5ba748730a18a3caea
description: "Le Handball Club 310 ouvre cette année une séance Handfit. Au programme : réveil musculaire, petits jeux pour stimuler la proprioréception et solliciter la coordination gestuelle, un peu de cardio et des phases de jeux adaptées."


---

