---
date: 2019-07-18 08:30:00
images:
- /uploads/st-gregoire-rennes-metropole-handball-_-n2__pwqz5q.png
layout: post
title: 'Samedi 31 août : journée 100% HAND à Mordelles'
---
<div class="news__content smart_pictures_width">
<p>A noter dans votre agenda ! On vous attend nombreux toute la journée du samedi 31 août à Mordelles, l'entrée sera gratuite avec de belles rencontres en perspective. </p>
<p>Animation pour nos jeunes handballeurs de l'école de hand de 13h à 14h15 ! </p>
<p> </p>
<p>N'hésitez pas à vous proposer pour donner un coup de main et si possible merci de faire un gâteau qui sera vendu lors de cette journée. </p>
<p> </p> </div>