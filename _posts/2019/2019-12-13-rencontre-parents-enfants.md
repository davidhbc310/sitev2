---
date: 2019-12-13 15:11:00
images:
- /uploads/35__mz38z9.jpg
layout: post
title: Rencontre parents enfants
---
<div class="news__content smart_pictures_width">
<p>Vous avez reçu un mail vous présentant l'organisation de ces rencontres et des annulations d'entraînements de certaines catégories : </p>
<p>mercredi 18 : rencontre parents enfants des catégories moins de 11 et moins de 13, rendez-vous à partir de 18h salle colette Besson à Bréal, tournoi de 18h30 à 20h30 suivi d'un moment de convivialité ( chacun apporte à boire et à manger ). </p>
<p>-vendredi 20 : rencontre parents enfants des catégories moins de 15 et moins de 18, rendez-vous à partir de 18h30 salle Beauséjour à Mordelles, tournoi de 19h à 21h30 suivi d'un moment de convivialité ( chacun apporte à boire et à manger ). </p>
<p>-samedi 21 : rencontre parents enfants de l'école de hand, rendez-vous à partir de 10h salle Beauséjour à Mordelles, tournoi de 10h30 à 12h30 suivi d'un moment de convivialité ( chacun apporte à boire et à manger ). </p>
<p> </p>
<p>Ce temps placé sous le signe de la "détente" est une bonne occasion de faire mieux connaissance avec les coachs, entraîneurs et familles des partenaires de vos enfants. Rapprochez vous des coachs pour les détails d'organisation</p>
<p>On compte sur vous nombreux !</p> </div>