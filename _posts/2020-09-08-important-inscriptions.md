---
date: 2020-09-08 13:56:00
images:
- /uploads/25__mz38z9.jpg
layout: post
title: 'IMPORTANT : inscriptions'
tags: [club]
---
<div class="news__content smart_pictures_width">
            Si la reprise des matchs officiels est encore un peu tardive ( début octobre ), <strong>l'engagement des équipes jeunes est en cours et doit se faire très très rapidement. ( nous sommes dans les derniers délais !! )</strong>
 
Il est donc vraiment important d'accélérer le processus afin de permettre à vos enfants de participer aux brassages.
On doit avoir une vision globale du nombre d'équipes à engager, on a encore quelques doutes.
 
On compte sur vous.
 
Rappel : module d'inscription en ligne <a href="https://www.helloasso.com/associations/handball-club-310/adhesions/inscription-handball-club-310-saison-2020-2021-2?fbclid=IwAR2OHn5OchhhuECpaZcSRPMg6dIhILOH8Q-XSXQyIG9kRP2yG7AaxbtKrEw" rel="nofollow noreferrer noopener" target="_blank">https://www.helloasso.com/associations/handball-club-310/adhesions/inscription-handball-club-310-saison-2020-2021-2</a> </div>