---
date: 2020-05-19 14:17:00
images:
- /uploads/hbc-310-9__qakvhk.png
layout: post
title: Formulaire pré contact recrutement saison 2020-2021
---
<div class="news__content smart_pictures_width">
<p>N'hésitez pas à entrer en contact si vous voulez intégrer une équipe du club ( dans n'importe quelle équipe et catégorie ). On vous présentera le club, les objectifs, les lieux d'entraînement...</p>
<p><strong>Ne concerne que les NOUVEAUX ( débutants, mutations d'autres clubs, reprise...). </strong></p>
<p>MERCI de diffuser, de partager dans le monde entier...</p>
<p>Cliquer sur le lien : <a href="https://forms.gle/rKysi68n4GErsgYAA" rel="nofollow noreferrer noopener">ICI</a></p> </div>