---
layout: post
date: 2023-06-28 17:00:05 +0000
title: Assemblée générale
tags:
- a la une
images:
- "/uploads/assemblee_generale_2023.jpg"

---
<p>Assemblée générale du club.</p>
<p>Une occasion de faire le bilan de l'année, de se projeter sur la suivante et de passer un moment convivial pour clôturer la saison.
On espère vous voir nombreux et on souhaiterait voir du monde intégrer le CA pour avoir une meilleure représentation de l'ensemble des catégories du club dans l'instance principale de l'association. N'hésitez pas à candidater à ce sujet par mail ou par MP.</p> 
<p>Une procuration a été jointe dans le mail d'invitation à l'AG en cas d'absence. 
A la fin de l'AG, apéritif offert par le club puis chacun apporte de quoi grignoter. 
</p>
