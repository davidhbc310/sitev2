---
date: 2020-07-03 10:18:00
images:
- /uploads/st-gregoire-rennes-metropole-handball-_-n22__qcvwe5.png
layout: post
title: 'A noter dans votre agenda : 29 août TOURNOI de haut niveau'
---
<div class="news__content smart_pictures_width">
<p>Comme l'an dernier le club a la chance de pouvoir accueillir 4 équipes de haut niveau à Beauséjour avec trois équipes de N1, SGRMH-2, BREST BH-2 et Colombelles ainsi que le CPB qui évolue en N2. </p>
<p>Une belle occasion de se retrouver autour du terrain et dans les tribunes. </p>
<p>Nous aurons besoin de bénévoles, un doodle va être diffusé à cette occasion. Des animations seront organisées : entraînements, initiation. On compte sur vous !! </p> </div>