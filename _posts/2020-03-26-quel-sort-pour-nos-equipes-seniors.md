---
date: 2020-03-26 09:50:00
images:
- /uploads/90528266_2565525877051033_6822980214587392000_n__q7slwe.jpg
layout: post
title: Quel sort pour nos équipes seniors ?
---
<div class="news__content smart_pictures_width">
<p>La Ligue travaille sur les montées et descentes de tous les championnats jusqu'en N3. Dans la logique des annonces fédérales avec finalement une montée et une descente ?<br/>On vous tiendra bien sûr au courant.</p>
<p><br/>Bon courage à la Ligue de Bretagne pour tout le travail à venir et pour ce sacré casse-tête !</p>
<p>Pour les équipes seniors du HBC 310 :</p>
<p>-seniors garçons 1 : maintien<br/>-seniors garçons 2 : avant dernier donc en attente de la décision fédérale pour savoir si maintien ou descente. ( petit espoir de maintien malgré tout )<br/>-seniors filles : logiquement descente MAIS groupe de 9 et un match en moins par rapport au 8ème que les filles avaient battu à l'aller. ( très très faible espoir )</p> </div>