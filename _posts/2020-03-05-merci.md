---
date: 2020-03-05 19:08:00
images:
- /uploads/48011_8022_1583309680_rect_350-220-ffffff___q6qfpy.jpg
layout: post
title: Merci
---
<div class="news__content smart_pictures_width">
<p>Un grand merci à <a href="https://www.facebook.com/emycars/?__tn__=K-R&amp;eid=ARCBWUvWsDuybNEYJiZyKZB6AeVvME3Di6Qo6SEQzfqQ-sTFZtADOj7-NqhaYeGNP6X6ZOV8iNBhS4jL&amp;fref=mentions&amp;__xts__%5B0%5D=68.ARCH2P-fe57ZN46aR5BIHbEDOCwXnrvCEsHmW__O-X6pQEU23N6f2pCHXoRN4tLnYln_Q5rerhAYPkhK82h_tRKP35le2SgbNWEtiITCBxtsa0EPjz6uFGkf67k6tbgAlxsiqFiHwhYaflVnSd6Z9GgDTJN1nz0Av7oidcmaVZhMVdkp_ZlX_y-A-GzIHtMwcQUv-1LAGjjVnHxGmB9zU97q009HJE1aVAtgN4R3aaWL3JXgv-l-xHTi_Sg_c_WwLkMt1ifALFA0CVvGbftk3YI3dt4v-S9n6c_31t1NlYfH6fAxo1wTCWpL3--RVlrTIMkuJRwXe2no1xoTgAGdkyZ-pg" rel="nofollow noreferrer noopener">Emycars</a> qui est partenaire du club depuis janvier 2019.</p>
<p><br/>Si vous avez un projet d'achat de véhicule d'occasion vous pouvez vous rendre rue du Hindré à Bréal.</p>
<p><br/>Visitez le site internet : <a href="https://www.emycars.fr/pro/emycars-8022/?fbclid=IwAR3wZrQheh9DGD_NhHqjikVtJXXhPMqluVD7nyyKRTGhWxfSD9Fli70_LxA" rel="nofollow noreferrer noopener" target="_blank">https://www.emycars.fr/pro/emycars-8022/…</a></p>
<p>Emycars a permis la réalisation des nouvelles tenues avec une présence de leur logo sur la manche. Encore MERCI.</p>
</div>