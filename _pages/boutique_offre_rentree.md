---

layout: page
title: Offre de rentrée
subtitle: Saison 2023-2024
permalink: /boutique/offre-rentree/
description: Pour la saison 2023-2024, la boutique du HBC 310 et son partenaire Sport 2000 vous proposent ces packs à tarif préférentiel.
widget-helloasso: https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree-hbc-310-saison-2023-2024-erima-commande-3/widget
formulaire-helloasso: https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree-hbc-310-saison-2023-2024-erima-commande-3
header-buttons:
 - {
    label: Procedure de commande,
    external-link: "/uploads/images/boutique/procedure_offre_rentree.png"
   }
 - {
    label: Guide des tailles Erima,
   external-link: "/uploads/images/boutique/guide_taille_erima.png"
   }
images:
- /uploads/images/boutique/visuel_offre_rentree.png
ariane-sub-elements:
- {title: Boutique,
    url: /boutique/}
---
Les pré commandes sont ouvertes sur [HELLOASSO]({{page.formulaire-helloasso}}){:target="_blank"} ou via le [formulaire en bas de page](/boutique/offre-rentree/#formulaire) avec la même [procédure](/uploads/images/boutique/procedure_offre_rentree.png) que l'an dernier.  
Des offres promo avantageuses : 
 - 1 pack entraînement composé d'un short et d'un maillot personnalisé avec le logo du HBC 310. (version enfant, version homme et version femme).
 - la "veste club" adulte ou enfant avec le logo du HBC 310, 
 - 2 packs accessoires (sac à dos idéal pour l'école de hand et gourde ou sac de sport et gourde). Logo sur les sacs. 

<br>
<div class="container">
    <div class="columns is-multiline is-mobile ">
            <div class="column is-4-tablet is-10-mobile ">
                <div class="box ">
                    <div class="has-text-centered">
                           <img src="{{site.url}}/uploads/images/boutique/article1.png" loading="lazy">
                    </div>
                </div>
            </div>
            <div class="column  is-4-tablet is-10-mobile ">
                <div class="box ">
                    <div class="has-text-centered">
                           <img src="{{site.url}}/uploads/images/boutique/article2.png" loading="lazy">
                    </div>
                </div>
            </div>
            <div class="column is-4-tablet is-10-mobile ">
                <div class="box ">
                    <div class="has-text-centered">
                           <img src="{{site.url}}/uploads/images/boutique/article3.png" loading="lazy">
                    </div>
                </div>
            </div>
            <div class="column  is-4-tablet is-10-mobile ">
                <div class="box ">
                    <div class="has-text-centered">
                           <img src="{{site.url}}/uploads/images/boutique/article4.png" loading="lazy">
                    </div>
                </div>
            </div>
    </div>
</div>

<br><br>
### Formulaire de commande


<span id="formulaire"></span>
 {% include widget-helloasso.html  url=page.widget-helloasso %}