---
layout: team
code: SG
title: Seniors Garçons
categorie: adultes
coach: 
ages: "en 2006 et avant"
sexe: G
#widget-prochain-match : 5f8c63695f90c83bc756efde
#widget-classement : 5f2eec615fb8545636100548
description: L'équipe Senior 1 masculine du Handball Club 310 évolue au plus haut niveau départemental d'Ille et Vilaine et vise l'accession en Région pour l'année prochaine.
header-buttons:
    - {label: Equipe 1,
        link: /equipes/seniorsgarcons/#equipe1
    }
    - {label: Equipe 2,
        link: /equipes/seniorsgarcons/#equipe2
    }
competitions:
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/16-ans-1ere-division-territoriale-masculine-25546/poule-147226",
        file: saison-2024-2025-20_regional_16-ans-1ere-division-territoriale-masculine-25546_poule-147226,
        equipe: 1
    }
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/16-ans-2eme-division-territoriale-mas-p-10-26148/poule-149909",
        file: saison-2024-2025-20_regional_16-ans-2eme-division-territoriale-mas-p-10-26148_poule-149909,
        equipe: 2
    }




---




