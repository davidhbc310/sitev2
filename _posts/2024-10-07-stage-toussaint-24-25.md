---
layout: post
date: 2024-10-07 22:00:05 +0000
title: Stages Toussaint 2024
tags: 
- a la une
images:
- "/uploads/462188753_1075758741218043_6766156105553272216_n.jpg"

---

Bonne nouvelle pour les vacances de la Toussaint, on propose **2 jours de stage** pour les U11 / U13 et U15, salle D. Costantini :  
 - **U11** : Mardi 22 et Mercredi 23 Octobre, de 9h à 17h30.
 - **U13** : Mercredi 23 et Jeudi 24 Octobre, de 9h à 17h30.
 - **U15** : Jeudi 24 et Vendredi 25 Octobre, de 9h à 17h30.

⚠️Vous pouvez tout à fait mettre à zéro la participation HELLOASSO, n'oubliez pas de décocher.⚠️
   
<a href="https://www.helloasso.com/associations/handball-club-310/evenements/stages-toussaint-hbc-310-2024-25" class="button is-dark" target="_blank">Je m'inscris au stage</a>
