---
date: 2020-04-28 08:53:00
images:
- /uploads/hbc-310__q9hkh6.png
layout: post
title: Portrait de joueur-2
---
<div class="news__content smart_pictures_width">
<span>Portrait N°2-Senior garçon. TEDDY.</span>
<span>Notre très expérimenté "TED", roi de la passe dans le dos et du tir laser. </span>
<span>Merci d'avoir joué le jeu. </span>
<span><span>#TeamHBC310</span></span> <span><span><span> </span></span></span></div>