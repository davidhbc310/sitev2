---
layout: post
date: 2021-05-18T06:50:55.000+00:00
title: Opération Fête des Pères
tags: []
images:
- "/uploads/fetePeres.png"
formulaire-helloasso: https://www.helloasso.com/associations/handball-club-310/evenements/operation-fete-des-peres-du-hbc-310/widget

---
Toujours dans l'idée de mettre en avant nos partenaires, nous vous proposons deux coffrets cadeau : un lot de 3 bières artisanales à 12 euros et un lot de deux bonnes bouteilles de vin ( un rouge et un blanc ) à 25 euros. 

Les produits sont sélectionnés par les meilleurs cavistes du monde : la Cavavin Mordelles.
Inscription obligatoire sur Helloasso pour gérer la logistique via le formulaire ci-dessous ou directement sur le site <a href="https://www.helloasso.com/associations/handball-club-310/evenements/operation-fete-des-peres-du-hbc-310">helloasso.com</a>

Paiement en ligne directement ou aux entraînements ( dans ce cas mettre PAYDIRECT en code promo ). Vous pourrez revenir récupérer vos bouteilles le vendredi 18 à Bréal en fin d'après-midi et le samedi à Mordelles. Les quantités ne sont pas limitées, vous pouvez partager autour de vous et en faire profiter vos amis !

On compte sur vous !