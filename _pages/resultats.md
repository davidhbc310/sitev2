---
title: Derniers résultats
subtitle: Saison 2023-2024
description: Retrouvez les résultats des derniers matchs des équipes du Handball Club 310 pour la saison 2021-2022  !
permalink: /competitions/resultats/
layout: resultats
images:
 - /uploads/images/pages/resultats.png
filters-buttons:
 - {label: equipes-compet
   }
header-buttons:
 - {label: Classements,
   link: "/competitions/classements/"
   }
ariane-sub-elements:
- {title: Compétitions,
    url: /competitions/}
categories:
 - {code: jeunes,
     libelle: Jeunes
   }
 - {code: adultes,
    libelle: Adultes
 }
 - {code: loisirs,
    libelle: Loisirs
 }

---





