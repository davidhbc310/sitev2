---
layout: post
date: 2020-12-17T07:34:53.000+00:00
title: Stages des vacances de Noël
tags:
- a la une
images:
- "/uploads/programme-de-reprise-8.png"

---
Le HBC 310 propose des stages pendant les vacances de Noël. Tous les créneaux sont positionnés dans la nouvelle salle de Bréal ( sauf les moins de 11 filles qui sont aussi à Bréal mais dans la salle Colette Besson ). Merci de bien venir à l'heure car la mairie a mis en place un système de fermeture automatique ( et il est donc impossible d'entrer sans badge ! ). Ces stages sont gratuits, mais pour avoir une idée des effectifs, l'inscription est obligatoire via un formulaire en ligne : [https://forms.gle/r9zJzbUTioPqgcfQ6](https://forms.gle/r9zJzbUTioPqgcfQ6 "https://forms.gle/r9zJzbUTioPqgcfQ6")

Bien entendu respect strict des consignes sanitaires en vigueur : rester à l'entrée de la salle en attendant que l'entraîneur vienne chercher le groupe, masque obligatoire, gel à l'entrée, pas de vestiaire donc venir en tenue avec sa gourde remplie.