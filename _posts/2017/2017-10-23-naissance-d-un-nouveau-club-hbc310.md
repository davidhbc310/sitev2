---
date: 2017-10-23 17:16:00
images:
- /uploads/retenu-logo_mbsthb_421_resized__oybvey.jpg
layout: post
title: 'Naissance d''un nouveau club : HBC310'
---
<div class="news__content smart_pictures_width">
<p>Bienvenue sur le nouveau site officiel du club <strong>35 HBC310</strong> </p>
<p>Cette nouvelle structure rassemble deux anciens clubs : l'USM-Handball et le BSTHB.</p>
<p>Fort de plus de 260 licenciers venant de près de 40 communes, souhaitons une longue vie et un avenir radieux au HBC310!</p> </div>