---
date: 2019-03-24 11:54:00
images:
- /uploads/tournoiloisir__povaba.jpg
layout: post
title: Les loisirs sont partis chasser le Sanglier!
---
<div class="news__content smart_pictures_width">
<p>Participation réussie pour les loisirs au tournoi organisé par le club de Liffré!</p>
<p>Belle prestation tant dans la tenue de match que dans le niveau de plaisir développé lors des rencontres!</p>
<p>Bravo à la section loisir.</p> </div>