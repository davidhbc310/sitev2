---
date: 2018-06-27 10:06:00
images:
- /uploads/merci_greenball_2018__paz2ib.png
layout: post
title: Merci à vous!
---
<div class="news__content smart_pictures_width">
<p>Le HBC310 remercie chaleureusement tous ses partenaires, les bénévoles et participants de l'édition 2018 du Greenball! A bientôt pour de nouvelles aventures sportives :)<br/>  <br/>    </p> </div>