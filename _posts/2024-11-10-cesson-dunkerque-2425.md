---
layout: post
date: 2024-11-10 22:00:05 +0000
title: On retourne à Cesson !
tags: 
- a la une
images:
- "/uploads/467231246_1109355221191728_7039558183317317837_n.jpg"

---

On retourne à Cesson...et pour un gros match contre USDK Dunkerque Handball Grand Littoral **le dimanche 8 décembre à 16h**.  
Logiquement toutes les équipes sont dispos, tout le monde joue samedi, les SG1 jouent à Auray à 11h le dimanche donc ça peut le faire aussi. 
Comme d'habitude, 100% gratuit pour les licenciés (mais inscription obligatoire) et 7 euros pour les accompagnants.  

De mon côté je fais au mieux pour regrouper tout le monde et c'est un GROS CHANTIER surtout avec la numérotation très chaotique de la GLAZ. Merci par avance pour votre compréhension.  

**ATTENTION car helloasso n'est qu'une première étape, il faut récupérer les VRAIS BILLETS ensuite !!**.  
**Permanences le vendredi 06 de 18h30 à 20h** au foyer de la salle de Bréal **et samedi 7 à Bréal** (précisions un peu plus tard mais il y a beaucoup de matchs donc ça devrait être assez simple).  
   
<a href="https://www.helloasso.com/associations/handball-club-310/evenements/match-cesson-dunkerque-8-decembre-2024" class="button is-dark" target="_blank">Je réserve</a>
