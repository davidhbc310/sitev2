---
layout: post
date: 2023-03-15T18:17:09.000+00:00
title: 'Opération couscous '
tags:
- a la une
images:
- "/uploads/unnamed_2023_03_15.jpg"

---
RAPPEL : avec le retour du soleil, le pôle animation vous propose une vente de parts de couscous royal avec l'objectif de faire encore mieux que la tartiflette. Mêmes modalités, retrait à la salle de Bréal le vendredi 31 mars selon des créneaux que vous choisissez entre 16h30 et 20h. On compte sur vous pour la diffuser au maximum !

Paiement directement en ligne ( si vous préférez payer sur place : mettez le code ZEROHBC310 )

[https://www.helloasso.com/associations/handball-club-310/boutiques/operation-couscous](https://www.helloasso.com/associations/handball-club-310/boutiques/operation-couscous "https://www.helloasso.com/associations/handball-club-310/boutiques/operation-couscous")