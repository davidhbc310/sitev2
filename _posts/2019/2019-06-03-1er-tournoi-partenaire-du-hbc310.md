---
date: 2019-06-03 18:29:00
images:
- /uploads/affichepartenaires__psj75l.png
layout: post
title: 1er Tournoi Partenaire du HBC310!
---
<div class="news__content smart_pictures_width">
<p><span style="color:#000000;">A bréal sous Montfort (Gymnase C.Besson)</span></p>
<p><span style="color:#000000;">Mercredi 19 juin à partir de 19h, le HBC310 organise son premier tournoi partenaires afin d'offrir aux personnes qui nous soutiennent un moment de sport, de détente et de convivialité!</span></p>
<p><span style="color:#000000;">Les loisirs du HBC310 ainsi que les séniors seront là pour les accompagner.</span></p>
<p><span style="color:#000000;">Vous pouvez venir rencontrer nos partenaires et assister à des rencontres endiablées!</span></p>
 </div>