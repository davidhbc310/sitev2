#!/bin/bash

#Get all teams
files=./../../_teams/*.md

for f in $files; do
    
    # Get name of file
    #echo filename="${f##*/}"
    #echo {$f}

    while IFS= read -r line
    do
        if [[ $line = *code:*pool* ]]
        then
            poolCode=$(echo "$line" | tr -d -c 0-9)
            wget -O - 'https://jjht57whqb.execute-api.us-west-2.amazonaws.com/prod/pool/'$poolCode | gunzip -c > pool$poolCode.json
            wget -O - 'https://jjht57whqb.execute-api.us-west-2.amazonaws.com/prod/player/'$poolCode | gunzip -c > player$poolCode.json

        fi
   
    done < "$f"

done 


mv *.json ./../../_data/ffhb/

#wget -O - 'https://jjht57whqb.execute-api.us-west-2.amazonaws.com/prod/pool/65279' | gunzip -c > 65279.json
