---
date: 2020-06-26 17:03:00
images:
- /uploads/maxresdefault__qcjgi3.jpg
- /uploads/explications-inscriptions-1__qcjjuh.png
- /uploads/j-avais-une-licence-au-hbc-310-l-an-dernier-62__qcjjui.png
layout: post
title: INSCRIPTIONS et PLANNINGS saison 2020-2021
---
<div class="news__content smart_pictures_width">
<p>Vous trouverez en haut de ce site internet un onglet "<strong>saison 2020-2021"</strong> avec plusieurs parties : </p>
<p>-les plannings prévisionnels pour l'école de hand, la section masculine, la section féminine et la section loisirs. Le rendu est moyen à cause du site clubeo, je vais les afficher en plus grand. </p>
<p>-les documents explicatifs : les étapes de la procédure et quelques explications globales. </p>
<p>-un lien vers le module de paiement et d'inscription. ( ou directement en cliquant : <a href="https://www.helloasso.com/associations/handball-club-310/adhesions/inscription-handball-club-310-saison-2020-2021-2" rel="nofollow noreferrer noopener">ICI</a> )</p> </div>