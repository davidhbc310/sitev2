---
date: 2020-06-26 19:11:00
images:
- /uploads/hbc-310-63__qcjmee.png
layout: post
title: Entraînements semaine du 29 juin au 4 juillet
---
<div class="news__content smart_pictures_width">
<p>Excellente nouvelle, on finit en salle !! ENFIN. Et tout le monde a droit à un créneau. On vous attend nombreux ! </p>
<p>Inscription via doodle : <a href="https://doodle.com/poll/5xf28nhc7garn4p7?fbclid=IwAR03RvTGBtTdLtDw7R_jb6S1a93B2ppBAwbLLOIQ6kOsld2OdUbIQ96II1A" rel="nofollow noreferrer noopener" target="_blank">https://doodle.com/poll/5xf28nhc7garn4p7</a></p> </div>