---
layout: team
code: U18G_DEP
title: U18 Garçons - Groupe Départemental
coach: 
categorie: jeunes
ages: "en 2007, 2008 et 2009"
sexe: G
#widget-prochain-match : 5f8c6ae5e7b69a3d12a34b09
#widget-classement : 5f8c6685e7a7213e4c0a8ea6
description: L'équipe 1 masculine des moins de 18 ans du Handball Club 310 regroupe les joueurs nés en 2007, 2008 et 2009 et évolue dans le championnat départemental de la ligue de Bretagne
competitions:
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/u18-masculin-territoriale-25564/poule-156650",
        file: saison-2024-2025-20_regional_u18-masculin-territoriale-25564_poule-156650,
        equipe: 2
    }

---

