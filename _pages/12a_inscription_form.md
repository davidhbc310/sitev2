---

layout: page
title: Formulaire d'inscription
subtitle: Saison 2024-2025
permalink: /inscriptions/formulaire/
categories: club
description: Saison 2024-2025, Nouveau ou ancien licencié au sein du HBC310 et paiement en ligne
widget-helloasso: https://www.helloasso.com/associations/handball-club-310/adhesions/inscriptions-saison-2024-2025/widget
formulaire-helloasso: https://www.helloasso.com/associations/handball-club-310/adhesions/inscriptions-saison-2024-2025
images:
 - /uploads/images/pages/inscriptions2024.png
header-buttons:
- label: Helloasso
  external-link: "https://www.helloasso.com/associations/handball-club-310/adhesions/inscriptions-saison-2024-2025"
ariane-sub-elements:
    - {title: Inscriptions,
        url: /inscriptions/}
---




### Principes généraux
Les inscriptions se font via le formulaire ci-dessous ou directement sur [Helloasso]({{page.formulaire-helloasso}}).  
Attention à bien mettre une adresse mail valide qui servira ensuite pour finaliser votre
inscription.  
Enfin attention à bien choisir la bonne catégorie (en fonction des années de naissance).

[Plus d'infos sur l'adhésion sur la page précédente.]({{site.url}}/inscriptions/)

 {% include widget-helloasso.html  url=page.widget-helloasso %}