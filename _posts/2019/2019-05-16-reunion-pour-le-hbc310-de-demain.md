---
date: 2019-05-16 06:49:00
images:
- /uploads/reunion-4j__psj5i0.jpg
- /uploads/hbc__prkyqn.jpg
- /uploads/stru__prkyqp.jpg
layout: post
title: REUNION  POUR LE HBC310 DE DEMAIN
---
<div class="news__content smart_pictures_width">
<p style="text-align:justify;">B<span style="color:#000000;">onjour le HBC310,</span></p>
<p><span style="color:#000000;">    Vous avez reçu dernièrement un message d’Éric annonçant son souhait de quitter la présidence du HBC310 à l’issu de la saison 2019. D’autres postes importants au sein du bureau vont vraisemblablement être également vacants.</span></p>
<p> </p>
<p style="text-align:center;"><span style="color:#000080;"><strong>Afin de poursuivre la dynamique impulsée par le président actuel depuis 6 ans et construire le HBC310 de demain, nous avons besoin de vous.</strong></span></p>
<p> </p>
<p><span style="color:#000000;">    Nous avons pris l’initiative avec quelques membres du bureau de réfléchir à une nouvelle forme de gouvernance. Avec près de 300 licenciés, les responsabilités peuvent faire peur. C’est dans cette optique que nous vous proposons une nouvelle forme de bureau dont vous trouverez un schéma en pièce jointe à ce message. <br/></span></p>
<p> </p>
<p style="text-align:center;"><span style="color:#000080;font-size:24px;">La réunion d'échanges, discussions, </span></p>
<p style="text-align:center;"><span style="color:#000080;font-size:24px;">propositions autour du futur projet de gouvernance du HBC310</span></p>
<p style="text-align:center;"><span style="color:#000080;font-size:24px;"> se déroulera le </span><strong><br/></strong></p>
<p style="text-align:center;"><span style="font-size:24px;color:#000080;"><strong>Mardi 4 juin </strong></span></p>
<p style="text-align:center;"><span style="font-size:24px;color:#000080;"><strong>ATTENTION CHANGEMENT D'HORAIRE :</strong></span></p>
<p style="text-align:center;"><span style="font-size:36px;color:#ff0000;"><strong>20H45</strong></span></p>
<p style="text-align:center;"><span style="font-size:24px;color:#000080;"><strong>au gymnase de Mordelles</strong>.</span></p>
<p style="text-align:center;"> </p>
<p style="text-align:left;"> </p>
<p><br/>     <br/>     Un sondage a été mis en place, vous avez dû être mis au courant par les responsables d’équipes. Si ce n’est pas le cas, voici le lien pour y accéder : <a href="https://forms.gle/MBDi5BcFeSoRnjdT7" rel="nofollow noreferrer noopener" target="_blank">https://forms.gle/MBDi5BcFeSoRnjdT7</a><br/><br/>     <br/>     Merci à toutes les personnes qui se sentent potentiellement intéressées pour un coup de main ponctuel ou plus, de se signaler par retour de mail (<a href="mailto:35hbc310@gmail.com" rel="noreferrer noopener" target="_blank">35hbc310@gmail.com</a>) ou par téléphone (Laurent LE FLECHER : 06.73.53.26.37).</p>
<p> </p>
<p>Laurent LE FLECHER (Membre du bureau)</p> </div>