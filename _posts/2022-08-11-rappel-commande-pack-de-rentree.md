---
layout: post
date: 2022-08-11 19:44:01 +0000
title: 'Rappel : commande pack de rentrée '
tags:
- important
images:
- "/uploads/offre_rentree2022.png"

---
Une première commande a été passée en juillet, il est possible d'en passer une autre en août avec quelques commandes supplémentaires, ce qui permettra une livraison début septembre. 

Bien entendu une autre commande sera prise à la rentrée. 

Pour pré commander : cliquez [https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree](https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree "https://www.helloasso.com/associations/handball-club-310/boutiques/offre-rentree")