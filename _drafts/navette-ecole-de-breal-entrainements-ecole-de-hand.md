---
layout: post
date: 2021-09-08 20:30:15 +0000
title: Navette école de Bréal entraînements école de Hand / mise à jour 15 septembre
tags:
- a la une
images:
- "/uploads/pedibus2-1000x670.jpg"

---
Un mail a été envoyé aux parents de l'école de hand.

Le HBUS 310 reprend du service et passera les lundi, jeudi et vendredi récupérer les enfants des écoles de Bréal vers 16h15 pour l'école publique et vers 16h20 pour l'école privée.

**Merci de vous inscrire obligatoirement :** 

\-auprès du club : [https://forms.gle/SGcnnUfMLseKxSmc8](https://forms.gle/SGcnnUfMLseKxSmc8 "formulaire hbus310 club")

On verra par la suite si la mairie active le portail ou non ! 

Si vous avez des questions, comme toujours, envoyez un mail à l'adresse "contact@hbc310.fr"