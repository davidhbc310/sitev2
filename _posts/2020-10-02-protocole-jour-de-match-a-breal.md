---
date: 2020-10-02 18:32:00
images:
- /uploads/jaune-et-blanc-avec-photo-protection-de-l-environnement-evenement-affiche-12__qhl1xh.png
layout: post
title: Protocole jour de match à bréal
tags: [competitions]
---
<div class="news__content smart_pictures_width">
<p>Bonne nouvelle : les parents et le public pourront venir assister aux matchs, bien entendu dans le strict respect des consignes sanitaires. </p> </div>