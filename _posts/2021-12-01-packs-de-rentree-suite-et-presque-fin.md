---
layout: post
date: 2021-12-01 14:01:55 +0000
title: Packs de rentrée ( suite et presque fin )
tags: []
images:
- "/uploads/istockphoto-520294182-612x612.jpg"

---
Bonne nouvelle : quasiment tous les packs restants sont arrivés ( sauf le M femme qui a encore un petit peu d'attente...mais c'est imminent ).

**Toutes nos excuses pour ces retards,** Sport 2000 a fait au mieux mais la situation est très très tendue et il y a eu de nombreuses ruptures chez tous les équipementiers suite à la crise sanitaire puis à la reprise ( trop ) forte de l'activité.

Les t shirts coton 152, les packs adultes hommes et le pack femme en S sont donc déposés dans l'après midi à Bréal ( à côté des sacs de maillots, voir avec les entraîneurs pour les demander ). 