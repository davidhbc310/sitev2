---
date: 2019-03-18 19:21:00
images:
- /uploads/handf__pokr0m.png
layout: post
title: PASSIONNÉM'HAND FÉMININ ...
---
<div class="news__content smart_pictures_width">
<p style="text-align:center;"><span style="font-size:24px;">IMPORTANT : le HBC 310</span></p>
<p style="text-align:center;"><span style="font-size:24px;"> lance l'opération </span></p>
<p style="text-align:center;"><span style="font-size:24px;">passionném'hand féminin</span></p>
<p style="text-align:center;"><span style="font-size:24px;"> en ouvrant ses créneaux d'entraînement</span></p>
<p style="text-align:center;"><span style="font-size:24px;"> aux filles du secteur</span>.</p>
<p style="text-align:center;"><br/>Alors ,si vous avez une fille, une nièce, une voisine, une cousine, une amie de la famille ( née entre 2004 et 2011inclus) n'hésitez pas à lui proposer de venir nous rejoindre pour essayer le handball !</p>
<p style="text-align:center;"><br/>Merci une fois de plus de partager au maximum et de bien diffuser l'information.</p>
<p style="text-align:center;"><br/>Plus d"infos sur le flyer et sur le site internet du club ( voir Adrien si besoin d'infos complémentaires )</p> </div>