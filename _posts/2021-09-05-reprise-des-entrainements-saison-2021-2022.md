---
layout: post
date: 2021-09-05 06:59:11 +0000
title: Reprise des entraînements saison 2021 2022
tags:
- important
images:
- "/uploads/unnamed.jpg"

---
**Une nouvelle saison démarre !!** 

Les créneaux 2021 2022 entrent en vigueur à partir du lundi 6 septembre. 

En cas de doute, il sont visibles [ICI.](https://hbc310.fr/entrainements/ "Entraînements ") 

Pour l'école de hand à Bréal, il n'y aura pas de navette HBUS 310 depuis les écoles pour la semaine de reprise, elle va donc démarrer lundi 13 septembre ( mais il y aura bien entraînement ). 

Vous avez dû recevoir ces informations par mail, si ce n'est pas le cas, consultez vos spams et envoyez-nous un message via l'adresse "contact@hbc310.fr". Certaines adressent bloquent nos envois groupés ( hotmail par exemple ). 

On souhaite à tout le monde une excellente reprise sportive. 