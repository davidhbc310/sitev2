---
date: 2020-06-01 15:59:00
images:
- /uploads/29__mz38z9.jpg
layout: post
title: Reprise partielle et en petits groupes
---
<div class="news__content smart_pictures_width">
<p>Vous avez dû recevoir par mail un bilan ( complet, désolé !! ) du Conseil d'Administration.</p>
<p>Les parents des catégories moins de 13, moins de 15 et moins de 18 ont dû aussi recevoir un mail avec un lien doodle pour une reprise partielle des activités dans le strict respect des consignes sanitaires. Les places sont très limitées. </p>
<p>Si ce n'est pas le cas, vérifiez bien vos spams et envoyez un mail éventuellement à <a href="mailto:ronanhbc310@gmail.com">ronanhbc310@gmail.com</a>. </p>
<p> </p>
<p> </p> </div>