---
layout: post
date: 2021-11-01 07:50:32 +0000
title: 'Invitation match à Cesson '
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-19.png"

---
Le Handball club 310 a la chance d'être invité par le club de Cesson Rennes métropole pour aller voir un match de LNH contre Toulouse. 

**Match le vendredi 12 novembre à 20h.** 

Places gratuites pour les licenciés. 

Places à seulement 5 euros pour les accompagnants non licenciés ( pas de nombre limite ! ). 

Réservation rapide obligatoire soit via les coachs qui ont dû vous sonder soit par mail : ronanhbc310@gmail. 

On avisera ensuite avec Cesson pour les modalités de paiement et le rendez-vous sur place. 