---
layout: post
date: 2021-05-18T06:50:55.000+00:00
title: 'Reprise du handball en INTÉRIEUR pour les jeunes ! '
tags:
- a la une
images:
- "/uploads/handball-club-310-52.png"

---
Excellente nouvelle, les entraînements vont enfin pouvoir reprendre de façon quasi normale à partir du 19 mai pour les catégories jeunes. 

Encore un peu de patience pour les adultes mais ça se rapproche. 

Seule petite difficulté pour nos U18 avec un seul créneau à cause du couvre feu qui est repoussé mais qui ne nous permet pas de proposer plus ( ce qui sera possible après le 9 juin ). 

On espère revoir un maximum de monde à cette occasion.