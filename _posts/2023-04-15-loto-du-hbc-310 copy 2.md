---
layout: post
date: 2023-04-15 17:00:05 +0000
title: Loto du HBC 310
tags:
- a la une
images:
- "/uploads/337704632_964209141252801_7399306861591345999_n_2023_04_15.jpg"

---
Dans 15 jours, loto du HBC 310 animé par [Gégé Loto](https://www.facebook.com/gege.loto.14?__cft__\[0\]=AZXUC4gHNVf9oBcfJJq0UWTgwzyn3kAaRR3VvRBtUrop9Zw0dxsxzLmik6nG7kYE2jwBpS4Ya7_IRQVChsp068cbz1HBod5CVQ538Jdb58P0OW9ivQi99PT-7WaaFsEwfhsa4i_ixa1-6BNRJpeybjsMUMfKnNkjBOodlfqesoL9WZobrk6fwnnqIpeZVfUqNxs&__tn__=-\]K-R) !

On compte sur vous en grand nombre.

Un appel à bénévolat a été lancé, même sur un petit créneau, on a besoin de monde de 10h jusqu'au rangement de la salle.

Si vous êtes dispos un petit mail à "poleanimationhbc310@gmail.com".