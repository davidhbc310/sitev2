---
layout: post
date: 2020-10-25 10:03:44 +0000
title: École de hand'lloween
tags:
- a la une
images:
- "/uploads/noir-et-bleu-a-motif-adolescents-taches-tableau-5.png"

---
**Samedi 31 octobre**, matinée spéciale Halloween pour l'école de Hand. Déguisement obligatoire ( dans lequel on peut faire du sport ! ). **_Merci de prévenir de la présence de votre enfant par mai_**l ( ronanhbc310@gmail.com ) afin de préparer les bonnes quantités d'araignées et de venin de serpents pour la potion...