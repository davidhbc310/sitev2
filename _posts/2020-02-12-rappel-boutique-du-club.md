---
date: 2020-02-12 14:59:00
images:
- /uploads/35__mz38z9.jpg
layout: post
title: 'RAPPEL : boutique du club'
---
<div class="news__content smart_pictures_width">
<span>N'oubliez pas la nouvelle boutique en ligne du club : <a href="https://hbc310.ecwid.com/" rel="nofollow noreferrer noopener">https://hbc310.ecwid.com/</a></span>
<span>Un grand merci pour tout le travail engagé par David et par Baptiste. </span>
<span>Il est d'ailleurs possible d'acheter SA tenue de match avec son numéro fétiche et le flocage de son prénom ou de son surnom ! ( la classe ;) </span>
<span>Si vous avez des questions envoyez un mail à "boutiquehbc310@gmail.com"</span>
</div>