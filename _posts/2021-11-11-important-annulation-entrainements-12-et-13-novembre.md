---
layout: post
date: 2021-11-11 09:40:15 +0000
title: Annulation entraînements 12 et 13 novembre
tags:
- a la une
images:
- "/uploads/important-2.jpg"

---
**Vendredi 12 novembre :** 

En raison du nombre très élevé de participants au match de Cesson, les entraînements de vendredi sont annulés ( sauf pour l'école de hand qui est maintenue ). 

**Samedi 13 novembre :** 

En raison du plateau organisé par le club pour les Arthur de 10h à 12h, avec la réception de clubs voisins, les entraînements pour les Lancelot et Merlin ne pourront avoir lieu. 

Merci pour votre compréhension. 