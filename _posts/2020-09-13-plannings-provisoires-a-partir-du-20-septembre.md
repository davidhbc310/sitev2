---
date: 2020-09-13 21:29:00
images:
- /uploads/27__mz38z9.jpg
layout: post
title: Plannings provisoires à partir du 20 septembre
tags: [club]
---
<div class="news__content smart_pictures_width">
<p>Bonne nouvelle <strong>: finalement les mesures sanitaires nous permettent d'ouvrir les gymnases. </strong></p>
<p><strong>En revanche pas d'entraînements loisirs et seniors à Mordelles. </strong></p>
<p><strong>De <span style="text-decoration:underline;">façon provisoire,</span> en attente de la livraison de la nouvelle salle de Bréal ( on espère l'avoir début octobre ) : les créneaux à Bréal du lundi ( école de hand, moins de 11 garçons équipe 2 et équipe 3, moins de 15 garçons 2 et moins de 13 garçons équipe 1 ) sont annulés. </strong></p>
<p><strong>Le créneau du mercredi à Bréal des moins de 13 filles et des moins de 15 filles-2 de 16h à 17h30 est aussi annulé. </strong></p>
<p>Situation liée au retard de la salle et à l'incendie de la salle de tennis qui a entraîné un partage des créneaux entre tous les associations bréalaises. Nous n'avons aucune solution de repli. Le retour à la normale est très proche maintenant avec la future livraison de la salle du collège. </p>
<p><strong><em>TOUS LES AUTRES CRÉNEAUX ONT LIEU NORMALEMENT. </em></strong></p> </div>