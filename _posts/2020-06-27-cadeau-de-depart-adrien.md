---
date: 2020-06-27 10:29:00
images:
- /uploads/les-cadeaux-qui-feront-le-plus-plaisir-cette-annee-grande__qckswr.jpg
layout: post
title: Cadeau de départ Adrien
---
<div class="news__content smart_pictures_width">
<p>Vous êtes nombreux à me solliciter, la solution la plus simple est la mise en place d'une cagnotte participative pour toutes celles et ceux qui souhaitent le remercier. </p>
<p>Le montant lui sera remis en tout de début de saison prochaine pour "fêter" son départ comme il se doit lors de l'Assemblée générale ( avec fanfare et feu d'artifice...). </p>
<p>Le lien et les modalités vont être précisés d'ici quelques jours ( tout se bouscule un peu en cette fin de saison !! ). </p> </div>