---
date: 2020-04-05 09:20:00
images:
- /uploads/hbc-310-24__q8b0dl.png
layout: post
title: Remerciements à nos bénévoles
---
<div class="news__content smart_pictures_width">
<span>Parce qu'ils font vivre le club. </span>
<span>Parce qu'ils donnent de leur temps pour les autres. </span>
<span>Parce qu'ils sont souvent en première ligne</span>
<span>Parce qu'ils n'attendent rien en retour.</span>
<span>Parce qu'ils sont peu nombreux</span>
<span>Juste un GRAND MERCI à toutes les personnes impliquées dans la vie du club sans qui rien n'est possible.</span>
 


<span><span>#TeamHBC310</span></span>
</div>