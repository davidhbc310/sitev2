---
layout: post
date: 2021-01-10 14:35:24 +0000
title: Entraînements semaine du 11 au 16 janvier
tags:
- a la une
- important
images:
- "/uploads/programme-de-reprise-18.png"

---
Ouverture de deux créneaux par catégories ( et non plus par équipes 1 ou 2 ). Merci de bien venir à l'heure en tenue, il est impératif de bien respecter les horaires en raison du couvre feu. MERCI. 