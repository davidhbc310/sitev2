---
date: 2020-02-26 16:46:00
images:
- /uploads/19247652_182888552242746_9118888126810369888_n__q6bfst.png
layout: post
title: MERCI
---
<div class="news__content smart_pictures_width">
<p>Un grand merci à notre fidèle partenaire, le pub-brasserie-restaurant "La gamelle", situé ZA du Hinglé, 180 rue du Pré Miel à Bréal. Pour un apéro, un repas convivial, une soirée concert entre amis, c'est là bas qu'il faut aller.<br/>Le pub la gamelle est l'un de nos sponsors maillot.<br/>Encore merci.<br/>Plus d'infos et programmation sur leur page FB : <a href="https://www.facebook.com/publagamelle/?__tn__=K-R&amp;eid=ARAm0Y7rU6omw2ku1g8J0Xs6AWG_y6aaTc-X-PGO-GON0gB1f_Rq6rVbmZcG0ktxGP4rzmyJRXzBGAPn&amp;fref=mentions&amp;__xts__%5B0%5D=68.ARCvZfDjiwlv2JZ9zxeTuRvZuZ8Ot0bypNfB-uuL9_iWxnZJunx-i9PHqxnwne7CpS_J8OfwMDWH8iyEgOAngbi078oPXaleLj564b9HFTFAOgy0lhFd2f3VXIPuaZkbS9lLh1dPkkhpp9EzorrAJpWd3vyjCwPLxAkSd30ipvHYQpyPj3icMq9U2yaR1yJbeXke3N19BRs7UG1_TYiSwpvOQFdPvnYKvD78I7w73wcHI2odyRCDQKPA20Fa8EujdgbAnD6eJ0Iml95zN-XS53NNWu_mcN8iIo4C01gp4zUAQ4en-R1SsBPHz_iEzUeznkEQNnyWaaywqv1z3Dj1WqRI1w" rel="nofollow noreferrer noopener">Pub la gamelle</a> ou leur site internet : <a href="http://lagamelle35.fr/" rel="nofollow noreferrer noopener">http://lagamelle35.fr/</a></p> </div>