---
date: 2020-06-10 11:47:00
images:
- /uploads/hbc-3104__qbpf7i.png
layout: post
title: Récompense pour l'école de hand
---
<div class="news__content smart_pictures_width">
<p>Un grand merci à nos bénévoles qui vont vivre cette école de hand. </p>
<p>On a tous hâte de revoir les sourires de nos graines de champion sur les terrains dès que cela sera autorisé ! </p> </div>