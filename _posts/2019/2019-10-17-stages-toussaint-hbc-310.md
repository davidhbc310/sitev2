---
date: 2019-10-17 09:18:00
images:
- /uploads/26__mz38z9.jpg
layout: post
title: Stages Toussaint HBC 310
---
<div class="news__content smart_pictures_width">
<p>Des stages sont prévus pendant les vacances de la Toussaint, désolé pour l'annonce tardive ( la réponse de la mairie pour les salles a été très longue ). </p>
<p>Voici le document à compléter ( <a href="https://www.casimages.com/f/2ipFs1n8eSb" rel="nofollow noreferrer noopener">fichier stage toussaint</a> à télécharger et imprimer pour le rendre le jour même du stage avec le règlement de 5 euros ).</p>
<p>Merci de bien avertir rapidement de la présence de votre enfant par mail : <a href="mailto:adrienhbc310@gmail.com">adrienhbc310@gmail.com</a></p>
<p>Possibilité d'amener un copain et une copine dans toutes les catégories d'âge. </p>
<p>Calendrier : </p>
<p><strong>Lundi 21 octobre : Mordelles BSJR</strong></p>
<p>-10H-12h30 stage arbitrage ( gratuit ) / -14h-16h : stage gardien de but</p>
<p><strong>Mardi 22 octobre : Mordelles BSJR</strong></p>
<p>-école de hand : 10h-12h / -moins de 11 : 14h-16h</p>
<p><strong>Mercredi 23 octobre : Bréal</strong></p>
<p>-école de hand : 10h-12h / -Moins de 11 : 14h-16h</p>
<p> <strong>Jeudi 24 octobre : Bréal</strong></p>
<p>-Moins de 13G : 10h-12h30 / -Moins de 15G : 14h-16h30</p>
<p> <strong>Vendredi 25 octobre : Bréal</strong></p>
<p>-Moins de 13G : 10h-12h30 / -Moins de 15G : 14h-16h30</p>
<p><strong> Lundi 28 octobre : Mordelles BSJR</strong></p>
<p>-Moins de 13F : 10h-12h30 / Moins de 15F : 14h-16h30</p>
<p><strong>Mardi 29 octobre : Mordelles BSJR</strong></p>
<p>-Moins de 13F : 10h-12h30 / Moins de 15F : 14h-16h30</p> </div>