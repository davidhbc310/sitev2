---
date: 2020-05-11 09:12:00
images:
- /uploads/hbc-310-40__qa5o11.png
layout: post
title: Portrait N°15
---
<div class="news__content smart_pictures_width">
<p>Portrait N°15-Arbitre-Mehdi<br/>Un grand merci à Mehdi d'avoir complété lui aussi son portrait. Très engagé dans la vie du club, il a occupé les fonctions de vice président et a été l'un des grands acteurs de la création du HBC 310.<br/>Bien entendu un grand merci aussi pour son investissement comme arbitre, acteur incontournable du jeu et pas assez mis en avant au quotidien.<br/><a href="https://www.facebook.com/hashtag/teamhbc310?__eep__=6&amp;source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBKAM1Cm3a8DOO65TXTzn11MZIxFAVrHDslnOGacac9RFkIRQQ849U6WHkeN-gVd852eLaARJMaDIKCvaHsslKpDr83B4InDlMahCeu50OvlgLaPdHOpudb3oLWxjq7sJAc3a5JioZPWXD1Rxv-hnfWBdaA2NktqkTC-qP81MR9aj0MziDWfqLPRs_y3iprhGueMKzMpZOfVhhRehYrjHlZjN4NVycFBm2QpI3bTdy3l_eHJsTJifSB2hi75O6RECD2o_u5oBNIxK6i20vUxUzbBAIM2ETvG2MAStPDLCBuOdEV1HB0lWOhm_cZNPE3HRzYiI_plHiu6Z7rZtSTyCAwpg&amp;__tn__=%2ANK-R" rel="nofollow noreferrer noopener"><span><span>#</span><span>Teamhbc310</span></span></a><span><span> </span></span></p></div>