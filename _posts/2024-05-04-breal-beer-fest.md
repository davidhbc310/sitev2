---
layout: post
date: 2024-05-04 22:00:05 +0000
title: Bréal Beer Fest
tags: 
- a la une
images:
- "/uploads/437530065_949514133842505_5523968568306139436_n.jpg"

---

A noter sans faute, la première BRÉAL BEER FEST, **samedi 1er juin derrière** la salle Colette Besson. Avec dégustation et vente de bières locales, de quoi (bien manger) et de la musique.  

**Entrée gratuite !**  

On compte sur vous et sur vos partages, une belle occasion de passer un bon moment.  
Avec la ***Brasserie Yamas***, la ***Microbrasserie Draenek***, la ***Brasserie de la Bizhhh***, ***Diskenn***, la ***Brasserie Odyssée*** et la ***Microbrasserie Horla***. Sans oublier ***Breizh Galettes*** et ***Apero delices***.  

Avec le son de ***Loud'n Live*** et ***DJ Regio***.