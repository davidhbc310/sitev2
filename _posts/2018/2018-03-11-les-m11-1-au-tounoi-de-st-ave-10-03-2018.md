---
date: 2018-03-11 09:26:00
images:
- /uploads/sa1__p5f3g9.jpg
- /uploads/sa2__p5f3gd.jpg
- /uploads/sa3__p5f3gi.jpg
- /uploads/sa4__p5f3gm.jpg
- /uploads/sa6__p5f3gr.jpg
- /uploads/sa8__p5f3gv.jpg
- /uploads/sa9__p5f3gz.jpg
- /uploads/sa10__p5f3h4.jpg
- /uploads/sa11__p5f3hb.jpg
- /uploads/sa14__p5f3hj.jpg
- /uploads/sa13__p5f3hq.jpg
- /uploads/sa16__p5f3hv.jpg
layout: post
title: LES M11-1 AU TOUNOI DE ST AVE ( 10/03/2018)
---
<div class="news__content smart_pictures_width">
<p>samedi 10 , les m11-1 du hbc310, leurs parents et leur entraineur ont mis leur réveil pour aller à ST AVE  disputer leur premier tournoi en salle: départ 7h30 ...</p>
<p>match de 10 min ,5 matchs à disputer dans la première phase</p>
<p>match 1: RIANTEC1/HBC310 : 01/16</p>
<p>match 2: HBC310/PONTIVY : 12/0</p>
<p>match 3 : ST AVE /HBC310 : 02/08</p>
<p>match 4 : HBC310 /RIANTEC2 : 17/03</p>
<p>match 5 : hbc310/RHUYS :16/01</p>
<p>BRAVO , maintenant place à la demi finale</p>
<p>1/2 finale : RHUYS/HBC310 :02/10</p>
<p>finale : HBC310/PONTIVY  13/0</p>
<p> </p>
<p>GAGNANT DU TOURNOI SANS AUCUN MATCH PERDU ...</p>
<p>BRAVO aux joueurs qui ont  su rester concentrés  toute la journée et qui ont joué le jeu à fond .</p>
<p>Excellent état d'esprit pour cette équipe d'ami(e)s avant tout</p>
<p>Bravo aux accompagnatrices qui ont été présentes toute la journée pour soutenir l'équipe  et qui n'ont pas hésité à quitter leur lit de  bonne heure un samedi ( Mallaury et Elodie)</p>
<p>merci aux parents qui sont venus faire du bruit dans les tribunes .. toujours une bonne ambiance</p>
<p>et merci Aymeric pour les cookies ,qui ont su nous apporter de l'énergie</p>
<p> </p>
<p> </p> </div>