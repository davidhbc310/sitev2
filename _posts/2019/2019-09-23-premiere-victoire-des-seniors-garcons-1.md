---
date: 2019-09-23 09:48:00
images:
- /uploads/71001570_1369001939921778_7660635516051652608_n__py9xp6.jpg
layout: post
title: Première victoire des seniors garçons 1
---
<div class="news__content smart_pictures_width">
<p>La perf. du week-end est pour l'équipe seniors garçons 1 qui a réalisé une très belle prestation dans un match maîtrisé du début à la fin. Victoire 36-24 qui lance la saison de la plus belle des manières. Place à la coupe de France samedi prochain à domicile contre HB Détente.</p>
<p> </p> </div>