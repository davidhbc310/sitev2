---
date: 2018-06-23 19:28:00
images:
- /uploads/recrutement_2018_v3__pauhtt.png
- /uploads/sfr2__pasdvs.jpg
layout: post
title: LES SENIORS FILLES  du HBC310  RECRUTENT...
---
<div class="news__content smart_pictures_width">
<p>Née avant 2002 , venez rejoindre l'équipe des Séniors Filles du HBC310.</p>
<p>2 entraineurs - coachs et un groupe avec un super état d'esprit, du sport, du fun , du plaisir, de la compétition aussi car 2018/2019 est l'année de tous les challenges!!</p>
<p>motivée ?  l'envie de jouer au hand ?</p>
<p>un mail au  :  <a href="mailto:hbc310sF@GMAIL.COM">hbc310sf@GMAIL.COM</a></p>
<p>Tu peux aussi déjà participer au "HBC310 Summer challenge" sur facebook pour te préparer physiquement à la reprise ;) <a href="https://www.facebook.com/groups/234305840707230/" rel="nofollow noreferrer noopener">https://www.facebook.com/groups/234305840707230/</a></p>
<p>Reprise de séances courtes (physique) après la semaine du 15 août et entraînements en salle dès début septembre <img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/f57/1/16/1f609.png" width="16"/></p> </div>