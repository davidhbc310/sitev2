---
date: 2020-07-07 12:12:00
images:
- /uploads/hbc-310-29__qd3gca.png
layout: post
title: Félicitations !
---
<div class="news__content smart_pictures_width">
<p>Un grand bravo à nos bacheliers. Bon courage à celles et ceux qui iront au rattrapage ! </p> </div>