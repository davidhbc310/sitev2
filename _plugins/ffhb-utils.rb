require "net/http"

# coding: utf-8
module Jekyll



    module FFHBUtils
      
      @@fdmHistory = Hash.new
  
      def teamIsHBC310(input,teamData)
        
        if input.include? "310" or (input.eql? "ENT RENNES SUD HB 1 " and teamData.eql? "U18 Filles")
          return true
        end
        return false
      end

      def getTeamTitle(input,teamData)

        #if (input.eql? "ENT RENNES SUD HB 1" and teamData.eql? "U18 Filles")
        #  return "Ent. ST GILLES / HBC 310"
        #end
        return "HBC 310"
      end

      def checkFDMIsOffline(input)

        code = 0
       
        if (@@fdmHistory.include?(input))
          code = @@fdmHistory[input];
        else
         
          url = URI.parse(input)
          req = Net::HTTP.new(url.host, url.port)
          req.use_ssl = true if url.scheme == 'https'
          res = req.request_head(url.path)
          code = res.code
          @@fdmHistory[input] = code
          
        end
        
        puts "La feuille de match suivante n'est pas disponible  #{input} : #{code} " unless code == "200"

        return code != "200"

      end

      


    end
  end
  
  Liquid::Template.register_filter(Jekyll::FFHBUtils)