---
date: 2019-10-18 16:05:00
images:
- /uploads/27__mz38z9.jpg
layout: post
title: Match St Grégoire 19 octobre
---
<div class="news__content smart_pictures_width">
<p>A tous les inscrits, logiquement vous avez reçu un mail !! </p>
<p>Rendez-vous salle de la Ricoquais à St Grégoire à 19h30 sans faute ( ou 18h pour voir le match d'ouverture ). </p>
<p>Bien prévoir 3 euros par adultes en faisant l'appoint pour faciliter le travail des bénévoles à l'entrée. </p>
<p>Apportez une petite tenue de sport à votre enfant s'il participe au protocole d'avant match. </p>
<p>Il est toujours possible de venir mais il faut obligatoirement prévenir par mail : <a href="mailto:ronanhbc310@gmail.com">ronanhbc310@gmail.com</a></p>
<p> </p> </div>