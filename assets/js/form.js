function sendForm(form){


    try {
  
        //On masque l'eventuel message d'erreur précédent
        document.getElementById('errorMessageBox').classList.add("is-hidden");
  
        var data = [].slice.call(form).map(function(control) {
          return 'value' in control && control.name_form_export ?
            control.name_form_export + '=' + (control.value === undefined ? '' : control.value) :
            '';
        }).join('&');
        var xhr = new XMLHttpRequest();
  
        xhr.open('POST', form.action + '/formResponse', true);
        xhr.setRequestHeader('Accept',
            'application/xml, text/xml, */*; q=0.01');
        xhr.setRequestHeader('Content-type',
            'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.send(data);
  
        //Affichage de la confirmation de la commande
        document.getElementById('confirmMessageBox').classList.remove("is-hidden");
  
        //On vide le panier puisqu'il a été commandé
        simpleCart.empty();
        //On empèche de resoumetre le formulaire
        document.getElementById('validerCommande').setAttribute("disabled", "disabled");
  
  
      } catch(e) {
  
        document.getElementById('errorMessageBox').classList.remove("is-hidden");
      }
  
  
      return false;
  
  
  }
