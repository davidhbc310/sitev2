---
date: 2020-05-26 17:55:00
images:
- /uploads/hbc-310-162__qay48s.png
layout: post
title: Opération "soutiens ton club" !
---
<div class="news__content smart_pictures_width">
<p>Comme la majorité des clubs amateurs, toutes disciplines confondues, le HBC 310 a répondu favorablement à la proposition de la fondation des sports de création d'une cagnotte solidaire pour faire face aux pertes engendrées par l'annulation d'événements club ( loto, tombola, tournoi moins de 11 ) et pour anticiper d'éventuelles baisses de sponsoring ou d'aides publiques la saison prochaine.<br/>A SAVOIR :<br/>85 % des sommes versées iro<span>nt directement au club,<br/>10 % sont versés dans une caisse de solidarité dédiée aux clubs les plus en difficultés qui n’auraient malheureusement pas reçu de dons et 5 % servent aux frais de fonctionnement de la plate-forme.<br/>Le CNOSF a déjà participé à hauteur de 100 000 € dans la caisse de solidarité.<br/>L'opération « Soutiens ton club » a obtenu le statut de fondation d’utilité publique, ce qui donne le droit de délivrer des reçus fiscaux et permet ainsi aux gens de déduire 60 % à 66 % du don de leurs impôts</span></p>
<p> </p>
<p><span>LIEN DIRECT : <a href="https://www.soutienstonclub.fr/events/31luha8i" rel="nofollow noreferrer noopener">https://www.soutienstonclub.fr/events/31luha8i</a></span></p> </div>