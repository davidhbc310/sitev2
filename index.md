---
title: Accueil
layout: home
chatbot-is-enabled: true
hide_widgets-scoreandco: true
show_sidebar: true


image-sidebar-1: /uploads/inscriptions_en_cours.png
link-sidebar-1: inscriptions/

image-sidebar-2: /uploads/inscriptions_en_cours.png
link-sidebar-2: inscriptions/

#text-widgets-matchs: Compétitions
#widget-gauche-type: week-events
#widget-gauche-code: 628

#widget-droit-type: ranking
#widget-droit-code: 630

description: >
  Le Handball Club 310 (ou HBC310) est le club de Handball de Bréal-sous-Monfort, Mordelles et Saint-Thurial, sité à l'ouest de Rennes. Près de 300 licenciés portent les couleurs du club aux niveaux départemental (Ille et Vilaine) et régional (Bretagne).

acces-rapides-title: "Saison 2024-2025"
acces-rapides:
 - {title: Inscriptions,
    subtitle: Formulaire et paiement en ligne,
    link: /inscriptions/,
    color: dark
   }
 - {title: Entraînements,
    subtitle: "Lieux, horaires ...",
    link: /entrainements/,
    color: primary
   }
 - {title: Boutique,
    subtitle: Nouvelle gamme Erima !,
    link: /boutique/,
    color: primary
   }

zones: 
 - {image1: ,
    image2: ,
    link-text: ,
    link-page: }



tags_news: [a la une]

---


