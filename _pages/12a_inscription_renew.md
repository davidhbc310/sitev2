---

layout: page
title: Renouvellement d'une licence
subtitle: Saison 2021-2022
permalink: /inscriptions/renouvellement/
categories: club
description: Saison 2021-2022, Renouvellement et paiement en ligne
widget-helloasso: https://www.helloasso.com/associations/handball-club-310/adhesions/inscription-saison-2021-2022-renouvellement-1/widget
images:
 - /uploads/images/pages/inscriptions.png
ariane-sub-elements:
- {title: Inscriptions,
    url: /inscriptions/}
---



### Principes généraux
Cette page ne concerne que les licenciés au Handball Club 310 de la saison 2020-2021.
Les réinscriptions se font via le formulaire ci-dessous ou directement sur [Helloasso]({{page.formulaire-helloasso}}).
Attention à bien mettre une adresse mail valide qui servira ensuite pour finaliser votre
inscription.
Enfin attention à bien choisir la bonne catégorie (en fonction des années de naissance).

[Plus d'infos sur l'adhésion sur la page précédente.]({{site.url}}/inscriptions/)


### Formulaire d'inscription

{% include widget-helloasso.html  url=page.widget-helloasso %}