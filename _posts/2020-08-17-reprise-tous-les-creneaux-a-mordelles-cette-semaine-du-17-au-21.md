---
date: 2020-08-17 15:59:00
images:
- /uploads/programme-de-reprise-16__qf7oqp.png
layout: post
title: 'REPRISE : tous les créneaux à Mordelles cette semaine ( du 17 au 21 )'
---
<div class="news__content smart_pictures_width">
<p>Bonjour tout le monde, on espère que vous allez bien. Petits ajustements pour le programme de la semaine...vous imaginez bien que la situation sanitaire complique un peu la donne, donc MERCI à la mairie de Mordelles qui nous ouvre une fois de plus les équipements sportifs de la commune et qui nous fait confiance. Nous avons eu confirmation ce matin ( d'où ce message tardif ) et nous préférons donc faire tous les créneaux de la semaine à Beauséjour ( à Bréal c'était en extérieur et les prévisions météo ne sont pas bonnes mercredi ). Cela concerne donc l'ensemble des catégories concernées par la reprise cette semaine : moins de 15 / moins de 18 et seniors. [ on vous tient au courant au plus vite pour la semaine prochaine <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png" width="16"/><span>;)</span></span> ]</p> </div>