---
layout: post
date: 2020-11-05T07:18:11.000+00:00
title: 'Suspension des matchs et entraînements '
tags: []
images:
- "/uploads/suspensioncovid.png"

---
Petite pause dans ce début de saison, on espère vous revoir le plus vite possible sur les terrains. **Prenez soin de vous et de vos proches**. Et n'oubliez pas de suivre le club sur nos réseaux sociaux FB et Instagram pour garder le lien.