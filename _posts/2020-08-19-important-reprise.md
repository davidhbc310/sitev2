---
date: 2020-08-19 19:09:00
images:
- /uploads/programme-de-reprise-24__qfbmb8.png
layout: post
title: 'Important : reprise'
---
<div class="news__content smart_pictures_width">
<p>Un petit rappel concernant les créneaux de reprise ! Surtout bien faire attention et faire suivre éventuellement les changements de lieux. On fait du mieux possible pour avoir des salles ( ce qui n'est pas possible à Bréal ). Mordelles nous communique en fin de semaine les disponibilités, logiquement on alternera entre Coubertin et Beauséjour la semaine prochaine. ( sauf le 27, ça dépendra de la météo mais je confirme tout ça ce weekend ).</p> </div>