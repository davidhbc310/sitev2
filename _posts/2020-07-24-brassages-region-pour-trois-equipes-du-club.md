---
date: 2020-07-24 17:13:00
images:
- /uploads/noir-et-bleu-a-motif-adolescents-taches-tableau-14__qdzbnc.png
- /uploads/noir-et-bleu-a-motif-adolescents-taches-tableau-13__qdzbnc.png
- /uploads/noir-et-bleu-a-motif-adolescents-taches-tableau-15__qdzbnd.png
layout: post
title: Brassages région pour trois équipes du club !
tags: [competitions]

---
<div class="news__content smart_pictures_width">
<p>On compte sur vous pour motiver autour de vous, on a besoin de renforts pour passer le cap des brassages en moins de 15 garçons 1 ( 2006-2007 ), en moins de 15 filles 1 ( 2006-2007 ) et en moins de 18 filles 1 ( 2005-2004-2003 ) ! </p>
<p>De belles affiches et de belles rencontres en perspective dès le 3 octobre ( on croise les doigts ! ). </p> </div>