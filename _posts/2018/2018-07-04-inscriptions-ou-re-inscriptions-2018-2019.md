---
date: 2018-07-04 14:14:00
images:
- /uploads/rein__pbcdgh.jpg
layout: post
title: INSCRIPTIONS OU RÉ-INSCRIPTIONS 2018/2019
---
<div class="news__content smart_pictures_width">
<p>BONJOUR,</p>
<p>les documents pour s'inscrire ou se ré-inscrire pour la saison qui arrive sont en ligne ! ainsi que le planning des entrainements ..</p>


<p>n'hésitez pas à vous inscrire <span style="font-size:14px;"><strong>dès maintenant </strong></span>pour pouvoir jouer les matchs dès la reprise ( pas de licence qualifiée pas de match.)</p>
<p><span style="color:#000000;">et pour ceux qui ont passé commande à la boutique, il est possible de récupérer ses achats ( short-tee shirt- chaussettes) aux permanences</span></p>
<p>vous pourrez donner vos dossiers aux permanences :</p>
<p><strong><span style="color:#000080;"> à la salle Colette Besson de Bréal</span></strong></p>
<p><strong><span style="color:#000080;">- le Vendredi 6 Juillet de 19 h à 20 h</span></strong></p>
<p><strong><span style="color:#000080;">- le Lundi 16 Juillet de 19 h à 20 h</span></strong></p>
<p><strong><span style="color:#000080;">- le Mardi 28 Août de 19 h à 20 h</span></strong></p>
<p><strong><span style="color:#800080;"> à la salle Beauséjour de Mordelles</span></strong></p>
<p><strong><span style="color:#800080;">- le Lundi 9 Juillet de 19 h à 20 h</span></strong></p>
<p><strong><span style="color:#800080;">- le mardi 17 Juillet de 19 h à 20 h</span></strong></p>
<p><strong><span style="color:#800080;">- le Lundi 20 Août de 19 h à 20 h</span></strong></p>
<p> </p>
<p> </p> </div>