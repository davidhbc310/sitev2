---
layout: post
date: 2022-07-19 17:28:17 +0000
title: 'Nouveautés boutique en ligne '
tags:
- a la une
- important
images:
- "/uploads/293793077_2273836382771658_590754368306189842_n.png"

---
**BONNE NOUVELLE**  
La nouvelle boutique du club est en ligne, avec des nombreuses nouveautés ( dont le beau maillot bleu et le sweat ), mais aussi la veste de survêtement, les claquettes, le short, les chaussettes !  
A vous de tout découvrir.  
Commande 100% autonome et gérée par sport 2000, vous pouvez choisir la livraison à domicile ou en relais colis ( option payante ) mais aussi en magasin ( gratuitement ).  
Personnalisation avec le logo du club en option pour les hauts et les sacs. 

Attention aux délais, commandez assez tôt si vous voulez être équipés pour la rentrée. [https://hbc310.fr/boutique/](https://hbc310.fr/boutique/ "https://hbc310.fr/boutique/")

Bien entendu l'offre de rentrée ( maillot blanc + pack est toujours possible, avec des prix un peu plus avantageux ) [https://hbc310.fr/offre-boutique-speciale-rentree/](https://hbc310.fr/boutique/ "https://hbc310.fr/boutique/")