---
date: 2020-07-12 09:04:00
images:
- /uploads/hbc-310-322__qdcgzj.png
layout: post
title: Besoin d'aide pour ranger le local
---
<div class="news__content smart_pictures_width">
<p>Opération tri, rangement, inventaire et surtout en priorité nettoyage des ballons. Il y a beaucoup de travail. Si vous êtes disponibles, même une petite heure on ira plus vite ! </p>
<p>Rendez-vous lundi 13 juillet, salle Colette Besson à Bréal à partir de 16H !</p>
<p>( merci de prévenir de votre venue par mail : <a href="mailto:ronanhbc310@gmail.com">ronanhbc310@gmail.com</a> ) </p> </div>