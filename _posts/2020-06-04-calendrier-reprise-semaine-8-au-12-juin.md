---
date: 2020-06-04 19:27:00
images:
- /uploads/hbc-310-30__qbewi7.png
layout: post
title: Calendrier reprise semaine 8 au 12 juin
---
<div class="news__content smart_pictures_width">
<p>Programme de la semaine prochaine. On a tenu compte de la reprise pour beaucoup d'élèves en décalant un peu les séances.<br/>Attention à bien vous inscrire sur les bons créneaux.<br/>Bien rappeler les gestes barrière et les règles de distanciation à vos enfants.<br/>On ne peut pas proposer plus de 9 places par créneaux, on en est désolés.<br/>Rendez-vous terrain de foot de Bréal.<br/>Lien doodle pour les inscriptions : <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fdoodle.com%2Fpoll%2F2nmzw388qhfkbh2y%3Ffbclid%3DIwAR0_okXHdWtXkbb1Vej-Zy2d8OKEiCj22ErJI_8YPWAJPMZL98GhVSIqARM&amp;h=AT1ErNqPWDM1bibhx7wsK-WjdBUHikseYvAi0J9LnKrdmSuKr4Lm_E_Q4RFG4UdtosGSHWSRbyl_o3uGKxgQQNKBYx-gzDpBMqjPJPANjjMIn2deq8SDrZwvPBcNnFkQGJgyOYRXzbnj-wx_iWxKf_j0YfdpbWnTVMjHYGZJQOFtg93SWB1YZwJYWWAd8Of63pnP1avSXbnAZDBg3BdMnCcZk-fGobsbfHuO0XAOpr3bVWgnwzZCRl9Uieb9SUg7Xw7Kb_eD75qRVn0EwunZK6p1_AQP2FXCknjGX0QIQ9cYfThXCvd8V5MQvd_ed1xk59G493A89NLartT3RMAajWEedTch24Z7igx6atWXyVH3c27wm8KUpF5UP24ROQ7gZgN0EGCklnki22zxIzjr_jwwceLmVkUSjy5cJGMVb5M2JeJ4ZhKXNHhN0pCDhlgQa2sCv9nnXic-tsffgyM8_SY5rbVqnnv-XhjC4jO2yv1JJuRY1krdx0nceiYF9Uxd5igkuiE5tM98coUSAK27s4KyudbxsOeuQXNFPVz09LyEkpEU9nmBtgaupiWU8cO7Tg88Q0G7EWVme1W0jyaGCUncAofh_9BlBlCuhYesT-acXNz44o5ijKAb-10Cg4IDb92jqPztu5BmvjCKVw" rel="nofollow noreferrer noopener" target="_blank">https://doodle.com/poll/2nmzw388qhfkbh2y</a></p>
<p> </p>
<p>Petit point inscriptions ( samedi 6 juin 10H30 ) /<em><strong> je vais actualiser régulièrement</strong></em></p>
<p>LUNDI 8 juin : 16h45-17h45 : moins de 11 dernière année nés en 2009 ( futur moins de 13 ) + moins de 13 G ( nés en 2008 ) : <strong>il reste 2 places </strong>  </p>
<p>LUNDI 8 juin : 17h45-18h45 : moins de 13 G ( 2007 ) + moins de 15 G : <strong>COMPLET</strong></p>
<p>MARDI 9 juin : 16h45-17h45 : moins de 13 F ( prioritaires ) + moins de 15 F : <strong>il reste 5 places </strong>  </p>
<p>MARDI 9 juin : 17h45-18h45 : moins de 18 G ( + moins de 15 G nés en 2005 ) :<strong> COMPLET</strong>  </p>
<p>JEUDI 11 juin : 16h45-17h45 : moins de 15 F : <strong>COMPLET</strong>  </p>
<p>JEUDI 11 juin : 17h45-18h45 : moins de 18 F ( + moins de 15 F nées en 2005 ) : i<strong>l reste 8 places</strong>.</p> </div>