---
layout: post
date: 2022-05-23 13:43:15 +0000
title: 'Plannings de fin de saison '
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-11.png"

---
\[IMPORTANT\] A partir de lundi prochain 30 mai, opération bascule des catégories d'entraînement pour préparer la saison prochaine.

Bien entendu en fonction de vos possibilités et de votre organisation familiale.

N'hésitez pas à prendre contact si vous avez des questions.

Et on compte sur vous pour amener des copains / copines / cousins / cousines / voisins / voisines...faire des essais et découvrir le handball.