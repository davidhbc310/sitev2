---
date: 2020-06-08 14:33:00
images:
- /uploads/43__mz38z9.jpg
layout: post
title: Certificat médical OBLIGATOIRE
---
<div class="news__content smart_pictures_width">
<p>Pour anticiper les inscriptions qui vont bientôt arriver et le rush chez le médecin...je vous joins le modèle à compléter. Pour la prochaine saison le certificat sera obligatoire pour TOUT LE MONDE !! </p>
<p> </p>
<p>Lien direct en cliquant <a href="https://ffhb-cloudinary.corebine.com/ffhb-production/image/upload/v1590483956/ffhb-prod/assets/2020-21_certificat_medical.pdf?utm_source=email&amp;utm_campaign=Licences_202021__Prcisions_complmentaires_concernant_le_certificat_mdical&amp;utm_medium=email" rel="nofollow noreferrer noopener">ICI </a></p>
<p> </p> </div>