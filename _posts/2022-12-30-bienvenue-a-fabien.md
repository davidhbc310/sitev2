---
layout: post
date: 2022-12-30 13:16:53 +0000
title: Bienvenue à Fabien
tags:
- a la une
images:
- "/uploads/chartres-de-bretagne-21-_2022_12_30.png"

---
\[ANNONCE OFFICIELLE\]  
Nous sommes très heureux de vous présenter Fabien, notre nouveau salarié à temps plein qui débutera donc officiellement après les vacances de Noël. V

oici quelques mots de présentation, il a été très humble dans ses réponses. 

Vous allez découvrir un jeune entraîneur déjà expérimenté et motivé.   
Merci de lui réserver un excellent accueil ! 

**BIENVENUE FABIEN 💙**