import os
import re
import locale
import requests
import argparse 
import calendar
from markdown import markdown
from dateutil import parser
from datetime import datetime
import re
import json
from playwright.sync_api import sync_playwright

# Configurer le locale en français
locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

# Dictionnaire pour la conversion des mois en français vers l'anglais
months_translation = {
    'janvier': 'january', 'février': 'february', 'mars': 'march', 'avril': 'april',
    'mai': 'may', 'juin': 'june', 'juillet': 'july', 'août': 'august', 'septembre': 'september',
    'octobre': 'october', 'novembre': 'november', 'décembre': 'december'
}

def generate_filename_from_url(url, suffixe = ''):
    # Retire le préfixe "https://www.ffhandball.fr/competitions/"
    prefix = "https://www.ffhandball.fr/competitions/"
    if url.startswith(prefix):
        url_suffix = url[len(prefix):]
    else:
        url_suffix = url

    # Remplace les caractères non alphanumériques (sauf '-') par des underscores
    filename = re.sub(r'[^a-zA-Z0-9-]', '_', url_suffix)

    filename += suffixe;
    # Ajoute l'extension .json
    return f"{filename}.json"


# Fonction pour lire les fichiers Markdown et extraire les URLs
def extract_competition_urls(md_directory):
    urls = []
    url_pattern = re.compile(r'https://www\.ffhandball\.fr/competitions/[^ \n]+')
    for filename in os.listdir(md_directory):
        if filename.endswith('.md'):
            filepath = os.path.join(md_directory, filename)
            with open(filepath, 'r', encoding='utf-8') as file:
                content = file.read()
                found_urls = url_pattern.findall(content)
                # Nettoyage des caractères indésirables
                cleaned_urls = [url.replace(',', '').replace('"', '') for url in found_urls]
                urls.extend(cleaned_urls)
    return urls

def translate_months(date_str):
    """Traduire les mois en français vers les mois en anglais."""
    for fr_month, en_month in months_translation.items():
        if fr_month in date_str.lower():
            date_str = date_str.replace(fr_month, en_month)
    return date_str

def extract_week_number(date_str):
    """
    Extrait le numéro de la semaine à partir d'une chaîne de date.
    Exemple d'entrée : 'samedi 14 septembre 2024'
    Exemple de sortie : 37
    """
    formatted_date = format_date(date_str)
    date_obj = datetime.strptime(formatted_date, '%Y-%m-%d')
    week_number = date_obj.isocalendar()[1]
    return week_number

def extract_day_of_week(date_str):
    # Convertir la chaîne en objet datetime
    date_obj = datetime.strptime(date_str, '%Y-%m-%d')
    
    # Obtenir le jour de la semaine et le jour du mois
    day_of_week = calendar.day_name[date_obj.weekday()]  # Convertir le numéro du jour en nom complet
    
    # Formater la date
    formatted_date = f"{day_of_week}"
    return formatted_date

def format_date_with_week(date_str):
    try:
        # Convertir la date en minuscules pour que 'dateutil.parser' puisse la comprendre
        date_str = date_str.lower()

        print(date_str)

        # Traduire les mois en français vers les mois en anglais
        translated_date_str = translate_months(date_str)
        
        print(f"Date traduite : {translated_date_str}")
        # Convertir la chaîne de caractères en objet datetime
        date_obj = datetime.strptime(translated_date_str, '%d %m %Y')
        # Extraire le numéro de la semaine
        week_number = date_obj.isocalendar()[1]
        # Retourner la date formatée et le numéro de la semaine
        return date_obj.strftime('%Y-%m-%d'), week_number
    except ValueError as ve:
        print(f"Erreur de formatage de la date '{date_str}': {ve}")
        return None, None
    except Exception as e:
        print(f"Erreur inattendue : {e}")
        return None, None

def extract_classement(page):
    classement  = []

    item = page.query_selector_all('table.style_classement__VzowG')
    if item:
        classementElement = page.query_selector_all('table.style_classement__VzowG')[0]
        if classementElement:

            rows = classementElement.query_selector_all('tbody tr')
            if rows:
                for row in rows:
                    classement_ligne = {};

                    if row.query_selector('td:nth-of-type(1)'):
                        classement_ligne = {

                        'position': row.query_selector('td:nth-of-type(1)').text_content().strip(),
                        'club': row.query_selector('td:nth-of-type(2)').text_content().strip(),
                        'points': row.query_selector('td:nth-of-type(3)').text_content().strip(),
                        'joues': row.query_selector('td:nth-of-type(4)').text_content().strip(),
                        'gagnes': row.query_selector('td:nth-of-type(5)').text_content().strip(),
                        'nuls': row.query_selector('td:nth-of-type(6)').text_content().strip(),
                        'perdus': row.query_selector('td:nth-of-type(7)').text_content().strip(),
                        'buts_plus': row.query_selector('td:nth-of-type(8)').text_content().strip(),
                        'buts_moins': row.query_selector('td:nth-of-type(9)').text_content().strip(),
                        'diff_buts': row.query_selector('td:nth-of-type(10)').text_content().strip(),
                        'forme': row.query_selector('td:nth-of-type(11)').inner_html().strip()
                        }
                        classement.append(classement_ligne)
    return classement

def extract_week_match(page):
    # Sélectionne le div contenant la date
    date_div = page.query_selector('div.styles_title__80Rc8')
    
    # Vérifie si la date est présente
    if date_div:
        date_text = date_div.inner_text().strip()
        print(f"Date trouvée : {date_text}")
        
        # Sélectionne la div contenant la date de mise à jour (la sous-titre)
        update_div = page.query_selector('div.styles_subtitle__zx7H8 b')
        
        # Si on trouve la date de mise à jour juste après
        if update_div:
            update_text = update_div.inner_text().strip()
            # Effectue le test pour s'assurer que les deux divs sont corrects
            if "au" in date_text and "Date de mise à jour" in update_text:
                print("Le format est valide et la date de mise à jour est présente.")
            else:
                print("Le format est invalide ou la date de mise à jour est manquante.")
            return date_text
        else:
            print("Date de mise à jour non trouvée.")
    else:
        print("Date non trouvée.")
    return ""   

def extract_match_details(browser, page, pageIndex):
    try:

        start_week = "Inconnu";
       
            
        matches = page.query_selector_all('a.styles_rencontre__9O0P0')
        all_match_details = []

        # Extraire la semaine du match
        week_match = extract_week_match(page);
        print(week_match)

        for match in matches:
            details = {}
            details['date_text'] = week_match

            # Extraire les équipes
            team_elements = match.query_selector_all('.styles_teamName__aH4Gu')
            if team_elements and len(team_elements) == 2:
                team1 = team_elements[0].inner_text().strip()
                team2 = team_elements[1].inner_text().strip()
            else:
                details['team1'] = "Inconnu"
                details['team2'] = "Inconnu"

            if "310" in team1 or "310" in team2 or "ENT BREAL BRUZ MORDELLES HB" in team1 or "ENT BREAL BRUZ MORDELLES HB" in team2:

                details['team1'] = team1
                details['team2'] = team2
                details['week_number'] = start_week
                details['competition_name'] = page.locator('h1.style_title__CYMWM').text_content().strip()
                if "310" in team1 or "ENT BREAL BRUZ MORDELLES HB" in team1:
                    details['location'] = 'HOME'
                else:
                    details['location'] = 'AWAY'

                # Extraire le numéro de la journée depuis le texte
                details['day_number'] = pageIndex
                
                # Extraire le lien du match
                details['url'] = "https://www.ffhandball.fr"+match.get_attribute('href')
                
                # Extraire la date et l'heure du match
                date_time = match.query_selector('.block_date__dYMQX')
                if date_time:
                    date_time_text = date_time.inner_text().strip()
                    if " À " in date_time_text:
                        date_text, time_text = date_time_text.split(" À ")
                        formatted_date = format_date(date_text)
                        details['date_text'] = date_text
                        details['date'] = formatted_date
                        details['time'] = time_text
                        # Ajouter le numéro de la semaine
                        details['week_number'] = extract_week_number(formatted_date)
                        details['day_of_week'] = extract_day_of_week(formatted_date)
                    else:
                        #details['date_text'] = "Inconnu"
                        details['date'] = "Inconnu"
                        details['time'] = "Inconnu"

                else:
                    #details['date_text'] = "Inconnu"
                    details['date'] = "Inconnu"
                    details['time'] = "Inconnu"
                
                # Extraire les scores
                score_elements = match.query_selector_all('.styles_score__ELPXO')
                if score_elements and len(score_elements) == 2:
                    details['score1'] = score_elements[0].inner_text().strip()
                    details['score2'] = score_elements[1].inner_text().strip()
                else:
                    details['score1'] = "Inconnu"
                    details['score2'] = "Inconnu"
                
                # Click sur les détails
                # Ouvrir un nouvel onglet avec l'URL de détail
                if details['date'] != "Inconnu":
                    page_detail = browser.new_page()
                    response = page_detail.goto(details['url'])
                    if response.status != 404:
                        page_detail.wait_for_load_state('networkidle')  # Attend que la page se charge

                        salle_element = page_detail.locator('div.style_address__T80qe') 

                        if salle_element:
                            # Séparer l'adresse en plusieurs lignes (split par \n)
                            adresse_complete = salle_element.inner_text()
                            adresse_parts = adresse_complete.split('\n')

                            print(adresse_complete)
                            if "breal sous montfort" in adresse_complete.lower() or "mordelles" in adresse_complete.lower():
                                details['location'] = 'HOME'
                            else:
                                details['location'] = 'AWAY'

                            details['gymnase'] = adresse_parts[0] if len(adresse_parts) > 0 else "Libellé non disponible"
                            details['address'] = ', '.join(adresse_parts[1:]) if len(adresse_parts) > 1 else "Adresse non disponible"
                        
                    # Reviens à la liste des matchs
                    page_detail.close()
                # Revenir à la page précédente
                #page.go_back()
                #page.wait_for_load_state('networkidle')
                all_match_details.append(details)

        return all_match_details
    except Exception as e:
        print(f"Erreur lors de l'extraction des détails des matchs: {e}")
        return []

def save_to_json(data, filename, output_directory):
    # Créer le répertoire s'il n'existe pas
    os.makedirs(output_directory, exist_ok=True)
    
    file_path = os.path.join(output_directory, filename)
    with open(file_path, 'w', encoding='utf-8') as f:
        json.dump(data, f, indent=4, ensure_ascii=False)



def format_date(date_str):
    """
    Formate la chaîne de date en format ISO 8601.
    Exemple d'entrée : 'samedi 14 septembre 2024'
    Exemple de sortie : '2024-09-14'
    """
    # Mapping des mois en français à leurs équivalents numériques
    months = {
        'janvier': '01', 'février': '02', 'mars': '03', 'avril': '04', 'mai': '05', 'juin': '06',
        'juillet': '07', 'août': '08', 'septembre': '09', 'octobre': '10', 'novembre': '11', 'décembre': '12'
    }

    # Extraction de la date avec regex
    date_str = date_str.lower()
    match = re.match(r'\w+ (\d+) (\w+) (\d+)', date_str)
    exit
    if match:
        day, month_name, year = match.groups()
        month = months.get(month_name, '01')  # '01' comme valeur par défaut si le mois n'est pas trouvé
        return f'{year}-{month}-{day}'
    else:
        return date_str  # Retourne la chaîne originale si le format ne correspond pas

def main():

    # Création d'un analyseur d'arguments
    parser = argparse.ArgumentParser(description="Scrape une URL de compétition de handball.")
    # Ajout d'un argument pour spécifier l'URL à scrapper
    parser.add_argument('--url', '-u', type=str, help="L'URL de la compétition à scrapper")
    
    # Parsing des arguments
    args = parser.parse_args()

    # Récupérer l'URL depuis les arguments
    url = args.url
    if url:
        competition_urls = [url]
    else:
        md_directory = '../../_teams/'
        competition_urls = extract_competition_urls(md_directory)
    
    output_directory = '../../_data/ffhb'
    
    if not competition_urls:
        print("Aucune URL de compétition trouvée.")
        return

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True)

        for base_url in competition_urls:

            first_page_url = base_url + '/journee-1/'

            filename_classement = generate_filename_from_url(first_page_url, "classement")
            

            filename = generate_filename_from_url(base_url)
            page = browser.new_page()
            page.goto(first_page_url)
            all_matches = []

            # Extraction des détails de match par page
            page_count = 1
            while True:
                print(f"Scraping page {page_count} pour {base_url}")
                match_details = extract_match_details(browser, page, page_count)
                all_matches.extend(match_details)
            
                next_button = page.query_selector('button[title="Journée suivante"]')
                if next_button and not next_button.is_disabled():
                    page_count += 1
                    next_url = f'{base_url}/journee-{page_count}/'
                    page.goto(next_url)
                else:
                    break
            
            # Sauvegarde les détails des matchs dans un fichier JSON
            save_to_json(all_matches, filename, output_directory)

            print(f"Les détails des matchs ont été sauvegardés dans '{filename}'.")
            page.close()
            
            # Récupération des classements
            page_classement = browser.new_page()
            page_classement.goto(base_url + "/classements/")
            print(f"Scraping page classement pour {base_url}")
            if "loisir" not in base_url:
                classement = extract_classement(page_classement)
                # Sauvegarde du classement
                save_to_json(classement, filename_classement, output_directory)



        browser.close()

if __name__ == "__main__":
    
    main()