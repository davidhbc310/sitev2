---
date: 2019-09-07 20:44:00
images:
- /uploads/infographie-match-10__pxh5eb.png
layout: post
title: 'Important : pot de rentrée du club le vendredi 13 à 19 h à Mordelles Beauséjour'
tags: [agenda]
dateEvenement: 2019-09-13 19h00
---
<div class="news__content smart_pictures_width">
<p>On attend un maximum de licenciés et de parents pour ce moment convivial. Merci d'indiquer votre présence pour faciliter l'organisation. On compte sur vous. </p>
<p>Si vous avez commandé des packs tenues avec votre inscription, vous pourrez les essayer à cette occasion. </p>
<p> </p>
<p>Inscription par mail : <a href="mailto:ronanhbc310@gmail.com">ronanhbc310@gmail.com</a></p>
<p> </p> </div>