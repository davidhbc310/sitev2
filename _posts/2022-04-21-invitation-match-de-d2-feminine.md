---
layout: post
date: 2022-04-21 10:22:24 +0000
title: 'Invitation match de D2 féminine '
tags:
- a la une
images:
- "/uploads/278140945_2199959726825991_5774224817403113917_n.png"

---
Samedi 30 avril, le handball club 310 est invité à assister à une rencontre de D2 féminine à St Grégoire. Le SGRMH reçoit Clermont Ferrand dans un match capital dans l'optique du maintien. Nos jeunes licenciées seront mises à l'honneur à cette occasion ( surprise ! ). 

Invitation pour les licenciés du club avec un accompagnant. 

Les accompagnants adultes supplémentaires ont un tarif privilégié ( 3€ ). 

Inscription obligatoire par mail : ronanhbc310@gmail.com

Pensez à proposer aux autres membres de votre équipe pour covoiturer et permettre ainsi au plus grand nombre de venir. 

On compte sur vous. 