---
layout: post
date: 2021-08-26T19:00:46.000+00:00
title: Entraînements du 30 août et 1er septembre
tags:
- a la une
images:
- "/uploads/ajouter-un-titre.png"

---
\[PROGRAMME SEMAINE PROCHAINE\]

Petite semaine avec la rentrée scolaire et surtout la préparation des forums dans nos salles.

Attention, lundi c'est Bréal et mercredi c'est Mordelles à Coubertin !

Donc au programme :

**Lundi à Bréal :**

\-16h30-18h : U11

\-18h30-20h : U15

\-20h30-22h : SENIORS

**Mercredi à Mordelles Coubertin**

\-16h30-18h : U13

\-18h30-20h : U18

\-20h30-22h : LOISIRS

**Pas de séances jeudi, vendredi et samedi à cause de la préparation des forums par les mairies !** 

On fait passer le mot, on se motive, et on revient faire du VRAI hand ! Reprise de l'école de hand à partir du 6 septembre.