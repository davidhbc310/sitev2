---
layout: post
date: 2022-07-15 19:41:28 +0000
title: 'Un nouveau partenaire : moins 50% sur la licence ! '
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-15.png"

---
\[INSCRIPTIONS\]

Si vous connaissez dans votre entourage des chefs d'entreprise ou si vous êtes très bons clients dans certains commerces du secteur, n'hésitez pas à présenter la nouvelle plaquette de présentation du club avec les formules de partenariat. [https://hbc310.fr/partenaires/](https://hbc310.fr/partenaires/ "https://hbc310.fr/partenaires/")

Si vous réussissez à les convaincre vous bénéficierez d'une licence réduite de 50% ! Voir les précisions sur l'infographie.( cela concerne tout le monde, même celles et ceux qui sont déjà inscrits ). On compte sur vous !

Et si ça donne envie à des partenaires qui nous lisent de nous rejoindre, n'hésitez pas à nous contacter directement ! 