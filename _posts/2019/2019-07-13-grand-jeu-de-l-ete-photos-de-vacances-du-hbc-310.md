---
date: 2019-07-13 13:33:00
images:
- /uploads/jeu-photo-de-vacances-du-handball-club-310__pukw3l.png
layout: post
title: 'Grand jeu de l''été : photos de vacances du HBC 310 !'
---
<div class="news__content smart_pictures_width">
<p>Bientôt en vacances ? On pense à mettre dans ses valises le t-shirt du club pour jouer au grand concours de l'été du HBC 310 <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/f4c/1/16/1f642.png?_nc_eui2=AeH8pbFmQR0qn3B6m62u9EYhiAr5J5KQmQ8ItyaQi_K8d2erp8oVWFcqpddW1hQZQhgfjygFS0ud4AJROQy50U4zXN3p9-LRjmKk4seEGBV-Sw" width="16"/><span>:)</span></span> ! Excellentes vacances à celles et ceux qui ont la chance d'en avoir et bon courage à ceux qui travaillent encore. On vous attend en pleine forme pour la reprise. <br/>Ronan</p> </div>