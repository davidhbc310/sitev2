---
layout: post
date: 2023-10-03 22:00:05 +0000
title: Stages Toussaint
tags: 
- a la une
images:
- "/uploads/stage_toussaint_2023.png"

---

Inscription obligatoire via Helloasso, merci de privilégier le paiement des 10 euros directement en ligne pour faciliter la gestion.  Vous pouvez toujours mettre ZEROHBC310 et payer par chèque ou liquide à remettre à l'entraîneur. **Attention à bien vous inscrire malgré tout sur helloasso.**  
- Lundi 23 octobre de 9h à 17h : U15
- Mercredi 25 octobre de 9h à 17h : U13
- Jeudi 26 octobre de 9h à 17h : U11

    
<a href="https://www.helloasso.com/associations/handball-club-310/evenements/stages-toussaint-hbc-310" class="button is-dark" target="_blank">Je m'inscris au stage</a>


*A noter que des entraînements classiques auront lieu pour les U18, les seniors et les loisirs. Calendrier à venir.*