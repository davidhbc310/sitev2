---
layout: post
date: 2024-01-08 21:00:05 +0000
title: Un maillot pour les plateaux
tags: 
- a la une
images:
- "/uploads/418895524_874328754694377_993961066426107897_n.jpg"

---

En ce début d'année 2024, on souhaite **équiper nos super champions U9 et U7 de vrais maillots** pour les rassemblements mini hand (soit 20 maillots simples) ! On a opté pour le meilleur rapport qualité prix avec ERIMA France et SPORT 2000 Guichen.  
**On a donc besoin d'un partenaire pour faire aboutir le projet qui est bouclé et qui attend donc juste un logo** !  
Merci pour votre aide et pour votre soutien à nos 87 licenciés de l'école de hand (U5 + U7 + U9).  
On compte sur vous. Mail : ronanhbc310@gmail.com. *(Un CERFA sera donné pour déduction fiscale)*


