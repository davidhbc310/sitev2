---
layout: club
title: Le HBC 310 en détail
permalink: /club/
categories: club
header-buttons:
 - {label: Gymnases,
        link: /gymnases
   }
 - {label: Entraînements,
        link: /entrainements
   }
images:
 - /uploads/images/pages/hbc310.png

show_sidebar: true
description: Le Handball Club 310 (ou HBC 310) est le club de handball de Bréal-sous-Montfort, Mordelles et Saint-Thurial, 3 communes situées à l'ouest de Rennes. Pour la saison 2021-2022, près de 250 joueuses et joueurs évoluent dans les championnats départementaux et régionaux de la ligue de Bretagne.
---


