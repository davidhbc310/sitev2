---

layout: page
title: Les partenaires du HBC 310
permalink: /partenaires/
description: Les partenaires du Handball Club 310 qui nous aident à réaliser nos projets
hide_partners_footer: true
categories: club
images:
 - /uploads/images/pages/partenaires.png

---

<div class="container">
    <section class="hero is-dark has-dark-effect"> 
       <div class="hero-body"> 
            <div class="container"> 
                <div class="columns is-mobile is-centered is-vcentered is-multiline"> 
                    <div class="column is-8-tablet is-12-mobile"> 
                        <div class="title"> Devenez partenaire du HBC 310 </div> 
                        <div class="subtitle"> #TeamHBC310 </div> 
                    </div> 
                    <div class="column is-4-tablet is-6-mobile"> 
                        <div class="buttons"> 
                        <a href="{{site.url}}/documents/flyer-partenariat-HBC310.pdf" target="blank" class="button is-light is-medium">Book partenaires</a>
                                    <a class="button is-light is-medium" href="{{ '/contact/'| prepend: site.url }}">Contactez-nous</a> 
                                </div> 
                    </div>  
                    
                </div>
            </div>
        </div>
    </section>
</div>
<br>

<div class="container">
    <div class="columns is-multiline is-mobile is-centered is-vcentered">
        {% for partner in site.data.partners %}
            <div id="{{ partner.ID}}" class="column is-3-desktop is-4-tablet is-6-mobile ">
                <div class="box ">
                    <div class="has-text-centered">
                        <strong>{{ partner.title}}</strong> 
                        <br><br>
                        <a target="_blank" href="{{partner.web}}" rel="nofollow noreferrer noopener">
                            <img  src="{{site.url}}/{{ partner.logo }}" style="width:128px" loading="lazy">
                        </a>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
</div>



