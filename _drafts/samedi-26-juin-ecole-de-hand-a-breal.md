---
layout: post
date: 2021-06-22 16:14:03 +0000
title: 'Samedi 26 juin école de hand à BRÉAL ! '
tags:
- important
images:
- "/uploads/handball-club-310-95.png"

---
Attention, les entraînements de l'école de hand samedi 26 juin, auront lieu de façon exceptionnelle à Bréal, dans la nouvelle salle du collège ( aux mêmes horaires ). 

En effet Beauséjour n'est pas disponible. 

Merci pour votre compréhension. 