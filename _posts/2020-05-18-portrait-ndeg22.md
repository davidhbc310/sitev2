---
date: 2020-05-18 09:04:00
images:
- /uploads/hbc-3103__qaimc9.png
layout: post
title: Portrait N°22
---
<div class="news__content smart_pictures_width">
<span>Portrait N°22-Senior garçon-Louis</span>
<span>Dans la famille Beaudier, on a le droit aujourd'hui au grand frère. Merci à "Belette intrépide" d'avoir complété les portraits familiaux. Nous sommes rassurés une fois de plus par la présence de la fondue savoyarde en plat préféré, ce qui a fini de nous convaincre d'embaucher un nutritionniste dans le staff. En revanche, la musique de motivation d'avant match nous interroge un peu plus. </span><span><span><span> </span></span></span></div>