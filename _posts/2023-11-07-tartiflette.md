---
layout: post
date: 2023-11-07 22:00:05 +0000
title: Tartiflette
tags: 
- a la une
images:
- "/uploads/tartiflette2023.png"

---

**L'opération tartiflette est de retour.**  
Commande (et paiement si possible pour faciliter les choses) via helloasso **avant le 21 novembre**.  
**Récupération vendredi 24 novembre** de 16h30 à 20h à Bréal-foyer de la salle du collège... vous serez accueilli avec un bon vin chaud ! Merci au pôle animation et à notre grand chef cuisinier Anthony Karabat-Zic.  
Merci de partager en masse pour battre le record et de proposer aux voisins, à la famille ... !  

<a href="https://www.helloasso.com/associations/handball-club-310/boutiques/operation-tartiflette-2023?fbclid=IwAR2p7gU2ZNRi1zkeOEFwMTH5s29tuRqRD1RHtMJadqi0JqFD3lcufoyzXy0" class="button is-dark" target="_blank">Je commande</a>