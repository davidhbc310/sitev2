---
date: 2020-09-08 08:07:00
images:
- /uploads/pedibus__qgbt10.jpg
layout: post
title: NAVETTE école / école de HAND
tags: [club]
---
<div class="news__content smart_pictures_width">
<p>Les services scolaires ont besoin d'un peu de temps de mise en place, la navette sera donc proposée à partir du lundi 14 septembre. Merci de bien vouloir compléter le document joint et de prévenir l'école. </p>
<p><a href="https://s3.static-clubeo.com/uploads/hbc310/Medias/PediHandBus__qgbsxs.pdf">PediHandBus.pdf</a></p> </div>