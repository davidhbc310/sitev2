---
layout: post
date: 2022-06-26 14:35:38 +0000
title: 'Inscriptions 2022 2023 '
tags:
- a la une
- important
images:
- "/uploads/chartres-de-bretagne-14.png"

---
Les inscriptions sont ouvertes, vous trouverez la procédure, les plannings et les tarifs sur la page dédié. 

L'ensemble de la procédure est dématérialisé. 

En cas de soucis ou de questions n'hésitez pas à nous contacter via l'adresse " contact@hbc310.fr". 

Cliquez sur : [https://hbc310.fr/inscriptions/](https://hbc310.fr/inscriptions/ "https://hbc310.fr/inscriptions/")