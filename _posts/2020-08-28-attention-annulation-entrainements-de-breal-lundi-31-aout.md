---
date: 2020-08-28 10:14:00
images:
- /uploads/27__mz38z9.jpg
layout: post
title: 'ATTENTION : annulation entraînements de Bréal LUNDI 31 août !'
---
<div class="news__content smart_pictures_width">
<span>IMPORTANT : plannings semaine prochaine. </span>
<span>Il nous est impossible pour le moment de confirmer le planning des entraînements établi en juin à cause de la situation sanitaire. </span>
<span>Nous attendons la décision de la mairie de Bréal à propos de l'ouverture ou non du Complexe Colette Besson, pour ajuster et retravailler si besoin, même avec des créneaux en extérieur à Bréal sur quelques séances si nécessaire ( en attendant la livraison de la nouvelle salle ). </span>
<strong>A noter : lundi 31 août : les créneaux à Bréal sont donc annulés en revanche ceux de Mordelles sont maintenus.</strong>
 
    </div>