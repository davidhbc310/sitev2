---
layout: post
date: 2021-09-19 20:06:50 +0000
title: 'Permanence licences '
tags:
- a la une
images:
- "/uploads/permanences.jpg"

---
Permanence licence / paiement.

**Mercredi 22 septembre.** 

**18h45-20h30 à Bréal. Salle du collège**.

Attention de bien régulariser votre situation, en priorité les équipes qui jouent la semaine prochaine !! ( seniors garçons, U15 garçons et U18 garçons et filles ).

Pour celles et ceux qui ne peuvent se déplacer : merci d'envoyer un mail à l'adresse du club contact@hbc310.fr