---
date: 2020-06-26 11:32:00
images:
- /uploads/planning-2020-2021-1__qcj15z.png
layout: post
title: Planning école de HAND
---
<div class="news__content smart_pictures_width">
<p><strong>École de hand : trois bonnes nouvelles.</strong></p>
<p>La première : pour ne pas partir en vacances sans se revoir une dernière fois, réservez bien votre samedi matin du 4 juillet avec une séance de 9h30 à 10h30 pour les catégories "Merlin" et "Lancelot". Puis de 10h30 à 12h pour les "Arthur". Lieu à préciser en fonction de la météo ( et de l'aboutissement des négociations avec Mordelles <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png" width="16"/><span>;)</span></span> ! ).</p>
<p>La deuxième c'est le planning de la rentrée prochaine pour vous projeter et donner envie à nos ptits loups. Ouverture des inscriptions dans l'après-midi.</p>
<p>La troisième c'est qu'un créneau additionnel est possible pour les Arthur en "découverte-initiation" à St Thurial en fonction des effectifs dans cette commune et aux alentours. ( lundi 17h-18h ). On attend validation et confirmation ( mais c'est assez bien engagé ).</p>
<p>N'hésitez pas à partager l'info et à motiver autour de vous !</p>
</div>