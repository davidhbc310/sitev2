---
date: 2019-06-21 18:46:00
images:
- /uploads/ag1__ptgjxu.png
layout: post
title: ASSEMBLÉE GÉNÉRALE
---
<div class="news__content smart_pictures_width">
<p><span><span>On relance cette publication importante pour la vie de l'association; le vendredi 5 juillet, assemblée générale à 19 heures, suivie d'un apéritif offert par le club et enfin un repas convivial entre toute la "famille du HBC 310". Les modalités d'organisation de ce repas ( qui amène quoi ? ) ont été communiquées par mail via vos responsables d'équipes ou vos coachs. ( ou ils vont le faire très vite ). Après le repas, vous pourrez vous dépenser sur la piste de danse <span><img alt="" height="16" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png" width="16"/><span>;)</span></span> ! <br/> MERCI de répondre rapidement pour faciliter l'organisation et surtout VENEZ profiter de cette soirée avant les vacances.<br/> Ronan<br/><a href="https://www.facebook.com/hashtag/teamhbc310?epa=HASHTAG" rel="nofollow noreferrer noopener">#Teamhbc310</a></span></span><span><span> – <span>motivé.</span></span></span></p> </div>