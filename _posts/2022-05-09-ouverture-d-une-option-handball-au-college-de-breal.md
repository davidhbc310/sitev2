---
layout: post
date: 2022-05-09 18:59:00 +0000
title: 'Ouverture d''une option handball au collège de Bréal '
tags: []
images:
- "/uploads/ajouter-un-titre-62.png"

---
En 2022-2023 une option handball sera proposée aux futurs élèves de 6è et 5è ( garçons et filles ). 

Programme et modalités d'inscriptions :  [plaquette-option-corr2.pdf](/uploads/plaquette-option-corr2.pdf "plaquette-option-corr2.pdf")

\-dossier à compléter et à redonner soit directement au collège ou aux écoles primaires de Bréal. 

\-inscription et participation à l'une des séances de sélection obligatoires : mercredi 1er juin de 15h30 à 17h30 ou samedi 4 juin de 10h à 12h. 

Infos par mail : leflechereps@gmail.com 