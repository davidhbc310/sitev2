---
layout: post
date: 2022-10-12 07:10:49 +0000
title: HAND'LOWEEN 5 novembre à Bréal
tags:
- a la une
- important
images:
- "/uploads/orange-and-black-spooky-halloween-sale-promotion-instagram-post_2022_10_12.png"

---
A noter dans votre agenda.

Venez passer un moment horrifiquement convivial au tournoi hand'loween **samedi 5 novembre**. Le manoir des sports de Bréal sera pris d'assaut par une horde de monstres déchaînés, bien décidés à en découdre dans un tournoi 4x4 endiablé. 

Le pôle animation concoctera à l'occasion de quoi reprendre des forces : soupe d'oeil de limaces, beignet de cervelle de chauve souris et flan de nombril de zombies. 

Pour les moins téméraires il y aura une buvette et des saucisses-frites !

Afin d'organiser au mieux la confection des pierres tombales merci de bien vouloir vous inscrire via le formulaire suivant : [https://forms.gle/6KW4SV227WSLp5X68](https://forms.gle/6KW4SV227WSLp5X68 "https://forms.gle/6KW4SV227WSLp5X68")