---
date: 2020-05-08 09:10:00
images:
- /uploads/hbc-310-192__qa03xf.png
layout: post
title: Portrait N°12
---
<div class="news__content smart_pictures_width">
<p>Portrait N°12-Senior Fille-Emma<br/>J'ai bien insisté en demandant des photos "en tenue de hand", vous avez donc un aperçu des nouveaux survêtements du club qui arriveront l'an prochain. Je suis persuadé que ça va faire fureur.<br/>Un grand merci à Emma pour avoir complété son portrait et aussi pour son implication dans le club, notamment auprès des plus jeunes de l'école de hand.<br/><a href="https://www.facebook.com/hashtag/teamhbc310?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARDU7E4sX5AmIzRJnTorCceJmFoRP9nsrxzFJRFivC74219gwihkPuf7ARFwT4Ccb1kbrX7fyROiVhVeoy2WUXXn8NvgTfDh0oa1VBtcnPQvQR-_5QQpD0ndXmQwB58nRwzHTw5Hx-DG8sRXperZQ6ztn61gZ-EYSjfGiKmsU63EMH9eRWaL8YLBBXKSNLQ8keG8iJG_VdPOQ-_l-ZzlzPo1Rd8HJc4WHO6Jk13nkZgCFJ5sMB7r3fy8O9RpTTUowlcfITo36V08rk0SoELp-_B1CJ1I0msKBywNO9TKmQQNjBuOeOJn5EEPRieP23rdVma01fS2Vd567Eedv_9K0bPG0A&amp;__tn__=%2ANK-R" rel="nofollow noreferrer noopener"><span><span>#</span><span>Teamhbc310</span></span></a></p> </div>