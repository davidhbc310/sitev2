---
layout: post
date: 2022-08-26 14:00:21 +0000
title: 'Reprise pour quelques catégories et permanences semaine prochaine '
tags: []
images:
- "/uploads/301358529_2306476186174344_9177837675354182641_n.png"

---
Comme annoncé au début de l'été, reprise pour les moins de 15, moins de 18 ( brassages région ) et pour les seniors mardi prochain.

Une belle occasion de se retrouver pour faire l'état des lieux des bronzages et voir l'état de forme physique des troupes !

Rendez-vous : 

\-mardi 30 et jeudi 1er pour les moins de 15 ( mixtes ) de 17h30 à 19h. Puis les moins de 18 ( mixtes ) de 19h à 20h30. Et enfin les seniors ( mixtes ) de 20h30 à 22h. 

\-lors de ces deux journées, une permanence sera organisée pour répondre à vos questions et finaliser vos licences ( de 18h30 à 20h30 ). 

On espère vous voir nombreux et motivés. 

Pour toute question : contact@hbc310.fr 

Toutes les autres catégories reprennent la semaine du 5 septembre. 