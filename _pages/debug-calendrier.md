---
layout: page
title: Calendrier
subtitle: Saison 2024-2025
description: Retrouvez tous les classements des équipes du Handball Club 310 pour la saison 2021-2022!
permalink: /calendrier/

---
<div class="container">

 {% for team in site.teams %}	
    <h2>Team {{team.title}}</h2>
    {% for competition in team.competitions %}	
        <h3>{{competition.url}}</h3>
        <h3>{{competition.file}}</h3>
        {% if competition.equipe %}
            <h3>Equipe{{competition.equipe}}</h3>
        {% endif %}
        <div class="container">
            <div class="columns is-multiline">
                {% for match in site.data.ffhb.[competition.file] %}
                    {% include widget-ffhb-match-team.html match=match title=competitionTitle %}
                {% endfor %}
            </div>                 
        </div>                 
    {% endfor %}
{% endfor %}  
</div>


