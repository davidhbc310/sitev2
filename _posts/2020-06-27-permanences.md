---
date: 2020-06-27 17:55:00
images:
- /uploads/telechargement__qcldkk.png
layout: post
title: Permanences
---
<div class="news__content smart_pictures_width">
<p>Pour déposer vos chèques</p>
<p>Pour déposer vos tenues de match</p>
<p>Pour poser des questions si vous avez besoin d'aide ou d'assistance sur la procédure d'inscription</p>
<p>Pour venir nous dire un petit bonjour...</p>
<p><strong>Rendez-vous devant la salle de Bréal mercredi 1er juillet de 18h30 à 19h30 et vendredi 3 juillet de 19h à 20h devant la salle Beauséjour de Mordelles.</strong> </p> </div>