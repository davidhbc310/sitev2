---
date: 2018-05-26 12:33:00
images:
- /uploads/flyer_greenball_2018_v4__p9c001.png
layout: post
title: LE tournoi ET LA Soirée de l'année!
---
<div class="news__content smart_pictures_width">
<h2 style="text-align:center;"><span style="color:#ff0000;"><strong>EDITION 2018 GREENBALL</strong></span></h2>
<h3 style="text-align:center;"><span style="color:#ff0000;"><em><strong>A ne manquer sous aucun prétexte!</strong></em></span></h3>
<p>Grande <span style="text-decoration:underline;color:#0000ff;"><strong>NOUVEAUTÉ</strong></span> cette année = <span style="text-decoration:underline;color:#0000ff;"><strong>une soirée club avec un Concert</strong></span>. <em>Voir modalités auprès de votre entraîneur</em></p>
<p><em>inscription soirée avant le 10 juin : <a href="https://doodle.com/poll/q2a8tpmwfmgtkqas" rel="nofollow noreferrer noopener">https://doodle.com/poll/q2a8tpmwfmgtkqas</a>   </em></p>
<p>Présentation de la <strong><span style="color:#ff00ff;">boutique KEMPA</span></strong> en partenariat avec SPORT2000-Guichen avec essayage des produits de l'opération PROMO fin d'année (voir sur le site <a href="{{site.url}}/boutique/" rel="nofollow noreferrer noopener" target="_blank">ICI</a>)</p>
<p>Venez nombreux, en famille, avec vos amis, de tout niveaux et de tout âges (2 tournois : jeune et adulte).</p>
<p>On compte sur vous!</p>
 </div>