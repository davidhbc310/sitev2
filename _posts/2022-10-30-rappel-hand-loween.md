---
layout: post
date: 2022-10-30 13:10:40 +0000
title: 'Rappel : HAND''LOWEEN'
tags:
- a la une
- important
images:
- "/uploads/jack-o-lantern-3735387_1920_2022_10_30.jpg"

---
\[RAPPEL\]   
On se rapproche de notre tournoi déguisé de Hand'loween samedi 5 novembre ! 

N'oubliez pas d'indiquer votre présence en remplissant le formulaire dédié.   
[https://forms.gle/2UjwK2foScpSFZc8A](https://forms.gle/2UjwK2foScpSFZc8A "https://forms.gle/2UjwK2foScpSFZc8A")

On compte sur la présence d'un maximum de monstres...