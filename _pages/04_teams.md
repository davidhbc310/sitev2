---
layout: equipes
title: Les équipes du HBC 310
permalink: /equipes/
description: Toutes les équipes engagées parle Handball Club 310, de l'école de Handball aux Seniors, aux niveaux départemental et régional
images:
 - /uploads/images/pages/equipes.png
header-buttons:
 - {label: Ecole de handball,
        link: /equipes/#ecole
   }
 - {label: Jeunes,
        link: /equipes/#jeunes
   }
 - {label: Adultes,
        link: /equipes/#adultes
   }
 - {label: Loisirs,
        link: /equipes/#loisirs
   }
categories:
 - {code: ecole,
        libelle: Ecole de handball
   }
 - {code: jeunes,
     libelle: Jeunes
   }
 - {code: adultes,
    libelle: Adultes
 }
 - {code: loisirs,
    libelle: Loisirs
 }


---



