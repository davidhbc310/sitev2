---
layout: post
date: 2022-07-13 21:52:29 +0000
title: Précisions chèques vacances / pass sport et inscriptions
tags:
- important
images: []

---
A la suite de quelques remarques je vais donc préciser la démarche. 

Si vous voulez payer par chèques vacances OU / ET si vous bénéficiez d'un PASS SPORT. 

\->vous effectuez la pré inscription via HELLOASSO en indiquant à la fin de la procédure le code ZEROHBC310 ( vous ne paierez rien mais vous serez pré inscrits ). 

\->ensuite vous régulariserez votre situation lors des permanences ( fin août / aux forums et à la reprise des entraînements ) avec les chèques vacances ( +1.50 de frais de traitement ) et / ou le justificatif du pass sport. 

Bien entendu si les démarches ont été entreprises avant le 30 juillet vous bénéficierez de la réduction de 10€. 

Si vous avez des questions envoyez un mail : contact@hbc310.fr 