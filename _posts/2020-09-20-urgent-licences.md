---
date: 2020-09-20 19:06:00
images:
- /uploads/programme-de-reprise-322__qgyvjo.png
layout: post
title: URGENT licences
tags: [competitions,important,a la une]
---
<div class="news__content smart_pictures_width">
<p>Allez dernière publication. Les brassages commencent dans deux semaines, certaines équipes ( en région ) auront des matchs amicaux samedi prochain. Il est URGENT de finaliser vos documents ou de faire vos licences. Pas de licence, licence incomplète...pas de matchs ! On compte sur vous et faire grimper le nombre de licenciés ( 220 à ce jour ).</p>
<p>Lien direct vers le module d'inscription en cliquant <span style="font-size:24px;"><a href="https://www.helloasso.com/associations/handball-club-310/adhesions/inscription-handball-club-310-saison-2020-2021-2" rel="nofollow noreferrer noopener"><strong>ICI</strong></a></span></p> </div>