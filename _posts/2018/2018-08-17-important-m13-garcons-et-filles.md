---
date: 2018-08-17 22:30:00
images:
- /uploads/26__mz38z9.jpg
layout: post
title: IMPORTANT - M13 Garçons et Filles.
---
<div class="news__content smart_pictures_width">
<p><span style="color:#000000;">Bonjour à tous,</span></p>
<p><span style="color:#000000;">Veuillez nous excuser les changements de salle sur les premières semaines de septembre dans certaines catégories. </span></p>
<p><span style="color:#000000;">Pour les M13, les forums, le nombre important de joueurs et l'envie de créer une équipe féminine complique un peu le début de saison.</span></p>
<p><span style="color:#000000;">Pour ce mercredi (<span style="text-decoration:underline;"><strong>le 12 septembre</strong></span>) encore, nous allons faire un <span style="text-decoration:underline;"><strong>entrainement mixte (17h salle Coubertin à Mordelles)</strong></span>.</span></p>
<p><span style="color:#000000;">A partir de la semaine prochaine (mercredi 19 Septembre)</span></p>
<p><span style="color:#000000;">- <span style="color:#0000ff;"><strong>les garçons</strong></span> seront de <strong><span style="color:#ff00ff;">17h à 18h30 à Colette Besson (Bréal avec Laurent)</span></strong></span><br/><span style="color:#000000;">- <strong><span style="color:#ff00ff;">les filles</span></strong> seront de <strong><span style="color:#0000ff;">17h à 18h30 à Coubertin (Mordelles avec Adrien)</span></strong></span></p>
<p><span style="color:#000000;">Si vous connaissez des jeunes filles nées en 2008, 2007 ou 2006 (CM2, 6ème et 5ème), qui peuvent être intéressées par le hand, n'hésitez pas à les diriger vers nous!</span></p>
<p> </p>
<p><span style="color:#000000;">L'équipe Technique.</span></p> </div>