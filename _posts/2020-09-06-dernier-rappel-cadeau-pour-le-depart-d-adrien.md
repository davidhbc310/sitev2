---
date: 2020-09-06 18:40:00
images:
- /uploads/810-001__qg8wzd.jpg
layout: post
title: 'Dernier rappel : cadeau pour le départ d''Adrien'
---
<div class="news__content smart_pictures_width">
<p>Depuis le mois de juillet une cagnotte en ligne a été lancée pour permettre à celles et ceux qui le souhaitent de remercier Adrien pour ses années passées auprès de nos jeunes du HBC 310. </p>
<p>Voici le lien : <a href="https://www.helloasso.com/associations/handball%20club%20310/collectes/cadeau-commun-pour-adrien-hbc-310" rel="nofollow noreferrer noopener">https://www.helloasso.com/associations/handball%20club%20310/collectes/cadeau-commun-pour-adrien-hbc-310</a></p>
<p>Vous aurez la possibilité de laisser un petit message personnalisé. </p>
<p>Cette somme lui sera versée lors de l'assemblée générale ( qui reste à fixer ).</p> </div>