---
date: 2019-07-01 10:20:00
images:
- /uploads/45__mz38z9.jpg
layout: post
title: Inscriptions ou ré inscriptions saison 2019 2020
---
<div class="news__content smart_pictures_width">
<p>Tous les documents sont disponibles en fin d'article, merci de bien lire le règlement intérieur ainsi que la charte du club. </p>
<p>Pour les réinscriptions, les responsables d'équipes ont du vous envoyer une liste des éléments à renouveler ( photo éventuelle ou certificat qui ne sera plus valide la saison prochaine ). </p>
<p>A noter : les prochaines permanences du club pour récupérer l'ensemble des documents : </p>
<p><strong>-vendredi 30 août : 19h30-20h30 à Bréal ( salle Colette Besson )</strong></p>
<p><strong>-samedi 31 août à partir de 10h à Mordelles ( Beauséjour ) pendant le tournoi féminin. </strong></p>
<p><strong>-samedi 7 septembre FORUM DES ASSOCIATIONS : présence du HBC 310 le matin à Bréal à St Thurial et au Rheu . L'après midi à Mordelles. </strong></p>
<p>Si vous avez des questions n'hésitez pas à les envoyer à <a href="mailto:35hbc310@gmail.com">35hbc310@gmail.com</a> ou <a href="mailto:5335071@ffhandball.net">5335071@ffhandball.net</a>.</p>
<p><a href="https://www.hbcrhuys.com/media/uploaded/sites/443/document/5d10ec13cdf69_201920certificatmedical.pdf" rel="nofollow noreferrer noopener">Lien certificat médical</a></p>
<p><a href="https://www.hbcrhuys.com/media/uploaded/sites/443/document/5d10ec37e2a1f_201920attestationquestionnairesante.pdf" rel="nofollow noreferrer noopener">Attestation questionnaire de santé </a>( pour  les réinscriptions avec un certificat encore valide )</p>
<p><a href="https://www.casimages.com/f.php?f=190707082815955151.pdf" rel="nofollow noreferrer noopener">Règlement intérieur </a></p>
<p><a href="https://www.casimages.com/f.php?f=190707082904322137.pdf" rel="nofollow noreferrer noopener">Charte du club </a></p>
<p><a href="https://www.casimages.com/f.php?f=190706123405158063.pdf" rel="nofollow noreferrer noopener">Formulaire à compléter et document d'explication</a></p>
<p><a href="https://www.casimages.com/f.php?f=19071108420475362.pdf" rel="nofollow noreferrer noopener">Projet planning entraînements 2019-2020 </a></p>
<p><a href="https://rozhanddu29.fr/wp-content/uploads/2019/06/2019-20_Autorisation_parentale-1.pdf" rel="nofollow noreferrer noopener">Autorisation parentale </a></p>
<p> </p> </div>