---

layout: page
title: Contact
subtitle: Contactez-nous  pour toute question ou demande
description: Contactez le Handball Club 310 pour question sur les inscriptions, les compétitions, les partenariats ou tout autre sujet
permalink: /contact/
categories: club
images:
 - /uploads/images/pages/contact.png
---

<script>
</script>
<div class="columns is-mobile">
    <div class=" column is-6-desktop is-12-touch " >
      {% include form-contact.html   %}
    </div>
  <div class=" column is-6-desktop is-12-touch " >
    <span class="is-hidden-touch">
      <img src='{{ "/uploads/images/pages/equipes/default.png" | prepend: site.url }}' />
    </span>
  </div>
</div>
