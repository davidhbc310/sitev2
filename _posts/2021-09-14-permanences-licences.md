---
layout: post
date: 2021-09-14T16:50:41.000+00:00
title: 'Permanences licences '
tags:
- inscriptions
images:
- "/uploads/permanences.jpg"

---
**Permanences licences pour finaliser les paiements :** 

\-mercredi 15 à Bréal salle du collège de 18h45 à 20h30

\-jeudi 16 : 19h-20h30 à Bréal salle du collège

\-samedi 18 : 11h-12h à Mordelles salle Beauséjour ( entraînement des Arthur )

Si vous avez des questions : contact@hbc310.fr