---
layout: post
date: 2022-11-09 12:32:45 +0000
title: 'Rappel : boutique club '
tags:
- a la une
images:
- "/uploads/unnamed_2022_11_09.png"

---
Bientôt Noël, nous allons proposer une offre spéciale dans la boutique club très prochainement !   
N'hésitez pas à vous créer un compte pour voir ce qui déjà proposé : t shirt, vestes, sweat, chaussettes, aux couleurs du club.   
Boutique gérée par notre partenaire Sport 2000 avec livraison possible à domicile ( ou en magasin ).

[https://hbc310.fr/boutique/](https://hbc310.fr/boutique/ "https://hbc310.fr/boutique/")