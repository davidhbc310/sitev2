---
date: 2020-07-05 23:43:00
images:
- /uploads/montage-photo-de-patineur-monochrome-1__qd0mzu.png
layout: post
title: Bonnes vacances
---
<div class="news__content smart_pictures_width">
<p>Profitez bien de l'été et revenez en pleine forme pour la reprise. </p>
<p>Prenez soin de vous. </p> </div>