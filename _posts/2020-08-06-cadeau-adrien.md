---
date: 2020-08-06 14:00:00
images:
- /uploads/24__mz38z9.jpg
layout: post
title: cadeau Adrien
---
<div class="news__content smart_pictures_width">
<p>Petit rappel : une cagnotte participative a été mise en place afin de faire un cadeau commun pour remercier Adrien</p>
<p>Voici le lien : <a href="https://www.helloasso.com/associations/handball%20club%20310/collectes/cadeau-commun-pour-adrien-hbc-310" rel="nofollow noreferrer noopener">https://www.helloasso.com/associations/handball%20club%20310/collectes/cadeau-commun-pour-adrien-hbc-310</a></p>
<p>Vous avez la possibilité de mettre un petit message personnalisé. </p>
<p>Ce cadeau lui sera remis le soir de l'Assemblée générale le 4 septembre. </p> </div>