---
date: 2020-06-24 13:46:00
images:
- /uploads/j-avais-une-licence-au-hbc-310-l-an-dernier-6__qcfi0v.png
layout: post
title: Descriptif procédure inscriptions
---
<div class="news__content smart_pictures_width">
<p>Voici donc un document en image vous permettant de comprendre un peu le procédé et les nouveautés mises en place pour éviter de déplacer trop de monde. Vous aurez la possibilité de tout faire en ligne  ! Avec un paiement 100% sécurisé ( en 1 ou 3 fois ). Les licencié.e.s ( et leurs parents pour les mineurs ) vont recevoir un mail explicatif en plus ( avec notamment un passage relatif au prix, décomposant le prix de la licence pour vous permettre de mieux comprendre les tarifs ).</p>
<p><strong>On pense lancer le tout demain ou vendredi</strong>. On bloque toujours un peu avec les réponses des mairies ( plannings des salles, lieu des permanences...mais on avance. )</p>
<p>N'oubliez pas déjà les documents importants qui seront donc à conserver et à envoyer directement en fin de procédure grâce au mail reçu de la fédération. </p>
<p>-le certificat médical à faire compléter : <a href="http://staff.clubeo.com/uploads/hbc310/Medias/2020-21_certificat_medical%20(1).pdf" rel="nofollow noreferrer noopener">2020-21_certificat_medical (1).pdf</a></p>
<p>-l'autorisation parentale ( pour les joueuses et joueurs mineur.e.s ) : <a href="http://staff.clubeo.com/uploads/hbc310/Medias/autorisation%20parentale.pdf" rel="nofollow noreferrer noopener">autorisation parentale.pdf</a></p>
<p>-prévoir aussi une photo d'identité ( un portrait de bonne qualité pris par un téléphone est accepté ).</p>
<p>Comme toujours, n'hésitez pas à me contacter si vous avez des questions : ronanhbc310@gmail.com</p> </div>