---
date: 2020-03-26 07:43:00
images:
- /uploads/35__mz38z9.jpg
layout: post
title: Message du Bureau directeur du HBC 310
---
<div class="news__content smart_pictures_width">
<p>Vous avez dû logiquement recevoir ce mail hier soir ( si non peut être dans vos spams ? ) : je le copie ici </p>
<p> </p>
<p>L'épidémie de COVID-19 qui sévit affecte fortement notre quotidien avec cet épisode inédit de confinement. </p>
<p>L'évolution sanitaire et les mesures prises par les pouvoirs publics ont conduit les instances dirigeantes du handball à décider l'arrêt définitif des compétitions.</p>
<p>Toutes nos pensées vont donc à l'ensemble de nos joueuses et de nos joueurs, des plus petits de l'école de hand aux équipes seniors et loisirs. Tout le monde doit trouver le temps bien loin des terrains ! </p>
<p>Mais nos pensées accompagnent aussi tous les parents et bien entendu nos fidèles partenaires durement impactés et dans une situation économique compliquée. </p>
<p>Le club subit bien entendu de plein fouet cet épisode, économiquement ( annulation d'événements prévus et importants dans le budget comme le loto, la tombola ...), humainement avec la mise en chômage partiel d'Adrien, notre salarié.</p>
<p>Pour autant il faut rester optimiste pour la suite, <strong>le plus important est de rester en bonne santé </strong>afin de pouvoir effectuer une reprise dans de bonnes conditions, dès la fin de la période du confinement si c'est possible ( entraînements, matchs amicaux ? ) ou la saison prochaine. </p>
<p>Restez bien connectés et suivez le club sur les réseaux ( site internet, page Facebook ou compte instagram ). </p>
<p>Tous ensemble nous affronterons cette épreuve et nous reviendrons plus motivés que jamais,  </p>
<p>Prenez bien soin de vous, </p>
<p> </p>
<p><strong><em>Le bureau directeur du Handball club 310 </em></strong></p>
<p> </p> </div>