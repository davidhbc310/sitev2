---
date: 2020-02-09 10:22:00
images:
- /uploads/dsc_0092__q5fgqb.jpg
- /uploads/84960777_1530317177123586_1928993984433094656_o__q5fgqc.jpg
layout: post
title: Journée historique
---
<div class="news__content smart_pictures_width">
<p>Pour la première fois de son histoire, le HBC 310 a vu l'ensemble de ses équipes en compétition ce week-end avec la même tenue. Un grand merci à nos partenaires qui ont permis de réaliser ce projet. </p> </div>