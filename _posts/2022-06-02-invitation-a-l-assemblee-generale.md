---
layout: post
date: 2022-06-02 17:36:49 +0000
title: 'Invitation à l''Assemblée générale '
tags:
- a la une
images:
- "/uploads/telechargement.jpg"

---
Les mails ont été envoyés mais il est possible que certaines adressent bloquent ou que la liste ne soit pas tout à fait complète.

Voici donc le mail et les informations à retenir.

**Assemblée générale-17 juin**

Bonjour à toutes et à tous,

Vous êtes convié.e.s à **l'Assemblée générale du club qui se déroulera le vendredi 17 juin à Bréal ( foyer de la salle du collège ) à partir de 19h30**.

L'ordre du jour est précisé en bas de mail en annexe 1.

Un tournoi interne de hand à 4 sera organisé pour les catégories moins de 11, moins de 13, moins de 15 et moins de 18 de 18h à 20h.

L'Assemblée sera clôturée par un pot de fin de saison et par un repas convivial. Chacun apporte de quoi partager. L'organisation du tournoi et du repas sera assurée par les coachs via les outils de communication habituels.

**_Nous comptons sur votre présence pour ce moment important dans la vie du club. Nous avons besoin de nouveaux bénévoles pour s'investir de façon ponctuelle ou plus régulière ( pôle partenariat, pôle événementiel...)._** N'hésitez pas à prendre contact avec nous si vous avez des suggestions à formuler, des questions à poser lors de l'AG ou si vous voulez vous proposer en tant que bénévole. ( mail : contact@hbc310.fr ).

Vous trouverez une procuration en cas d'absence à remettre au licencié de votre choix en annexe 2 en bas et à présenter au début de l'Assemblée.

Excellente journée,

Sportivement,

**Annexe 1 : Ordre du jour de l'Assemblée générale**

Partie 1 : bilan de la saison 2021-2022

\-Bilan Moral

\-Bilan financier

\-Bilan sportif

\-Bilan événementiel

\-Bilan communication

Partie 2 : les perspectives pour la prochaine saison 2022-2023

\-Prévisionnel financier

\-Prévisionnel sportif

\-Les projets

\-Tarif licences

Partie 3 : Composition du CA

\-Sortants / entrants au CA

**Annexe 2 : Procuration**

( cliquez sur télécharger le fichier, il sera ensuite à imprimer et à compléter )

[procuration.pdf](/uploads/procuration.pdf "procuration.pdf")

Ronan Céron, coprésident, pour le Bureau du HBC 310.
