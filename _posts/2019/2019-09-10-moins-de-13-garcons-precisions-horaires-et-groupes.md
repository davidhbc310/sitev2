---
date: 2019-09-10 18:14:00
images:
- /uploads/31__mz38z9.jpg
layout: post
title: Moins de 13 garçons ( précisions horaires et groupes )
---
<div class="news__content smart_pictures_width">
<p>Le planning a été légèrement modifié et nécessite apparemment des précisions : </p>
<p>-le groupe moins de 13-1 est constitué de "confirmés" nés en 2007 capables de s'entraîner avec les moins de 15.</p>
<p>-le groupe moins de 13-2 concerne les 2008 et les débutants. </p>
<p>Ces modifications sont provisoires à la suite de dégradations commises dans la salle de Bréal. Très vite les deux groupes moins de 13 seront réunis et auront deux entraînements en commun par semaine ( sans les moins de 11 et sans les moins de 15 ), le mercredi à Bréal et le vendredi. Plus d'infos aux entraînements par Adrien. </p>
<p>Plus de précisions si besoin par mail : 35hbc310@gmail.com</p>
<p> </p>
<p> </p> </div>