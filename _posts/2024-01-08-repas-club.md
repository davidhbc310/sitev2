---
layout: post
date: 2024-01-08 22:00:05 +0000
title: Repas Club
tags: 
- a la une
images:
- "/uploads/412738366_860678692726050_735026690659718423_n.jpg"

---

A noter dans vos agendas, le repas club, un moment convivial incontournable, **le vendredi 26 janvier à la Biardais à Mordelles**.  
Motivez autour de vos dans chaque catégorie pour avoir le plus de monde et passer un bon moment tous ensemble.  

    
<a href="https://www.helloasso.com/associations/handball-club-310/boutiques/repas-club-hbc-310-2024?fbclid=IwAR0rP_5un1_uEJmIst3LyY0DT38xPNkP7bUUJRNgklnMmN2z1izzNoqRdwY" class="button is-dark" target="_blank">Je m'inscris au repas</a>


