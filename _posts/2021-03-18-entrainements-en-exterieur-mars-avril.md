---
layout: post
date: 2021-03-18 07:05:13 +0000
title: Entraînements en extérieur
tags:
- a la une
images:
- "/uploads/handball-club-310-13.png"

---
Voici le nouveau planning évoqué dans le mail envoyé à tous les licenciés ( si vous ne recevez rien, vérifiez vos spams ou envoyez moi un mail : ronanhbc310@gmail.com ). Alternance des deux communes, mercredi à Mordelles, samedi à Bréal dans la cour de l'école Pierre Leroux.

En cas de pluie ces créneaux seront annulés. 