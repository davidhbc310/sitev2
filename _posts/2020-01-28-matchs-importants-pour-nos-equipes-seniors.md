---
date: 2020-01-28 18:58:00
images:
- /uploads/infographie-match__q4twkt.png
layout: post
title: Matchs importants pour nos équipes seniors.
---
<div class="news__content smart_pictures_width">
<p>Dimanche 2 février, on compte sur votre présence MASSIVE dans les tribunes de la salle Davoine à Beauséjour ( à dtoite en rentrant ) pour encourager nos équipes seniors qui ont besoin de la victoire dans l'optique du maintien. </p>
<p>A 14 H les seniors garçons 1 reçoivent l'Armor Club la Motte 2. </p>
<p>A 16 H les seniors filles affrontent l'OC Montauban de Bretagne. </p>
<p><strong>MOBILISATION générale ! </strong></p> </div>