---
layout: team
code: LOISIRS
title: Loisirs
categorie: loisirs
coach: 
ages: "en 2006 et avant"
sexe: M
need-help: false
hide_classement: true
#widget-prochain-match : 5f8c6b5ba748730a18a3caea
description: L'équipe 1 "Loisirs" du Handball Club 310 regroupe des joueurs et joueuses de tout niveau souhaitant pratiquer le handball sans les contraintes d'une compétition.
header-buttons:
    - {label: Equipe Bréal,
        link: /equipes/loisirs/#equipe1
    }
    - {label: Equipe Mordelles,
        link: /equipes/loisirs/#equipe2
        }
competitions:
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/loisir-bretagne2024-2025-26612/poule-155191",
        file: saison-2024-2025-20_regional_loisir-bretagne2024-2025-26612_poule-155191,
        equipe: 1,
        name: Bréal
    }
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/loisir-bretagne2024-2025-26612/poule-155090",
        file: saison-2024-2025-20_regional_loisir-bretagne2024-2025-26612_poule-155090,
        equipe: 2,
        name: Mordelles
    }


       


---

