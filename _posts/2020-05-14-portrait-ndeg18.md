---
date: 2020-05-14 09:02:00
images:
- /uploads/hbc-310-47__qab7kk.png
layout: post
title: Portrait N°18
---
<div class="news__content smart_pictures_width">
<span>Portrait N°18-Senior garçon-Yoann</span>
<span>Merci à Yoann d'avoir contribué à cette série de portraits, vous y apprendrez ses secrets maison pour maintenir la forme. Avec tous les manchons à enfiler et la lotion à appliquer, il est le seul senior garçon à arriver la veille du match dans les vestiaires ;) ! </span>
<span>Plus que deux portraits en stock, à vous de jouer ;) ! </span>
<span><span>#Teamhbc310</span></span>
</div>