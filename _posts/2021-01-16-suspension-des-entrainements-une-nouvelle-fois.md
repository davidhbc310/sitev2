---
layout: post
date: 2021-01-16 08:42:43 +0000
title: Suspension des entraînements ( une nouvelle fois ! )
tags: []
images:
- "/uploads/programme-de-reprise-20.png"

---
Une fois de plus, les gymnases ferment leurs portes malgré les protocoles mis en place, les aménagements horaires incessants... Nous ne baissons pas les bras et nous espérons vous revoir très vite sur les terrains pour retrouver ces moments de convivialité et de partage qui nous sont interdits aujourd'hui. Prenez soin de vous.