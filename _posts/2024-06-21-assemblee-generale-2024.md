---
layout: post
date: 2024-06-21 17:00:05 +0000
title: Assemblée générale
tags:
- a la une
images:
- "/uploads/ag_2024.png"

---
Une occasion de faire le bilan de l'année, de se projeter sur la suivante et de passer un moment convivial pour clôturer la saison.
**On espère vous voir nombreux et on souhaiterait voir du monde intégrer le CA** pour avoir une meilleure représentation de l'ensemble des catégories du club dans l'instance principale de l'association. N'hésitez pas à candidater à ce sujet par mail ou par MP.  

Vous trouverez ci-dessous une **procuration** en cas d'absence à remettre au licencié de votre choix et à présenter au début de l'Assemblée, le **budget prévisionnel** ainsi que les **tarifs envisagés pour la saison prochaine**.  


<div class="content">
  
  
  <a class="button is-primary is-outlined" href="{{site.url}}/documents/procuration_ag_2023.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Procuration</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/BudgetBilanPrevis2425.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Budget - Bilan prévisionnel</span>
</a>

<a class="button is-primary is-outlined" href="{{site.url}}/documents/proposition_tarifs2425.pdf" target="blank">
<span class="icon">
<i class="fas fa-file-pdf"></i>
</span>
<span>Proposition Tarifs 2024-2025</span>
</a>



</div>


