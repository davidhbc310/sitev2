---
layout: post
date: 2022-03-24 17:06:10 +0000
title: Important organisation LOTO
tags:
- a la une
images:
- "/uploads/ajouter-un-titre-25.png"

---
Nous avons besoin de votre aide, pour la vente et l'installation ou simplement pour faire des gâteaux et crêpes qui seront venus le jour J. 

C'est très important pour la vie du club de réussir cet évènement. 

On espère pouvoir compter sur vous. 

Merci de vous manifester auprès des coachs ou entraîneurs, via les réseaux ou directement par mail ou téléphone à Isabelle, coprésidente en charge de l'événementiel. 

MERCI. 