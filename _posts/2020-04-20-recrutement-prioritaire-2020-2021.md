---
date: 2020-04-20 16:41:00
images:
- /uploads/infographie-couleur-hbc-310-1__q93cyx.png
- /uploads/hbc-310-2__q98fwb.png
layout: post
title: Recrutement prioritaire 2020-2021
---
<div class="news__content smart_pictures_width">
<p>Bonjour tout le monde, </p>
<p>Le confinement nous permet d'anticiper et de préparer avec sérénité la prochaine saison. Afin de poursuivre sa progression et de confirmer son ambition, le club a besoin de renforts en priorité dans les catégories : </p>
<p>-<strong>moins de 13 filles</strong> ( nées en 2008-2009 ) pour espérer constituer une équipe</p>
<p>-<strong>moins de 15 filles</strong> ( nées en 2006-2007 ) : des joueuses expérimentées pour l'équipe 1 ( Région ) et d'autres joueuses, même débutantes pour l'équipe 2.</p>
<p>-<strong>des seniors filles</strong>. </p>
<p> </p>
<p>A vous d'en parler autour de vous c'est très important. </p>
<p>Des essais seront possibles en fonction de la situation sanitaire ou à la rentrée. </p>
<p>Plus d'infos ou demande de renseignements par mail : ronanhbc310@gmail.com</p> </div>