---
date: 2019-09-05 10:30:00
images:
- /uploads/who_2__pxcnm4.png
layout: post
title: Modifications de quelques créneaux d'entraînement à partir du lundi 9 !
---
<div class="news__content smart_pictures_width">
<p>Suite à des ajustements liés aux effectifs et aux problèmes de salle ( dégradations à Bréal ) voici donc un nouveau planning avec quelques modifications. </p>
<p>Merci d'en tenir compte. </p>
<p>Ronan</p> </div>