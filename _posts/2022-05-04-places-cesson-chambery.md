---
layout: post
date: 2022-05-04 15:24:25 +0000
title: Places Cesson Chambéry
tags: []
images: []

---
Important : pour celles et ceux qui le peuvent, je propose une petite permanence de 20h à 20h30 ce mercredi à la salle de Bréal pour récupérer et payer vos places pour Cesson ( ce qui donnera plus de liberté sur les horaires de venue jeudi ).

Pour les autres, je serai donc jeudi devant la Glaz à 19h30, merci de venir assez tôt idéalement entre 19h30 et 19h45, j'aimerai aussi profiter de ma soirée, manger et voir un peu l'échauffement.