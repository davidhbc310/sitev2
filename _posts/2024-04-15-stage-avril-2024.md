---
layout: post
date: 2024-04-15 22:00:05 +0000
title: Stages des vacances d'avril
tags: 
- a la une
images:
- "/uploads/435762137_939070138220238_5329020768826641038_n.jpg"

---

Ouverture des inscriptions pour les stages d'avril.  
Désolé pour le léger retard, on a procédé à quelques ajustements suite aux retours des questionnaires.  

On espère ainsi avoir plus de filles sur ces journées !

    
<a href="https://www.helloasso.com/associations/handball-club-310/evenements/stages-vacances-avril" class="button is-dark" target="_blank">Je m'inscris au stage</a>


