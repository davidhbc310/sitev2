---
layout: team
code: SF
title: Seniors Filles
subtitle: 
coach: 
categorie: adultes
ages: "en 2006 et avant"
sexe: F
#widget-prochain-match : 5f8c6ad70450a440aabfde9a
#widget-classement : 5f2730de67e8bd0a177dd530
description: L'équipe Senior féminine du Handball Club 310 évolue en deuxième division départementale d'Ille et Vilaine.

competitions:
    - {
        url: "https://www.ffhandball.fr/competitions/saison-2024-2025-20/regional/16-ans-2eme-division-territoriale-feminine-p-8-26140/poule-149826",
        file: saison-2024-2025-20_regional_16-ans-2eme-division-territoriale-feminine-p-8-26140_poule-149826,
        equipe: 
    }

---


