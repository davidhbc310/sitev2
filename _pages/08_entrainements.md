---
layout: entrainements
title: Planning des entraînements
subtitle: Saison 2024-2025
permalink: /entrainements/
categories: club
description: Retrouvez le planning hebdomadaire des séances d'entraînement des différentes équipes du Handball Club 310
images:
 - /uploads/images/pages/entrainements.png



filters-buttons:
 - {label: equipes
   }

categories:
 - {code: ecole,
        libelle: Ecole de handball
   }
 - {code: jeunes,
     libelle: Jeunes
   }
 - {code: adultes,
    libelle: Adultes
 }
 - {code: loisirs,
    libelle: Loisirs
 }

---




    
    
